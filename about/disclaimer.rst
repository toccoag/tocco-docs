Disclaimer
==========

This document has been created purposely for internal use
within Tocco, it's written in a very technical manner, and
is mostly unsuitable for end users. It's public as some content
may proof useful to other parties. However, this document is
provided as-is, without and guarantees for correctness,
currentness, or usefulness.

See `license <https://gitlab.com/toccoag/tocco-docs/-/blob/master/LICENSE>`_
for a binding, legal disclaimer.


Help for End Users
==================

Check out our official, end-user manual:

.. figure:: resources/new_client.png

   Opening the manual from the admin interface.

.. figure:: resources/old_client.png

   Opening the manual from the admin interface (old interface).
