.. highlight:: yaml
.. index::
   single: Ansible; environment variable
   single: Ansible; application property

Ansible: Properties and Env. Vars.
==================================

.. index::
   single: application property; set propery
   single: environment variable; set variable

Set Property / Environment Variable
-----------------------------------

Application properties can be set via *env* variable:

config.yml::

    abc:
      env: !merge
        nice2.history.enabled: true
        nice2.history.keep_days: 30
        nice2.history.type: XmlHistory

        # converted to a comma-separated value of 'example.com,example.net' transparently
        nice2.n2c.domains:
        - example.com
        - example.net

        # remove inherited property
        nice2.n2c.session_id: null

**Use !merge as described in** :ref:`ansible-merge-variables`.

.. tip::

    In additional to Application Properties, any environment variable
    can be set via ``env``.


Available Properties / Env. Vars.
---------------------------------

**Properties**

See :doc:`/framework/configuration/configuration-parameters/properties_available`
