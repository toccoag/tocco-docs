.. highlight:: yaml
.. index:: Ansible; usage

Ansible: Usage
===============

Show Available Installations
----------------------------

.. code-block:: text

    $ cd ${ANSIBLE_GIT_REPO}/tocco
    $ ansible-inventory --graph
      @all:
      |--@tocco_installations:
      |  |--@customer_abbts:
      |  |  |--abbts
      |  |  |--abbtstest
      |  |--@customer_agogis:
      |  |  |--agogis
      |  |  |--agogistest
      |  |--@customer_anavant:
      |  |  |--anavant
      |  |  |--anavanttest
      …

| *abbts*, *abbtstest*, *agogis*, … are installations
| *customer_abbts*, *customer_agogis*, … are customers


Run Full Playbook (=Configure Everything)
-----------------------------------------

.. important::

    Always update your repository clone first:

    .. code-block:: sh

        $ cd ${ANSIBLE_GIT_REPO}/tocco
        $ git pull --rebase

.. code-block:: sh

    $ cd ${ANSIBLE_GIT_REPO}/tocco
    $ ansible-playbook playbook.yml -l abbts


.. tip::

    ``-l/--limit`` limits on which installations the playbook is
    executed. You may specify multiple installations and customers
    separated by comma::

        -l abbts,customer_anavant

    This will execute the playbook on installation *abbts* and
    all installations of customer *anavant*.

    Without ``-l/--limit`` the playbook is executed on all installations.


.. index:: Ansible; tags (-t TAG)

Run Playbook Partially (Tags)
-----------------------------

.. important::

    Always update your repository clone first:

    .. code-block:: sh

        $ cd ${ANSIBLE_GIT_REPO}/tocco
        $ git pull --rebase

It's possible to run only parts of the playbook by using what's
called tags. For instance, you can use the tags ``postgres``
and ``s3`` only run tasks setting up these services:

.. code-block:: sh

    $ cd ${ANSIBLE_GIT_REPO}/tocco
    $ ansible-playbook playbook.yml -t postgres,s3

Important Tags:

================ =====================================================
 mail             Configure allowed sender domains and default sender
                  addresses.
 postgres         Setup Postgres user and database and configure
                  connection settings in Tocco.
 route            Configure routes including enabling TLS certificates
                  via Let's Encrypt.
 s3               Setup S3 user and bucked and configure it in Tocco.
 teamcity         Setup continuous delivery in TeamCity
================ =====================================================

.. hint::

    A more complete and current list of tags can be obtained via
    ``--list-tags``. To see what tags tasks have assigned use
    ``--list-tasks``.

.. hint::

    ``--skip-tags TAG1,TAG2`` to skip tasks having certain tags assigned.


Run Playbook in Batches
-----------------------

When applying changes to a large number of installations, in particular
**if the change involves an automatic restart**, it's preferable to run the
playbook on a limited number of installations at a time. To this end,
``-e batch=BATCH_DEFINITION`` can be used to run the playbook in batches.

Examples:

Run the playbook for **one installation at a time**::

    -e batch=1

Run playbook on one installation first, then on five, and then keep
running it 20% of the installations::

    -e batch="[1,5,'20%']"

The next batch is started only when all changes could be applied
successfully.

This is internally implemented using Ansible's `serial keyword`_. Any
value accepted by *serial* can be used.


.. index:: Ansible; debugging

Check Mode
----------

The check mode can be used to show what would be changed without actually
applying the changes::

    $ cd ${ANSIBLE_GIT_REPO}/tocco
    $ ansible-playbook playbook.yml --check

.. warning::

    Many of the tasks modifying OpenShift/kubernetes configurations currently
    report incorrectly changes when running in check mode.

    Namely, these tasks currently report changes incorrectly:

    * *create ansible-edit rolebinding / grant TeamCity access for deployments*
    * *create nice deployment config*
    * *set mail domains*


.. index:: Ansible; debugging

Troubleshooting
---------------

**Debug output**:

Use ``-v`` show parameters passed to a module and the result returned
by it. Use ``-vvv`` to show full debug output.

**Analyze variables**:

You can display variables set for an installation:

.. parsed-literal::

    $ cd ${ANSIBLE_GIT_REPO}/tocco
    $ ansible-inventory --yaml --host **${INSTALLATION}**

or all installations:

.. code-block:: sh

    $ cd ${ANSIBLE_GIT_REPO}/tocco
    $ ansible-inventory --yaml --list


.. index:: Ansible; Vault

Ansible Vault - Passwords and API Tokens
----------------------------------------

All passwords are stored in an encrypted Vault. See :term:`secrets.yml`
and :term:`secrets2.yml`.


.. _serial keyword: https://docs.ansible.com/ansible/latest/user_guide/playbooks_delegation.html#rolling-update-batch-size
