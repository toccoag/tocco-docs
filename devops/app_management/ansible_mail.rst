.. highlight:: yaml
.. index:: Ansible; mail, mail, email

Ansible: Email Settings
=======================

.. _ansible-configure-email-sender-domains:

Configure Email Sender Domains
------------------------------

This is a list of domains that may be used as sender in emails. For instance, if
*tocco.ch* is listed *someuser@tocco.ch* may be used as a sender. Any other sender
address used is rewritten.

#. Ensure SPF and DKIM records are set

   See :doc:`/devops/mail/dns_entries`

#. Set allowed domains:

   .. code-block:: yaml
      :emphasize-lines: 2-4

       abc:   # <- customer
         mail_domains:
           abc.ch:             # <= List domains here
           abc.net:            # <=
         installations:
           abc:
           abctest:

#. Apply change:

    .. parsed-literal::

        ansible-playbook playbook.yml -t mail -l **${INSTALLATION}**


.. hint::

   In case SPF/DKIM has not been configured yet or the customer
   is unable or unwilling to configure it, consider not setting
   a domain at all and using our default, fallback domain. See
   `Default Domain tocco-mailing-service.ch`_

   While strongly discouraged, it's possible to set a `mail_domain`
   without adding a SPF or DKIM record by disabled the automated
   check:

   .. code-block:: yaml
      :emphasize-lines: 4-5

       abc:   # <- customer
         mail_domains:
           abc.ch:
             disable_dkim_check: true    # <= disable DKIM verification
             disable_spf_check: true     # <= disable SPF verification
           abc.net:

   **Expect mails to end up in spam or be refused. Particularly, with
   a missing or incorrect SPF.**


.. _ansible-configure-default-sender-addresses:

Configure Default Sender Addresses
----------------------------------

#. Set mail addresses in *config.yml*::

       abc:   # <- customer
         mail_sender_default: info@abc.ch     # <= Address used when sender domain is not listed
                                              #    in `mail_domains` and no default is set on
                                              #    business unit.

         mail_sender_noreply: noreply@abc.ch  # <= Address used in in special context where
         installations:                       #    replying doesn't make sense. For instance,
           abc:                               #    on the password reset mail.
           abctest:

   The domains of the sender addresses **must** be listed in ``mail_domains``. See above.

#. Apply change:

    .. parsed-literal::

        ansible-playbook playbook.yml -t mail -l **${CUSTOMER}**


.. _ansible-default-domain:

Default Domain *tocco-mailing-service.ch*
-----------------------------------------

The domain *tocco-mailing-service.ch* is used as fallback
for outgoing mail when no domain is configured explicitly.
That is, the following is used as default::

    mail_domains:
    - tocco-mailing-service.ch
    mail_sender_default: noreply@tocco-mailing-service.ch
    mail_sender_noreply: noreply@tocco-mailing-service.ch

This domain is under Tocco's control and may be used
when a customer does not wish to use their own domain or
as an interim solution while the customer is setting up
DKIM/SPF for their domain.


.. index: Ansible; email recipient, email; recipient
.. _ansible-restrict-allowed-mail-recipients:

Restrict Allowed Mail Recipients
--------------------------------

It's possible to restrict outgoing mails to certain domains or addresses by
rewriting the recipient address of mails not explicitly whitelisted.

Enable/disable whitelist in *config.yml*::

    abc:
      installations:
        abc:
          # only send out mails to domains/addresses listed
          # in mail_allowed_recipients
          mail_allowed_recipients_enabled: true
        abctest:
          # send out all mails
          mail_allowed_recipients_enabled: false

.. tip::

    By default, *mail_allowed_recipients_enabled* is set to **true
    on test** systems and **false on production**.

Extend whitelist in *config.yml*::

    # This should generally be set on customer level.
    abc:
      mail_allowed_recipients: !merge
      - example.com
      - jane@example.net

.. tip::

    Always use the !merge type (:ref:`ansible-merge-variables`) to extend the default
    which includes *tocco.ch* and *mail-tester.com*.

Redirect discarded mails *config.yml*::

    abc:
      mail_blackhole_address: joe@example.net

This will redirect any mail to a non-whitelisted recipient to *joe@example.net*.


.. index:: Ansible; mail relay, mail relay
.. _ansible-configure-mail-relay:

Configure Mail Relay
--------------------

For customers using their own mail relays, the following
options can be used for configuration::

    mail_relay: mail.smtp2go.com
    mail_relay_port: 25
    mail_relay_user: user@domain.tld
    mail_relay_password: '{{ secrets2.mail_relay_password.CUSTOMER_NAME }}'

Put password in :term:`secrets2.yml`. Look at existing *mail_relay_password*
entries in *config.yml* and *secrets2.yml* for guidance.
