Migration
=========

.. _elasticsearch_migration:

3.3: Elastichsearch
-------------------

In Short
^^^^^^^^

a) Keep a copy of the freshly migrated DB.
b) Check how long it takes for the index to be created.
c) If indexing would exceed the planned maintenance window,
   follow the instructions in `Details`_ or ask operations to do so.

Details
^^^^^^^

Create an Elasticsearch index when upgrading to v3.3 can take a long time.
When creating a testnew installation for a large customer, check how much
time was needed to create the index.

.. figure:: resources/index_task.png

   Index creation as seen on *Task_execution*.

Time to index, as shown above, can be inaccurate as result of restarts
during the migration. Recreate the index to obtain a more accurate
time.

Index won't be fully available while the index is being created. When index
creation exceeds the maintenance window planned either the customer needs
to be informed that index won't be fully available or the index needs to
be created in advance. You can see in what order entities are indexed and how
long indexing takes for a single entity in the bottom right corder of the above
screenshot. This may be useful when deciding whether it is acceptable for
indexing to exceed the maintenance window.

Create Index in Advance using Temporary Installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For this, create a *{{ customer_name }}test-index-migration* via Ansible
and configure it to use the index from producton::

    ${customer}:
      ${customer}-index-migration:
        monitoring_enabled: false

        # use prod settings
        elasticsearch_index_name: '{{ hostvars[customer_name].elasticsearch_index_name }}'
        elasticsearch_index_replicas: '{{ hostvars[customer_name].elasticsearch_index_replicas }}'
        elasticsearch_servers: '{{ hostvars[customer_name].elasticsearch_servers }}'
        elasticsearch_password: '{{ hostvars[customer_name].elasticsearch_password }}'

Replace *${customer}* with the customer's name but leave *{{…}}* untouched.

Then use a freshly upgraded copy of the DB for this installation and wait for
the indexing to complete. Then stop and :doc:`remove the installation again
</devops/app_management/remove_customer>`.

.. important::

   For this to work, a freshly updated DB must be used. The index check looks
   at pk and version to decide whether the index needs to be updated. There is
   a good chance that pk and version will incorrectly match when the DB
   is being used actively.

The automatic index repair will correct any differences after the prod migration
automatically.

Remove Temporary Installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Make sure to comment out elasticsearch settings when removing temporary installation.
The wrong index will be removed otherwise::

    ${customer}:
      ${customer}-index-migration:
        state: absent

        monitoring_enabled: false

        # use prod index
        # elasticsearch_index_name: '{{ hostvars[customer_name].elasticsearch_index_name }}'
        # elasticsearch_index_replicas: '{{ hostvars[customer_name].elasticsearch_index_replicas }}'
        # elasticsearch_servers: '{{ hostvars[customer_name].elasticsearch_servers }}'
        # elasticsearch_password: '{{ hostvars[customer_name].elasticsearch_password }}'
