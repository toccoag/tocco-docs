.. highlight:: yaml
.. index:: Ansible; ingress, Ansible; routes

Ansible: Ingress/Route Settings
===============================

.. _ansible-add-ingress:

Add Route / Endpoint
--------------------

#. Add the necessary :doc:`DNS entries </devops/wordpress/dns>`.

#. Add the route to :term:`config.yml`:

   .. code-block:: yaml
      :emphasize-lines: 4-8

       abc:   # <- customer
         installations:
           abc:  # <- installation
             routes:
               abc.ch:
               www.abc.ch:
               xyz.ch:              # <= add the new routes here
               www.xyz.ch:          # <=
           abctest:

   The default route *${INSTALLATION}.tocco.ch* is added implicitly. Only add it
   explicitly if you wish to override the default settings.

   Technical note: the default route, if absent, is added by the inventory script
   (``inventory.py``).

   .. hint::

      If a **reverse proxy** is in front of Nice (e.g. Cloudflare), a route
      needs to be added still and the type set to *proxy*:

      .. code-block:: yaml
         :emphasize-lines: 5-6

          abc:  # <- customer
            installations:
              abc:  # <- installation
                routes:
                  example.net:
                    type: proxy

      Proxies should use *https://${INSTALLATION}.tocco.ch* as upstream.

#. Apply change:

    .. parsed-literal::

        ansible-playbook playbook.yml -t route -l **${INSTALLATION}**

.. hint::

   The required TLS certificates are issued automatically in the background.
   This can take some time though. See :ref:`acme-troubleshooting` if you
   run into any issues.


Wordpress
---------

#. Add the necessary :doc:`DNS entries </devops/wordpress/dns>`
#. Add record to :term:`config.yml`:

   .. code-block:: yaml
      :emphasize-lines: 4-5

       abc:   # <- customer
         installations:
           abc:  # <- installation
             wordpress:
               page.abc.ch:

#. Apply change:

    .. parsed-literal::

        ansible-playbook playbook.yml -t route -l **${INSTALLATION}**

.. hint::

   When you create *page.abc.ch*, a route for *tocco.page.abc.ch* is added
   implicitly. If you need to override settings on the implicit route, add
   it explicitly:

   .. code-block:: yaml
      :emphasize-lines: 6

       abc:   # <- customer
         installations:
           abc:  # <- installation
             routes:
               tocco.page.abc.ch:
                 monitoring_enabled: false
             wordpress:
               page.abc.ch:

Remove Route / Endpoint
-----------------------

#. Remove route from ``config.yml``

#. Apply change:

   .. parsed-literal::

        ansible-playbook playbook.yml -t route -l **${INSTALLATION}**


.. index:: Ansible; monitoring, monitoring

Common Route Settings
---------------------

============================= =======================================================
 monitoring_enabled            Whether to enabled monitoring.

                               Valid values: ``true``, ``false``
 sso_enable_support_login      Enable login via /support-tocco. Enabled by default
                               for *${installation}.tocco.ch*.

                               Valid values: ``true``, ``false``
 hsts_secs                     Adjust *max-age*, in seconds, sent via
                               `Strict-Transport-Security`_ header.
 hsts_include_subdomains       Set *includeSubDomains* in Strict-Transport-Security
                               header.

                               Valid values: ``true``, ``false``
 hsts_preload                  Set *preload* in Strict-Transport-Security header.

                               Dangerous. Read warning on https://hstspreload.org
                               first.

                               Valid values: ``true``, ``false``
 http_request_timeout          Request read timeout enforced by OpenShift.

                               Examples values: ``60s``, ``3m``
============================= =======================================================

Settings can be applyied on customer, installation or route level:

.. code-block:: yaml
   :emphasize-lines: 2,5,9

    abc:
      monitoring_enabled: false  # disable for customer
      installations:
        abc:
          monitoring_enabled: true  # re-enable for installation
          routes:
            abc.org:
            www.abc.org:
              monitoring_alert_tocco: false  # do not send alerts for www.abc.org
        abctest:


.. _Strict-Transport-Security: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
