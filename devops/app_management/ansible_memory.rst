.. highlight:: yaml
.. index:: Ansible; memory, memory

Ansible: Memory Settings
========================

Basics
------

In most cases, memory should be set via ``memory`` on **production**::

    abc:
      installations:
        abc:
          memory: 4.5 GiB
        abctest:
          # memory:       # set based on prod memory


Available Settings
------------------

.. warning::

   The following table corresponds to the implementation for Nice 3.0
   and newer. Some of the variables listed may not have the intended
   effect on older versions. However, ``memory`` is save to use in any
   version.

================ =======================================================
 Name              Description
================ =======================================================
 **memory**       Primary memory setting which  corresponds,
                  approximately, to the maximum memory available to an
                  installation.  This is also the amount of memory which
                  is billed.

                  Memory is lightly overcommitted. That is, the sum
                  of all ``memory`` exceeds available memory, and it
                  expected that average memory use, among all
                  installations, is below ``memory``.

                  **Default**

                  | production: 3.4 GiB
                  | test: 30% to 80% production memory

                  See also definition :term:`global.yml` for details
                  and up-to-date values.

 memory_request   Amount of memory requested on Kubernetes/OpenShift.

                  This should correspond, roughly, to the memory
                  the application is expected to use ignoring peaks.

                  **Default**

                  If omitted, this value is set to a value slightly
                  lower than ``memory`` to account for
                  overcommitment. See ``memory_request_factor`` in
                  :term:`global.yml`.

                  **See also**

                  * `Motivation for CPU requests and limits`_

 memory_limit     Hard memory limit on Kubernetes/OpenShift.  Pods
                  exceeding this limit are killed immediately. This
                  should be set to a value the application is never
                  expected to reach.

                  **Default**

                  Set based on ``memory`` but well above.

                  **See also**

                  * `Motivation for CPU requests and limits`_

 memory_heap      Maximum Java heap memory. Corresponds to the ``-Xmx``
                  option of ``java``.

                  **Default**

                  Set based on a ``memory``. Currently approximates
                  to about 50% of ``memory`` to account for non-heap
                  memory use.

                  See also definition :term:`global.yml` for details.
================ =======================================================


.. _Motivation for CPU requests and limits: https://kubernetes.io/docs/tasks/configure-pod-container/assign-cpu-resource/#motivation-for-cpu-requests-and-limits
