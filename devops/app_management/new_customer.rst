.. index:: Ansible; create installation

Create new Customer or Installation
===================================

Add DNS Entry
-------------

Usually, during initial setup, installations are only made availiable at
*https\//${INSTALLATION}.tocco.ch* for which a DNS record can be added as
described in :ref:`dns-installation.tocco.ch`.

When creating a test system, also create dns records for these domains [#f2]_:

    | ${CUSTOMER}testnew.tocco.ch
    | ${CUSTOMER}testold.tocco.ch

When the customer uses Wordpress (Nice version >= v3.7), add the following dns records:

    ===================================== ============ ==============================
     ${INSTALLATION}.wp.tocco.ch           IN  CNAME    wp1.{prod/stage}.tocco.ch
     web.${INSTALLATION}.tocco.ch          IN  CNAME    ${INSTALLATION}.wp.tocco.ch
     tocco.web.${INSTALLATION}.tocco.ch    IN  CNAME    ${INSTALLATION}.tocco.ch
    ===================================== ============ ==============================

Should you need an additional, non-\ *tocco.ch* records, have a look
at :doc:`/devops/openshift/dns`.


Create OpenShift Project, S3 Bucket and Database
------------------------------------------------

.. warning::

     If you haven't setup Ansible yet, now is time to follow
     the instructions in :ref:`setup-ansible` and read through
     :doc:`ansible_usage`.

#. Update your `Ansible Repository`_ clone::

       cd ${PATH_TO_ANSIBLE_REPOSITORY}/tocco     # Note the `/tocco`
       git pull --rebase

#. Add customer/installation to ``tocco/config.yml``

    .. code-block:: yaml

       # ...
       abc:                                                # <-- customer
         mail_sender_default: info@domain.ch               # <-- fallback mail address
         mail_sender_noreply: noreply@domain.ch            # <-- fallback noreply address
         mail_domains:                                     # <-- domains allowed as email sender address
           domain.ch:
           domain.net:
         installations:
           abc:                                            # <-- production installation
             branch: releases/1.0
             db_server: db7.prod.tocco.cust.vshn.net
             mail_allowed_recipients_enabled: true         # <-- disable outgoing mails except for those listed
                                                           #     in mail_allowed_recipients (usually enabled
                                                           #     while installation is in pilot phase)
             env: !merge                                   # <-- Allow the wordpress installation as
               nice2.web.allowedRequestOrigins:            #     request origin in order for the wordpress
               - https://web.${INSTALLATION}.tocco.ch      #     plugin to work.
             routes:                                       #
               web.${INSTALLATION}.tocco.ch:               # <-- Create routes for the wordpress installation
                 create_ingress: false                     #     but don't issue a letsencrypt certificate
               tocco.web.${INSTALLATION}.tocco.ch:         #

           abctest:                                        # <-- test installation
             branch: releases/1.0
             db_server: db5.stage.tocco.cust.vshn.net
       # ...

   In case DKIM/SPF has not been configured yet, omit the ``mail_sender_default``,
   ``mail_sender_noreply`` and ``mail_domains`` settings. A default, fallback
   domain is used in that case. See :ref:`ansible-default-domain`.

   Just as shown in the example above, use these servers:

   ==================== ==================================================
    Environment          Servers
   ==================== ==================================================
    production / pilot   db_server: db7.prod.tocco.cust.vshn.net
    test                 db_server: db5.stage.tocco.cust.vshn.net
    testold/testnew      db_server: *copy from test*
   ==================== ==================================================

   .. important::

       Naming conventions:

       ===================== ===================================================
        Customer name         * May only contain lower-case letters a-z and
                                digits 0-9 and hyphens (-).
                              * Customer names may not contain the substring
                                "test".

        Installation name     * May only contain lower-case letters a-z,
                                digits 0-9 and hyphens (-).
                              * All installation names should start with
                                with the customer name. Customer systems
                                must.
                              * The **production** system **must** have the
                                same name as the customer itself. [#f1]_ By
                                extension, the substring "test" is banned
                                from appearing.
                              * The **primary test** system **must** be called
                                *{{ customer_name }}test*.
                              * Test systems created during migration are
                                called *{{ customer_name }}testold* and
                                *{{ customer_name }}testnew*.
                              * All further test systems should contain the
                                substring "test".
       ===================== ===================================================

   .. hint::

          More details about Ansible is available in :doc:`ansible_usage`

          Should you need more routes, see :ref:`ansible-add-ingress`.

#. Run Ansible Playbook

   Run playbook for installation **abc** and **abctest**:

   .. parsed-literal::

          cd ${GIT_ROOT}/tocco
          ansible-playbook playbook.yml --skip-tags monitoring -l **abc,abctest**

   Or run it for all installations belonging to **customer abc**:

   .. parsed-literal::

          cd ${GIT_ROOT}/tocco
          ansible-playbook playbook.yml --skip-tags monitoring -l **customer_abc**

.. hint::

    Ansible as shipped by many distribution is currently suffering from an
    incompatibility with our S3-compatible storage:

      Failed to get bucket tags: An error occurred (NoSuchTagSetError) when calling
      the GetBucketTagging operation: Unknown

    Should you see this error, it's easiest to patch Ansible locally to
    work around the issue. You have to find ``s3_bucket.py`` locally and
    patch it as shown in `this pull request`_. The file is likely somewhere
    in ``/usr``::

      find /usr -name s3_bucket.py

.. hint::

    When setting up the primary test system, "${CUSTOMER_NAME}test",
    be sure to run the playbook for the production system too. This
    because, once the test system is configured, Ansible will
    reconfigure the production system to reuse the Docker image
    used by the test system.


Update and Verify Installation Entry in BO
------------------------------------------

* update status
* set server


Add Customer Module
-------------------

:doc:`/framework/configuration/modules/add-customer-module`

(This is done only now as one cannot start an installation localy without
running Ansible first. It creates the S3 bucket used locally too.)


Deploy
------

Once all required changes have been merged, the installation can be deployed.

See :ref:`cd-simple`


Enable Monitoring
-----------------

.. important::

   Only set up monitoring once the installation is online.

.. parsed-literal::

       ansible-playbook playbook.yml -t monitoring -l **customer_abc**


.. rubric:: Footnotes

.. [#f1] By default, Ansible does check if *installation_name == customer_name* to
         decide if an installation is a production system and it will use that
         information to adjust the default settings. (See *installation_type*
         variable in *config.yml*.)
.. [#f2] These domains will later be used during migrations. See
         :doc:`/devops/deployment/migration`


.. _common.yaml: https://git.vshn.net/tocco/tocco_hieradata/blob/master/common.yaml
.. _Ansible Repository: https://git.tocco.ch/admin/repos/ansible
.. _this pull request: https://github.com/ansible-collections/amazon.aws/pull/150/files
