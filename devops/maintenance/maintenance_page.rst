.. index:: maintenance page

Maintenance Page
################

Installation
============

**Debian/Ubuntu users:**

If you followed the instructions in :doc:`/ide/machine_setup`, you'll find the
``tocco-mntnc`` command already installed.

**non-Debian/Ubuntu users:**

Clone the Git repository::

    cd ~/src/
    git clone https://gitlab.com/toccoag/maintenance-page.git

Then use the ``./mntnc`` script in the repository instead of the
``tocco-mntnc`` command.

Example::

    cd ~/src/maintenance-page
    ./mntnc status  # instead of `tocco-mntnc status`

Usage
=====

1. Login ``oc login``
2. Go to the project where the maintenance page should be deployed (e.g. ``oc project nice-master``)
3. Enable maintenance page ``tocco-mntnc start [-t "24.12.2020 6:00"]``

   Selected options:

   * ``-t``/``--till``: date/time until the application is unavailable If the argument is present, the message is *Please try again on [date/time]*. Else the more generic message *Please try again later.* is shown.
   * ``-h``/``--help``: see all available options

4. Disable/enable bypass as needed via */_maintenance/bypass*
   URL. Exact URL is printed by ``tocco-mntnc start``.

5. Disable maintenance page ``tocco-mntnc stop``
