.. index::
   pair: DNS; mail (outgoing)

DNS Records for Outgoing Mails
==============================

.. index:: mail (outgoing); SPF, DNS; SPF, SPF
.. _spf-record:

Create SPF Record
-----------------

As shown below, ``include:spf.tocco.ch`` needs to be added to the SPF record. The record shown below may need to be
extended depending on whether another relay is used to send mails from the same domain. Also, ``?all`` is
generally safe but the domain owner may wish to use ``~all`` or ``-all`` to avoid that anyone else can send mails using
her domain.

.. code::

    @ IN TXT "v=spf1 … include:spf.tocco.ch ?all"

.. warning::

    ``~all`` and ``-all`` may lead to mails being rejected or considered spam if the policy is incorrect. Be careful!

Wikipedia has comprehensive article on `SPF`_ if more information is needed.

.. figure:: nine_spf.png
    :scale: 60%

    Sample SPF record in Nine web interface

.. _SPF: https://en.wikipedia.org/wiki/Sender_Policy_Framework


Verify SPF Records
------------------

#. Go to https://mxtoolbox.com/spf.aspx
#. Enter domain name
#. Verify there is no errors printed
#. Verify record contains "include:spf.tocco.ch"

Have a look at `Verify Correctness`_ should you
require a more comprehensive test. Usually
only needed if there are known problems.


.. index:: mail (outgoing); DKIM, DNS; DKIM, DKIM
.. _dkim-record:

Create DKIM Record
------------------

A ``CNAME`` record for the name ``default._domainkey`` needs to be created.

.. code::

    default._domainkey IN CNAME default._domainkey.tocco.ch.

See wikipedia entry on `DKIM`_ for more details.

.. _DKIM: https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail

.. hint::

   In case the selector name ``default`` is used, an alternative selector
   called ``tocco`` can be used:

   #. Switch domain to the ``tocco`` selector. See commit `e3c08e3ff16b`_ for
      an example.
   #. Add the following DNS record::

          tocco._domainkey IN CNAME tocco._domainkey.tocco.ch.
   #. Use alternative selector in :term:`config.yml`:

      .. code-block:: yaml

          example:  # <-- customer
            mail_domains:
              example.net:   # <-- domain
                dkim_selector_name: tocco
                dkim_value: '{{ _dkim_value_selector_tocco }}'

.. figure:: nine_dkim.png
    :scale: 60%

    Sample CNAME record in Nine web interface redirecting to our DKIM entry.


Verify DKIM Record
------------------

#. Go to https://mxtoolbox.com/dkim.aspx
#. Enter domain name and "default" as selector
#. Verify there is no errors printed
#. Check value is correct by checking if record contains "MyHW9JhhHiLdYNar9H77Ob1"
   (which is part of the public key).
#. Go to https://dnslookup.online/txt.html
#. Look up "default._domainkey.${DOMAIN}" (e.g. default._domainkey.example.net)
#. Ensure result contains

   ====== ==============================
    Type   Cname    
   ====== ==============================
    CNAME  default._domainkey.tocco.ch
   ====== ==============================

.. hint::

    Old records may be missing the CNAME record. This is
    okay but new records **must** contain it.

Have a look at `Verify Correctness`_ should you
require a more comprehensive test. Usually
only needed if there are known problems.

.. index:: mail (outgoing); DMARC, DNS; DMARC, DMARC

Create DMARC Record
-------------------

A ``TXT`` entry for the name ``_dmarc`` needs to be created. Reports about mails rejected or classified as spam are sent
the the mail address specified in the record.

The subdomain policy, ``sp``, may need to be adjusted if the domain owner wishes to send mails from subdomains. The
policy ``p=none`` is generally safe but it can also be set to ``quarantine`` or ``reject`` if a stricter policy is
desired.

.. code::

    _dmarc IN TXT "v=DMARC1;p=none;sp=quarantine;pct=100;rua=mailto:dmarcreports@example.com"

.. warning::

    Be careful with ``quarantine`` and ``reject``. If SPF or DKIM records fail to validate the mail is going to be moved
    to spam or reject respectively.

Wikipedia has some more details on `DMARC`_.

.. _DMARC: https://en.wikipedia.org/wiki/DMARC

.. figure:: nine_dmarc.png
    :scale: 60%

    Sample DMARC record in Nine web interface


Verify Correctness
------------------

There are several online services that test the records for you. `Mail Tester`_ is one of the simpler ones and it covers
all the DNS entries listed above.


.. _Mail Tester: https://www.mail-tester.com/
.. _e3c08e3ff16b: https://git.vshn.net/tocco/tocco_hieradata/-/commit/e3c08e3ff16bfdbf59f7ebd711e77a9c924d5bf9
