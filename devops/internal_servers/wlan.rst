Wlan Network
============

The Network
-----------

Tocco has an internal Wlan consisting of five Unifi APs [#f1]_

  ======================== ======================== ======================== ========================
   Type                     Location                 Domain                   IP
  ======================== ======================== ======================== ========================
   UniFi AP-Pro             2nd floor PM             unifi01.tocco.ch         DHCP
   UniFi AP-Pro             2nd floor BS             unifi02.tocco.ch         DHCP
   UniFi AP-Pro             2nd floor Aquarium       unifi03.tocco.ch         DHCP
   UniFi AP-Pro             1st floor Maurice        unifi04.tocco.ch         DHCP
   UniFi AP-Pro             1st floor Dev / Devops   unifi05.tocco.ch         DHCP
  ======================== ======================== ======================== ========================

Access The Control Software
---------------------------

The network and the APs can be controlled via the **Unifi Controller Software**. It is installed on unifi.tocco.ch Debian vm on host03a.
You can install it on your local machine but it is not recommended. You have to reconfigure all APs. That means that all wireless networks will be reset.
Only do it in case of a broken/unavailable edge vm. If so, follow the `installation manual <https://dl.ubnt.com/guides/UniFi/UniFi_Controller_UG.pdf>`_ under Chapter 1: System Setup.

#. Open a browser and navigate to https://[2001:8e3:5396:c92::3e]:8443/. The credentials are stored in ansible-vault. See :term:`secrets.yml`

#. The UniFi Control Panel will open.


Unifi Controll Panel
--------------------

The Control Panel has a very intuitive design but there are two basic interfaces which we need to configure/maintenance the wlan's.

#. The Panel for Devices. Here you can manage all the APs (update, restart).

   .. figure:: wlan/unifi_device_panel.png

#. The Panel for wlan's. You can find it in the settings tooltip. Just click on the button named **Wireless Networks**.
   Then The panel opens and you see all the available wlan's. In case of a reset of the APs you have to configure the wlan's like in the second picture.

   .. figure:: wlan/unifi_network_setting_panel.png

#. From time to time you should change the guest wlan password.

   .. figure:: wlan/unifi_change_guest_pw.png

.. [#f1] Abbreviation for Access Point.

