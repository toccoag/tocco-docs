DITA manual and specification
*****************************

Heretto CMS (since Nice v3.3)
=============================

To create Tocco manuals and specifications several repositories are needed.

Content
-------

In contrast to the old manual, the content sources aren't kept in a Git repository anymore. Now, they're managed
in a DITA CMS: https://tocco.easydita.com/ezdnxtgen

Plugins and Templates
---------------------

To generate PDF and HTML artifacts from the DITA sources in the CMS, some plugins and templates are needed.
Here's a list of the repositories containing those plugins and templates. Check the README.md in those repositories
for further information.

.. list-table::
   :header-rows: 1

   * - Repository
     - Description
   * - `heretto-pdf-template <https://gitlab.com/toccoag/heretto-pdf-template>`_
     - The template for the PDF artifacts.
   * - `docs-dita-ot-html-plugin <https://gitlab.com/toccoag/docs-dita-ot-html-plugin>`_
     - DITA OT plugin for the HTML artifacts.

Publication
-----------

Both the PDF and HTML documents are published via the repository
`docs-publish <https://gitlab.com/toccoag/docs-publish>`_. Check the README.md there for futher information.

Source in own repository and DITAC conversion (until Nice v3.2)
===============================================================

To create Tocco manuals and specifications two repositories are needed.

Content
-------

Repository `nice2_documentation <https://git.tocco.ch/#/admin/projects/nice2_documentation>`_ contains content.

Converter
---------

Repository `nice2_documentation_build <https://git.tocco.ch/#/admin/projects/nice2_documentation_build>`_ contains everything to convert and build.

Check `README.md`_ in *nice2_documentation_build* for further information.


.. _README.md: https://git.tocco.ch/gitweb?p=nice2_documentation_build.git;a=blob;f=README.md
