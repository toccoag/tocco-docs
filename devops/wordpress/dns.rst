.. index:: DNS; website/route

DNS (Wordpress)
###############

.. highlight:: ini

.. _wordpress-add-dns-records:

Add DNS Records
===============

.. important::

   This is only for Wordpress hosted by Tocco. Self-hosted solutions
   will need other records.

Finding the Right Instructions
------------------------------

.. list-table::
   :header-rows: 1
   :widths: 10 20 20

   * - Pattern
     - Examples
     - Record
   * - {domain}.{tld}
     - | example.ch
       | example.net
     - :ref:`wordpress-dns-root-domain`
   * - **www**\ .{domain}.{tld}
     - | **www**\ .example.net
       | **www**\ .example.ch
     - :ref:`wordpress-dns-special-case-www`
   * - {subdomain}.{domain}.{tld}
     - | admin.example.net
       | intranet.example.net
       | :strike:`www.example.net`
       | web.dgh.tocco.ch
       | web.dghtest.tocco.ch
     - :ref:`wordpress-dns-subdomain`


.. wordpress-dns-installation.tocco.ch:

${INSTALLATION_NAME}.wp.tocco.ch
--------------------------------

Create a CNAME pointing to the corresponding server::

    example.net           3600 IN ALIAS   wp1.prod.tocco.ch
                                        ; ^^^^^^^^^^^^^^^^^
                                        ; Replace this with the actual server's name


.. _wordpress-dns-root-domain:

Root Domain (example.net)
-------------------------

For root domains, an ALIAS is to be used::

    example.net           3600 IN ALIAS   exampleinc.wp.tocco.ch.
                                        ; ^^^^^^^^^^^^^^^^^^^^^^^^
                                        ; ${INSTALLATION_NAME}.wp.tocco.ch.

.. note::

    **What if type ALIAS is not available?**

    Type ALIAS is sometimes known as ANAME::

        example.net    3600 IN ANAME   exampleinc.wp.tocco.ch.
                                     ; ^^^^^^^^^^^^^^^^^^^^^^^
                                     ; ${INSTALLATION_NAME}.wp.tocco.ch.

    Iff ALIAS and ANAME aren't available, create both of the
    following records instead::

        ; IPv4
        example.net.     3600 IN A      ?.?.?.?
        ; IPv6
        example.net.     3600 IN AAAA   ????:????::????

    Replace *?.?.?.?* / *????:????::????* according to this list:

    ====================  ================  ======================================
     Host                   IPv4               IPv6
    ====================  ================  ======================================
     wp1.prod.tocco.ch     195.15.210.114    2001:1600:13:100:f816:3eff:fe07:411c
     wp1.stage.tocco.ch    84.16.72.78       2001:1600:13:102:f816:3eff:fe65:8eb1
    ====================  ================  ======================================


.. _wordpress-dns-special-case-www:

Special Case *www*
------------------

*www*\ .some-domain.tld should point to the root domain::

    www.example.net       3600 IN CNAME   example.net.
        ^^^^^^^^^^^                       ^^^^^^^^^^^^
      ; root domain                       root domain

.. _wordpress-dns-subdomain:

Subdomain (some-name.example.net)
----------------------------------

Subdomains, with the exception of *www*, should point to
${INSTALLATION}.wp.tocco.ch::

    intranet.example.net  3600 IN CNAME   exampleinc.wp.tocco.ch.
                                        ; ^^^^^^^^^^^^^^^^^^^^^^^
                                        ; ${INSTALLATION_NAME}.tocco.ch.
