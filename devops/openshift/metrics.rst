.. index:: Nice2 debugging; metrics, metrics, OpenShift; metrics

OpenShift Metrics
#################

Cluster-Wide Graphs
===================

.. hint::

   Only available to members of group *tocco-admin*.

.. hint::

   SI units are used, thus, 1 GB = 1,000,000,000 bytes = ~0.93 GiB.

CPU
---

To debug a single application, see also:

* :doc:`/devops/nice/jmx`

All pods sorted by memory usage::

    oc adm top pod --all-namespace --sort-by cpu

Per node usage::

    oc adm top node


Memory
------

To debug a single application, see also:

* :doc:`/devops/nice/jmx`
* :doc:`/devops/nice/thread_and_memory_dumps`

All pods sorted by memory usage::

    oc adm top pod --all-namespace --sort-by memory

Per node usage::

    oc adm top node


Graphs
^^^^^^


=============== ===================== ===================================================
 Link             Short Description         Full Description
=============== ===================== ===================================================
 `graph 1`_       Total memory         Memory of all worker nodes (=nodes that run apps)

                                       Metrics:

                                       * Available memory
                                       * Available memory with redundancy [#f1]_
                                       * Requested memory [#f2]_
                                       * Used memory

 `graph 2`_      Used vs requested     Fraction of requested memory used.

                                       Expected to be between 0.8 and 0.9 generally. May
                                       be lower after maintenance as Nice requires
                                       less memory after a restart.

                                       Metrics:

                                       * maximum
                                       * 0.95 quantile (=95th percentile)
                                       * median
                                       * 0.05 quantile
                                       * minimum

 `graph 3`_      Used vs requested     Same as *used vs requested* above but per Nice
                 per version           minor version.

                                       Note that Nice <2.27 used G1GC as garbage
                                       collector which did not uncommit (=return to OS)
                                       memory.

 `graph 4`_      Used vs requested     Same as *used vs requested* but only for Nice
                 >= v2.26              >= v2.26.

                                       v2.26 and later use ShanandoahGC.

 `graph 5`_      Used vs requested     Same as *used vs requested* but only for Nice
                 < v2.26               < v2.26.

                                       Before v2.26 G1GC was used.

 `graph 6`_      Used vs requested     Same as *used vs requested* but only for
                 production            production systems.

 `graph 7`_      Used vs requested     Same as *used vs requested* but only for
                 test                  test systems.

 `graph 10`_     Used vs request       Same as *used vs requested* but per
                 per installation      installation.

 `graph 8`_      Unused per node       Memory not in active use (but possibly requested)

 `graph 9`_      Available per node    Available memory per node. This is, memory that
                                       has not been requested [#f2]_ and may be used
                                       for newly created pods.
=============== ===================== ===================================================


.. rubric:: Footnotes

.. [#f1] One node is kept for redundancy in case of a node failure. This graph shows the available memory
         in case a single node failed.
.. [#f2] See `Assign Memory Resources to Containers and Pods`_


.. _graph 1: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=label_replace%28sum%28node_memory_MemTotal_bytes%7Binstance%3D%7E%27worker-.*%27%7D%29+-+sum%28node_memory_MemAvailable_bytes%7Binstance%3D%7E%27worker-.*%27%7D%29%2C+%22label%22%2C+%22used%22%2C+%22%22%2C+%22.*%22%29&query1=label_replace%28sum%28node_memory_MemTotal_bytes%7Binstance%3D%7E%27worker-.*%27%7D%29%2C+%22label%22%2C+%22available%22%2C+%22%22%2C+%22.*%22%29&query2=label_replace%28sum%28kube_pod_resource_request%7Bresource%3D%27memory%27%2C+node%3D%7E%27worker-.*%27%7D%29%2C+%22label%22%2C+%22requested%22%2C+%22%22%2C+%22.*%22%29&query3=label_replace%28sum%28node_memory_MemTotal_bytes%7Binstance%3D%7E%27worker-.*%27%7D%29+-+%2864+*+1024%5E3%29%2C+%22label%22%2C+%22usable+%28with+redundancy%29%22%2C+%22%22%2C+%22.*%22%29
.. _graph 2: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=%28sum%28node_memory_MemTotal_bytes%7Binstance%3D%7E%27worker-.*%27%7D%29+-+sum%28node_memory_MemAvailable_bytes%7Binstance%3D%7E%27worker-.*%27%7D%29%29+%2F+sum%28kube_pod_resource_request%7Bresource%3D%27memory%27%2C+node%3D%7E%27worker-.*%27%7D%29
.. _graph 3: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=avg+BY+%28label_version%29+%28+++%28sum+by+%28namespace%2Cpod%29%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2C+pod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%3D%7E%27.%2B%27%7D%29
.. _graph 4: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=quantile+%280.95%2C+%28sum+by+%28namespace%2Cpod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%21%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query1=quantile+%280.5%2C+%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%21%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query2=quantile+%280.05%2C+%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%21%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query3=max+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%21%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query4=min+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%21%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query5=
.. _graph 5: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=quantile+%280.95%2C+%28sum+by+%28namespace%2Cpod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%3D%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query1=quantile+%280.5%2C+%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%3D%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query2=quantile+%280.05%2C+%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%3D%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query3=max+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%3D%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query4=min+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_version%3D%7E%272%5C%5C.1.*%7C2%5C%5C.2%5B0-5%5D%27%7D%29&query5=
.. _graph 6: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=quantile+%280.95%2C+%28sum+by+%28namespace%2Cpod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27production%27%7D%29&query1=quantile+%280.5%2C+%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27production%27%7D%29&query2=quantile+%280.05%2C+%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27production%27%7D%29&query3=max+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27production%27%7D%29&query4=min+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27production%27%7D%29&query5=
.. _graph 7: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=quantile+%280.95%2C+%28sum+by+%28namespace%2Cpod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27test%27%7D%29&query1=quantile+%280.5%2C+%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27test%27%7D%29&query2=quantile+%280.05%2C+%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27test%27%7D%29&query3=max+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27test%27%7D%29&query4=min+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%29%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%2Clabel_env%3D%27test%27%7D%29&query5=
.. _graph 8: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=node_memory_MemAvailable_bytes%7Binstance%3D%7E%27worker-.*%27%7D
.. _graph 9: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=sum+BY+%28node%29+%28label_replace%28node_memory_MemTotal_bytes%7Binstance%3D%7E%27worker-.*%27%7D%2C+%22node%22%2C%22%241%22%2C%22instance%22%2C+%22%28.%2B%29%22%29%29+-+sum+BY+%28node%29+%28kube_pod_resource_request%7Bnode%3D%7E%27worker-.*%27%2Cresource%3D%27memory%27%7D%29
.. _graph 10: https://console.apps.openshift.tocco.ch/monitoring/query-browser?query0=avg+BY+%28namespace%29+%28%28sum+by+%28namespace%2C+pod%29+%28container_memory_working_set_bytes%7Bcontainer%3D%27%27%2Cpod%21%3D%22%22%7D%29%0A+++%2F+on+%28namespace%2C+pod%29+sum+by+%28namespace%2Cpod%29+%28kube_pod_resource_request%7Bresource%3D%27memory%27%2Cpod%21%3D%22%22%7D%29%0A+++*+on+%28namespace%2C+pod%29+group_right%28%29+kube_pod_labels%7Blabel_run%3D%22nice%22%7D%29%29

.. _Assign Memory Resources to Containers and Pods: https://kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource/
