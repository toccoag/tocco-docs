.. meta::
    :keywords: Docker image, Docker build, Docker
.. index:: Docker; build Nice2 image

Build Nice Docker Image
=======================

Build Docker Image
------------------

#. Change working directory

   .. parsed-literal::

      cd **${ROOT_DIR_OF_NICE2_REPOSITORY}**

#. Build a specific customer:

   .. parsed-literal::

      ./gradlew clean customer:\ **${CUSTOMER_NAME}**\ :build -P productionMode=true -x test

   .. note::

      See :ref:`gradle-build-production` when doing production builds.

#. Building the Docker image

    .. parsed-literal::

        cd customer/\ **${CUSTOMER_NAME}**
        docker build -t nice .

    The resulting image is called **nice**.

Deploy Docker Image
-------------------

#. Login

   .. code-block:: bash

       oc whoami -t | docker login registry.apps.openshift.tocco.ch -u any --password-stdin

#. Tag image

   This additionally tags the image named **nice** with the name *registry.apps.openshift.tocco.ch/…*.
   **nice** is the name of the image if you followed the instructions above.

   .. parsed-literal::

      docker tag **nice** registry.apps.openshift.tocco.ch/nice-\ **${INSTALLATION}**\ /nice

   .. tip::

      You can also directly build an image with the appropriate name::

          docker build -t ${TAG_NAME}

#. Deploy image

   .. parsed-literal::

      docker push registry.apps.openshift.tocco.ch/nice-\ **${INSTALLATION}**\ /nice

   Deployment is automatically triggered once image has been pushed.


Pull a Docker Image from VSHN's Registry
----------------------------------------

This is useful when you want to copy a Docker image from production
for local debugging.

#. Login

   .. code-block:: bash

       oc whoami -t | docker login registry.apps.openshift.tocco.ch -u any --password-stdin

#. Find Image

   .. parsed-literal::

      oc project nice-**${INSTALLATION_NAME}**
      oc get is
      NAME      DOCKER REPO                          TAGS      UPDATED
      nice      172.30.1.1:5000\ :red:`/nice-stn/nice`   :blue:`latest`    2 weeks ago

   .. hint::

      An ``oc describe …`` on :term:`RC`\ s and :term:`pod`\ s will also reveal the used images.

      :term:`Nginx` image can be found in the project ``shared-imagestreams``.

#. Pull image

   .. parsed-literal::

      docker pull registry.apps.openshift.tocco.ch\ :red:`/nice-stn/nice`::blue:`latest`


.. _docker-nice2-run:

Start Nice2
-----------

Start Nice2:

.. parsed-literal::

    docker run --rm --network=host **${DOCKER_IMAGE_TAG}** run

This will start Nice2 with the default options.

Should you require to set any additional options, like a custom DB server,
create a **${OPTIONS_FILE}** and tell docker about it:

.. parsed-literal::

    docker run --rm --network=host --env-file **${OPTIONS_FILE}** **${DOCKER_IMAGE_TAG}** run

Minimal **${OPTIONS_FILE}** example::

    # DB settings
    hibernate.main.serverName=localhost
    hibernate.main.user=USER
    hibernate.main.password=PASSWORD
    hibernate.main.databaseName=nice_master

.. tip::

    You can obtain the settings used for an installation on OpenShift like so:

    .. parsed-literal::

        oc project nice-\ ${INSTALLATION_NAME}
        oc set env -c nice dc/nice --list

    The output can be used, verbatim, in **${OPTIONS_FILE}**.


.. _docker-nice2-schema-upgrade:

Schema Upgrade
--------------

Run schema upgrade:

.. parsed-literal::

    docker run --rm --network=host **${DOCKER_IMAGE_TAG}** dbref

Configuration can be changes as described in `Start Nice2`.
