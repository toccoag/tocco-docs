.. index:: Nice2 debugging; logs, Kibana

Logs
####

Getting Started
===============

.. hint::

   If you're logging in for the first time see `First Login`_

#. Log in on https://kibana-openshift-logging.apps.openshift.tocco.ch.

#. Important predefined searches and dashboards:

   .. hint::

      Links only work for admins.

   ============ ============================= =====================================================
    Search       `Nice`_                       Nice applications logs
    Search       `Nice - OOM`_                 OutOfMemoryErrors
    Search       `Nice - DB failed connect`_   Failures to acquire connections.
    Search       `Nice - DB timeout`_          Exceeded DB timeout. Exceeded global, Postgres-wide
                                               timeout or timeout on REST API.
    Search       `Nginx`_                      Nginx HTTP request logs
    Dashboard    `Ops`_                        Operations main dashboard

    \*           \*                            `Saved Objects`_ lists all available searches,
                                               dashboards and virtualizations
   ============ ============================= =====================================================

   These searches come with predefined filters:

   .. figure:: logs/predefined_filters.png
      :scale: 80%

   Edit and enable filter *kubernetes.namespace_name: "nice-master"* filter
   to filter logs of a specific installation.

.. important::

   **Admin users** have two `tenants`_, *admin* and *Private*. In order to see any
   of the predefined objects (searches, dashboards, etc.), change to tenant
   *admin*. This will happen automatically if you follow any of the above
   links. This is not available for **non-admin users** and objects need
   to be imported as described in `First Login`_.

   While you are *admin* all changes to objects, including creating
   new objects, will be done globally **for all admin users**. Change to
   tenant *Private* to create personal objects.

   Technical details: :vshn:`TOCO-513`


Advanced Usage
==============

Select a Time Range
-------------------

.. figure:: logs/time.png
   :scale: 80%


Filter by Project / Installation
--------------------------------

.. figure:: logs/filter_install.png
   :scale: 80%

To search in all installations, you can use the following Query DSL:

.. code-block:: json

    { "query": { "prefix": { "kubernetes.namespace_name": { "value": "nice-" }}}}

Note the *Edit Query DSL* in the above screenshot. This filter is active by
default in the predefined searches listed in `Getting Started`_.


Filter by Container
-------------------

.. figure:: logs/filter_container.png
   :scale: 60%

Available containers for Nice:

============ ====================
 *nice*       Application logs
 *nginx*      Request logs
============ ====================

Predefined searches, see `Getting Started`_, already filter the
corresponding container.


View Surrounding Documents
--------------------------

Show logs directly before/after a message:

.. figure:: logs/surrounding_documents.png
   :scale: 80%


Query Syntax
------------

.. figure:: logs/query.png
   :scale: 80%

1. Search for **ModelException** in field *stack_trace*.

2. The word **ERROR** must be contained in the result. Without the **+**, the results matching closest the query are
   returned even if they don't contain all search terms.

3. Filter out all results that contain the word **runtime**.

4. Search for the phrase **some text** rather than the words **some** and **text**. You may also have to use quotes if
   the search term contains special characters. For instance, if it contains a hyphen, like **start-up**, it is treated
   as two words. Using ``"start-up"`` avoids this. In case you want to search a particular field, use
   ``message:"my search term"``.

More about `Lucene Query Syntax`_

Add Columns
-----------

.. figure:: logs/add_column.png
   :scale: 60%

You can use the panel on the left or the detail view to show more columns.


Filter by Fields
----------------

.. figure:: logs/filter_by_field.png
   :scale: 60%

By using the +/- magnifying glasses in the detail view, you can filter based on a field's value.


Nice Logs
---------

Searching for Similar Exceptions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: logs/stack_hash.png
   :scale: 80%

1. Open entry detail

2. Filter by ``structured.stack_hash`` [#f1]_


.. _kibana-first-login:

First Login
-----------

Check if you are an admin user:

.. figure:: logs/admin_tenant.png
   :scale: 60%

If yes, follow instructions in `Admin Users`_. Else follow
those in `Non-Admin Users`_.

Admin Users
^^^^^^^^^^^

On first login the index patterns need to be setup as follows:

#. Set *index pattern* to '*' (asterisk).

   .. figure:: logs/setup1.png
      :scale: 60%

#. Select '\@timestamp' as *Time Filter field name*
   and proceed to *Discover*.

   .. figure:: logs/setup2.png
      :scale: 60%

See also `Getting Started`_.


Non-Admin Users
^^^^^^^^^^^^^^^

#. Download :download:`kibana_objects.json <logs/kibana_objects.json>`

#. Import objects:

   .. figure:: logs/import.png
      :scale: 60%

.. rubric:: Footnotes

.. [#f1] `Details About Stack Hashes <https://github.com/logstash/logstash-logback-encoder/blob/master/stack-hash.md>`__.


.. _Nice: https://kibana-openshift-logging.apps.openshift.tocco.ch/app/kibana?security_tenant=admin#/discover/3d426ed0-eaef-11ec-a307-391a1055e287
.. _Nice - DB failed connect: https://kibana-openshift-logging.apps.openshift.tocco.ch/app/kibana?security_tenant=admin#/discover/cd91fb60-ebbf-11ec-a307-391a1055e287
.. _Nice - DB timeout: https://kibana-openshift-logging.apps.openshift.tocco.ch/app/kibana?security_tenant=admin#/discover/c147c5b0-ebbf-11ec-a307-391a1055e287
.. _Nice - OOM: https://kibana-openshift-logging.apps.openshift.tocco.ch/app/kibana?security_tenant=admin#/discover/fe0e09a0-ebba-11ec-a307-391a1055e287
.. _Nginx: https://kibana-openshift-logging.apps.openshift.tocco.ch/app/kibana?security_tenant=admin#/discover/d4839f60-eaec-11ec-a307-391a1055e287
.. _Ops: https://kibana-openshift-logging.apps.openshift.tocco.ch/app/kibana?security_tenant=admin#/dashboard/a4fdb740-ebbd-11ec-a307-391a1055e287
.. _Kibana User Guide: https://www.elastic.co/guide/en/kibana/current/index.html
.. _Discover: https://www.elastic.co/guide/en/kibana/current/discover.html
.. _Lucene Query Syntax: https://www.elastic.co/guide/en/elasticsearch/reference/5.5/query-dsl-query-string-query.html#query-string-syntax
.. _Saved Objects: https://kibana-openshift-logging.apps.openshift.tocco.ch/app/kibana#/management/kibana/objects
.. _tenants: https://kibana-openshift-logging.apps.openshift.tocco.ch/app/security-multitenancy
