.. index:: DNS; website/route

DNS (Nice)
##########

.. highlight:: ini

.. todo::

    * describe how to add new domains (glue entries, etc.)


.. _dns-add-dns-records:

Add DNS Records
===============

Finding the Right Instructions
------------------------------

.. hint::

    When adding domain for Nice, see also :ref:`ansible-add-ingress`.

.. list-table::
   :header-rows: 1
   :widths: 10 20 20

   * - Pattern
     - Examples
     - Instructions
   * - | {installation_name}.\ **tocco.ch**
       | {service_name}.\ **tocco.ch**
       | (only subdomains of **tocco.ch**)
     - | dgh.tocco.ch
       | dghtest.tocco.ch
     - :ref:`dns-installation.tocco.ch`
   * - {domain}.{tld}
     - | example.ch
       | example.net
     - :ref:`dns-root-domain`
   * - **www**\ .{domain}.{tld}
     - | **www**\ .example.net
       | **www**\ .example.ch
     - :ref:`dns-special-case-www`
   * - {subdomain}.{domain}.{tld}
     - | admin.example.net
       | intranet.example.net
       | :strike:`www.example.net`
     - :ref:`dns-subdomain`


.. _dns-installation.tocco.ch:

${INSTALLATION_NAME}.tocco.ch
-----------------------------

Every installation has an record for ${INSTALLATION_NAME}.tocco.ch::

    ; 'exampleinc' is the installation's name
    exampleinc.tocco.ch.      3600 IN CNAME  os4.tocco.ch.

Instructions:

#. Go to `domain tocco.ch`_ (password in :term:`secrets2.yml`) (link only works when logged in to *tocco* account)
#. Remove any existing A, AAAA, ALIAS, ANAME and CNAME
   record for ${INSTALLATION_NAME}.tocco.ch.
#. Create record:

   .. figure:: dns_static/installation.tocco.ch.png
#. `Verify DNS Records`_


.. _dns-root-domain:

Root Domain (example.net)
-------------------------

For root domains, an ALIAS is to be used::

    example.net           3600 IN ALIAS   exampleinc.tocco.ch.
                                    ;     ^^^^^^^^^^^^^^^^^^^
                                    ; ${INSTALLATION_NAME}.tocco.ch

Instructions:

#. `Find out if Domain is Managed by Us`_
#. For domains managed by us, execute the following
   instructions in `Nine's Cockpit`_. Password in
   :term:`secrets2.yml`.

   For domains managed by the customer, or another third party,
   forward the following instructions for removing/adding
   DNS records to the customer.
#. Remove any existing A, AAAA, ANAME and ALIAS record
   for the domain. Do not touch subdomains such as,
   www.example.net or some-name.example.net.
#. Create record:

   .. figure:: dns_static/root_domain.png

      Example user interface. Other interfaces may
      look significantly different.
#. `Verify DNS Records`_

.. note::

    **What if type ALIAS is not available?**

    Type ALIAS is sometimes known as ANAME::

        example.net    3600 IN ANAME   exampleinc.tocco.ch.
                                 ;     ^^^^^^^^^^^^^^^^^^^
                                 ; ${INSTALLATION_NAME}.tocco.ch

    Iff ALIAS and ANAME aren't available, create both of the
    following records instead::

        example.net.     3600 IN A      5.102.151.37
        example.net.     3600 IN AAAA   2a06:c01::54a7


.. _dns-special-case-www:

Special Case *www*
------------------

*www*\ .some-domain.tld should point to the root domain::

    www.example.net       3600 IN CNAME   example.net.
        ^^^^^^^^^^^                       ^^^^^^^^^^^
      ; root domain                       root domain

Instructions:

#. `Find out if Domain is Managed by Us`_
#. For domains managed by us, execute the following
   instructions in `Nine's Cockpit`_. Password in
   :term:`secrets2.yml`.

   For domains managed by the customer, or another third party,
   forward the following instructions for removing/adding
   DNS records to the customer.
#. Remove any existing A, AAAA, ANAME, ALIAS or CNAME record
   for the www subdomain. Do not touch the domain (e.g example.net)
   or any other subdomains (e.g. some-domain.example.net).
#. Create record:

   .. figure:: dns_static/www_subdomain.png

      Example user interface. Other interfaces may
      look significantly different.
#. `Verify DNS Records`_


.. _dns-subdomain:

Subdomain (some-name.example.net)
----------------------------------

Subdomains, with the exception of *www*, should point to
${INSTALLATION}.tocco.ch::

    intranet.example.net  3600 IN CNAME   exampleinc.tocco.ch.
                                    ;     ^^^^^^^^^^^^^^^^^^^
                                    ; ${INSTALLATION_NAME}.tocco.ch

Instructions:

#. `Find out if Domain is Managed by Us`_
#. For domains managed by us, execute the following
   instructions in `Nine's Cockpit`_.Password in
   :term:`secrets2.yml`.

   For domains managed by the customer, or another third party,
   forward the following instructions for removing/adding
   DNS records to the customer.
#. Remove any existing A, AAAA, ANAME, ALIAS or CNAME record
   for the subdomain. Do not touch the domain (e.g example.net)
   or any other subdomains (e.g. another-subdomain.example.net).
#. Create record:

   .. figure:: dns_static/some_subdomain.png

      Example user interface. Other interfaces may
      look significantly different.
#. `Verify DNS Records`_


Find out if Domain is Managed by Us
===================================

Obtain a list of DNS server for the domain via terminal:

    .. parsed-literal::

        dig -t ns **${DOMAIN}}**

    Or do an `NS lookup`_ online.

    If the result contains the name servers ``ns1.tocco.ch``
    and ``ns2.tocco.ch``, the domain is managed by us.
    Otherwise, it is not.

    .. hint::

        **${DOMAIN}** is the domain part only, e.g. tocco.ch and
        not :strike:`www.tocco.ch` or :strike:`cockpit.tocco.ch`.

.. _dns-verify-dns-records:

Verify DNS Records
==================

Get **A** records for FQDN via terminal:

    .. parsed-literal::

        dig **${FQDN}**

Or do an `A lookup`_ online.

.. hint::

    **${FQDN}** is the full domain name, like tocco.ch,
    www.tocco.ch or admin.tocco.ch.

Verify output:

    The ``ANSWER SECTION`` of the output must contain the
    following **A** entries::

        ... IN A 5.102.151.37

    The output may also contain ``CNAME`` entries. However,
    it **must not** contain any other **A** entries. If it
    does, they must be removed.

Get **AAAA** records for FQDN via terminal:

    .. parsed-literal::

        dig -t aaaa **${FQDN}**

Or do a `AAAA lookup`_ online.

Verify output:

    The ``ANSWER SECTION`` of the output must contain the
    following **AAAA** entries::

        ... IN AAAA 2a06:c01::54a7

    The output may also contain ``CNAME`` entries. However,
    it **must not** contain any other **AAAA** entries. If it
    does, they must be removed.


Technical Notes
===============

Why use CNAME Pointing to ${INSTALLATION_NAME}.tocco.ch for Other Subdomains?
-----------------------------------------------------------------------------

Having all records point to ${INSTALLATION_NAME}.tocco.ch means that
we only need to change this single record to have all records changed
for a single installation.

Example::

    extranet.example.net. IN CNAME example.tocco.ch.
    example.tocco.ch.     IN CNAME os4.tocco.ch.
    example.tocco.ch.     IN A 5.102.151.37

In this example, **only** *example.tocco.ch.* needs to be adjusted
when moving the installation to another platform.

Additionally, many domains are managed by third parties. Having
*A*/*AAAA* records would mean communicating all changes to the
customer rather than being able to change them at our discretion.

Why use CNAME Pointing to Root Domain for *www* and it Only?
------------------------------------------------------------

The root domain, e.g. example.net, and the www subdomain,
e.g. www.example.net, are inherently linked and the two
are always expected to point to the same page in today's
internet. Hence, having the two "glued" together via CNAME
is but logical.

When it comes to other subdomains the situation looks
different. Let's consider the following subdomain:

.. parsed-literal::

    ; this is an example of how not to do it
    intranet.example.net.  IN CNAME  :strike:`example.net`.

For these two domains, *intranet.example.net* and
*example.net*, it's very much a possibility that only
the root domain, *example.net*, is moved without the
*intranet.example.net* subdomain. However, because of the
CNAME, *intranet.example.net* would be moved inadvertently
too.  There is no way for us to find out whether a root domain
is used as CNAME target for domains in the hand of third
parties.

Why use ALIAS/ANAME for Domains?
--------------------------------

The reasoning is the same as for subdomains. The reason
*CNAME* isn't used here is that the DNS protocol doesn't
allow it on root domain level. See `Why use CNAME Pointing to
${INSTALLATION_NAME}.tocco.ch for Other Subdomains?`_

ALIAS remains completely unspecified but an attempt has been
made to `standardize ANAME`_. However, this draft has expired
and current implementations very greatly.

How to detect the use of ALIAS/ANAME?
-------------------------------------

Due to the lack of specification, there is no definite way
to determine whether a server is using *A*/*AAAA* records
or *ALIAS*/*ANAME*. Yet, this may be necessary to
determine whether a DNS record needs to be changed.

One method is trying to see if the TTL on authoritative
server decreases over time:

    First, find an authoritative server:

    .. parsed-literal::

        $ dig -t ns +short **tocco.ch**
        **ns1.tocco.ch.**
        ns2.tocco.ch.

    Resolve *A* record twice with some delay:

    .. parsed-literal::

        $ for _ in a b; do
            dig +noall +answer +norecurse @\ **ns1.tocco.ch** **tocco.ch**;
            echo
            sleep 2
          done
        tocco.ch.           **4140**  IN  A     5.102.151.37

        tocco.ch.           **4138**  IN  A     5.102.151.37

    The decreasing TTL is a strong indication that ALIAS/ANAME
    is in effect.

Another possibility is to adjust the suspected target of an ALIAS/ANAME:

    Let's assume the following records existed::

        example.net.        3600  IN  ANAME  os4.tocco.ch.
        os4.tocco.ch.       3600  IN  A      5.102.151.37

    where, of course, we'd only see::

        os4.tocco.ch.  3600  IN  A           5.102.151.37

    In order to find out if there indeed is an ALIAS/ANAME in place,
    the record for *os4.tocco.ch* could be altered:

    We could remove one of the records (though be it rather dangerous)::

        os4.tocco.ch.  3600  IN  A           5.102.151.37

    Or temporarily add one::

        os4.tocco.ch.  3600  IN  A           5.102.151.37
        os4.tocco.ch.  3600  IN  A           1.2.3.4

    Of course, we'd have to make sure 1.2.3.4 returns a response
    clients handle well.

    Keep in mind possible caching as result TTLs.


.. _domain tocco.ch: https://cockpit.nine.ch/en/dns/unmanaged/domains/2156
.. _Nine's cockpit: https://cockpit.nine.ch/en/dns/domains
.. _NS lookup: https://toolbox.googleapps.com/apps/dig/#NS/tocco.ch
.. _A lookup: https://toolbox.googleapps.com/apps/dig/#A/tocco.ch
.. _AAAA lookup: https://toolbox.googleapps.com/apps/dig/#AAAA/tocco.ch
.. _standardize ANAME: https://datatracker.ietf.org/doc/html/draft-ietf-dnsop-aname
