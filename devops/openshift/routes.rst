.. index:: OpenShift; routes, OpenShift; ingress

Routes / Ingresses / Hostnames
==============================

Show Ingresses
--------------

.. code::

   $ oc get ingress
   NAME                       CLASS    HOSTS                 ADDRESS                                  PORTS     AGE
   nice                       <none>   tocco.tocco.ch        router-default.apps.openshift.tocco.ch   80, 443   100d
   nice-backoffice.tocco.ch   <none>   backoffice.tocco.ch   router-default.apps.openshift.tocco.ch   80, 443   100d
   nice-cockpit.tocco.ch      <none>   cockpit.tocco.ch      router-default.apps.openshift.tocco.ch   80, 443   100d
   nice-extranet.tocco.ch     <none>   extranet.tocco.ch     router-default.apps.openshift.tocco.ch   80, 443   100d
   nice-tocco.ch              <none>   tocco.ch              router-default.apps.openshift.tocco.ch   80, 443   100d
   nice-www.tocco.ch          <none>   www.tocco.ch          router-default.apps.openshift.tocco.ch   80, 443   100d


.. _ssl-certificates:

SSL Certificates
----------------

.. _ssl-cert-issuance:

Issuance
^^^^^^^^

.. admonition:: Deprecated
   :class: deprecated

   This is deprecated for use with Tocco but may still be used to enable
   SSL for other applications.

   To setup SSL for Tocco see :ref:`ansible-add-ingress`.

SSL certificates are issued automatically for ingresses  with an appropriate annotation.

#. Configure :doc:`dns`

#. Obtain the name of the ingress (**${INGRESS}**)::

       oc get ingress 

#. Add the annotation:

   .. parsed-literal::

       oc annotate ingress/**${INGRESS}** cert-manager.io/cluster-issuer=letsencrypt-production \
           cert-manager.io/private-key-rotation-policy=Always


.. _acme-troubleshooting:

Troubleshooting
^^^^^^^^^^^^^^^

.. hint::

   Expect certificate issuance to take up to 15 minutes.

Check for missing TLS certificates in the OpenShift project::

    $ oc project nice-${INSTALLATION}
    $ oc get certificates
    NAME                      READY   SECRET                    AGE
    tls-backoffice.tocco.ch   True    tls-backoffice.tocco.ch   100d
    tls-cockpit.tocco.ch      True    tls-cockpit.tocco.ch      100d
    tls-extranet.tocco.ch     True    tls-extranet.tocco.ch     100d
    tls-tocco.ch              True    tls-tocco.ch              100d
    tls-tocco.tocco.ch        True    tls-tocco.tocco.ch        100d
    tls-www.tocco.ch          True    tls-www.tocco.ch          100d

Note the *READY* column.

Show issuance details::

    $ oc get certificate ${CERTIFICATE_NAME}
    …
    Status:
      Conditions:
        Last Transition Time:  2022-07-22T05:24:39Z
        Message:               Certificate is up to date and has not expired
        Observed Generation:   2
        Reason:                Ready
        Status:                True
        Type:                  Ready
      Not After:               2022-10-20T04:24:37Z
      Not Before:              2022-07-22T04:24:38Z
      Renewal Time:            2022-09-20T04:24:37Z
      Revision:                1
    Events:
      Type    Reason     Age   From                                       Message
      ----    ------     ----  ----                                       -------
      Normal  Issuing    137m  cert-manager-certificates-trigger          Issuing certificate as Secret does not exist
      Normal  Generated  137m  cert-manager-certificates-key-manager      Stored new private key in temporary Secret resource "tls-toccotestnew.tocco.ch-hm7xq"
      Normal  Requested  137m  cert-manager-certificates-request-manager  Created new CertificateRequest resource "tls-toccotestnew.tocco.ch-cmz68"
      Normal  Issuing    137m  cert-manager-certificates-issuing          The certificate has been successfully issued

Issuance status and possible issues should be listed in the *Status*/*Events* sections.


See also:

* `Troubleshooting Issuing ACME Certificates`_


.. _Troubleshooting Issuing ACME Certificates: https://cert-manager.io/docs/faq/acme/#2-troubleshooting-orders
