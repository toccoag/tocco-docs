Persistent Volumes
------------------

In some cases it is necessary to add custom, persistent volumes.


Caveats
```````

Only *ReadWriteOnce* volumes are available. That is, only a single pod may have read or write access
to a volume. As result, the deployment strategy needs to be adjusted in the *deploymentconfig* (or *deployment*) to *Recreate*:

.. code-block:: yaml

   spec:
     strategy:
       type: Recreate
     recreateParams:  # this is used rather than rollingParams with Recreate
       # …

With the *recreate* strategy, the old pod is stopped before starting the new pod. Thus,
the application will be offline during deployment. However, it's avoided that both pods
need access to the volume concurrently.

Technical note:

The above description of *ReadWriteOnce* isn't fully accurate and technically read/write
access is limited to a single node. It may be possible to use `inter-pod affinity`_ to schedule
pods onto the same node in some cases to allow access to a volume by multiple pods.

See also:

* `Using deployment strategies`_


.. _persistent-volume-creation:

Creating a Persistent Volume
````````````````````````````

.. warning::

   Read `Caveats`_ first.

This creates a :term:`PVC` of size 1 GiB called ``cms`` which is mounted in the ``nice`` container at ``/app/var/cms``.

.. code::

    oc set volume dc/nice -c nice --add --name=cms --claim-name=cms --claim-size=1G --mount-path=/app/var/cms

.. hint::

   It's also possible to request cheaper and slower bulk storage via ``--claim-class=bulk``. Minimum
   size for bulk storage is 100 GiB.

   Show available classes: ``oc get storageclass``

You can list the PVCs using ``oc get pvc`` and you'll see the mounted volumes in the deployment config
using ``oc describe dc ${POD}``, section *Mount*.


Populating a Persistent Volume
``````````````````````````````

Here is how you copy the directory ``cms`` on your machine into a volume located at ``/var/app/cms``.


#. Find a running pod (a nice pod in this example)

   .. parsed-literal::

        # find a running pod (a nice pod in this case)
        $ oc get pods -l run=nice
        NAME             READY     STATUS    RESTARTS   AGE
        **nice-169-v2vsx**   2/2       Running   0          11m

#. Now, copy the content into the volume within that pod

   .. parsed-literal::

        oc cp -c nice cms **nice-169-v2vsx**:/app/var/cms


Resizing a Persistent volume
````````````````````````````

#. Show volumes::

       oc get persistentvolumeclaim

#. Resize volume::

       oc edit persistentvolumeclaim ${name}

   and edit the size:

   .. code-block:: yaml

       spec:
         resources:
           requests:
             storage: ${size}   # <-- e.g 15Gi


.. _persistent-volume-removal:

Removing a Persistent Volume
````````````````````````````

First remove the volume from the container. Then, remove the actual :term:`PVC`.

.. code::

        oc set volume dc/nice -c nice --remove --name=cms
        oc delete pvc cms


.. _inter-pod affinity: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity
.. _Using deployment strategies: https://docs.okd.io/latest/applications/deployments/deployment-strategies.html
