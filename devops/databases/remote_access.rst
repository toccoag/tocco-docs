Postgres Remote Access
======================

Using SSH Port Forwarding
-------------------------

Forward Port
````````````

.. note::

    This requires ssh access to the DB server.

.. parsed-literal::

    ssh ${USERNAME}@db1.tocco.cust.vshn.net -L 5432:localhost:5432 -N

.. hint::

    ``${USERNAME}`` consists of ``first_name.last_name`` (e.g. ``jane.doe``)

Now you should be able to connect to the DB server on **locahost:5432**.


Connect Nice to remote DB
`````````````````````````

Once port forwarding is established, you can tell Nice to directly connect to a DB on the remote server.

Obtain credentials:

    .. parsed-literal::

        $ oc project nice-**${INSTALLATION}**
        $ oc set env --list dc/nice \|grep "^hibernate\\.main\\."
        hibernate.main.databaseName=\ :green:`nice_tocco`
        hibernate.main.password=\ :red:`************`
        hibernate.main.serverName=db1.tocco.cust.vshn.net
        hibernate.main.user=\ :blue:`nice_tocco`
        hibernate.main.sslMode=require


    Copy the necessary properties (colored) from above.

    Create or alter ``customer/${CUSTOMER}/etc/application-development.properties``:

    .. parsed-literal::

        hibernate.main.serverName=localhost
        hibernate.main.databaseName=\ :green:`nice_tocco`
        hibernate.main.password=\ :red:`************`
        hibernate.main.user=\ :blue:`nice_tocco`


Direct Access
-------------

.. note::

    Direct access is only possible from whitelisted addresses.

.. important::

    Postgres doesn't enforce SSL by default, you **must** enable it. Take a look at `libpq - SSL Support`_ for more
    details.


Using PSQL
``````````

.. parsed-literal::

    psql 'postgresql://**${USER}**\ @db1.tocco.cust.vshn.net/**${DB_NAME}**?sslmode=verify-full&sslrootcert=\ **${CERT}**'

See `Certificates`_ for ``${CERT}``


Using Python
````````````

.. code-block:: python3

    import psycopg2

    conn = psycopg2.connect(
        host = "db1.tocco.cust.vshn.net",
        database = DB_NAME,
        user = USER,
        password = PASSWORD,
        sslmode = "verify-full",
        sslrootcert = CERT
    )

See `Certificates`_ for ``CERT``


Other Means of Accessing Postgres
`````````````````````````````````

There are many more libraries and tools that allow you to access a Postgres DB server. But be aware that Postgres doesn't
enable SSL verification by default, **you must make sure SSL certificates are verified!**  Take a look at
`libpq - SSL Support`_, most tools and libraries based on libpg. Thus, most of them use the same SSL settings.


Certificates
````````````

TLS certificates can be found in :ansible-repo:`roles/tocco/vars/certs.yml`.


.. _libpq - SSL Support: https://www.postgresql.org/docs/current/static/libpq-ssl.html
