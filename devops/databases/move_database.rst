Move Database between Servers
=============================

Checklist for moving DBs between clusters.

Pre-Move
========

* (prod-only) Announce downtime ~one week in advance.

* Check disk space::

      $ tco db-size ${installation}
      $ ssh ${target_server} df -h /var/lib/postgresql/

* (unannounced / test-only) check for users::

      $ tco sessions ${installation}

  If anyone is using the installation, consider postponing the
  move. Use ``tco sessions -v`` to see additional details.

Move
====

* Update ``db_server`` in :term:`config.yml` and create new
  DBs and user:

  .. parsed-literal::

     ansible-playbook playbook.yml -l **${INSTALLATION}** -t postgres

* Start maintenance page:

  tocco-mntnc --auto-scale start

  **This will stop the installation.**

  See :doc:`/devops/maintenance/maintenance_page`

* (prod-only) Initialize history DB::

      copy_history_db --init-only ${source_host} ${source_db} ${target_host} ${target_db}

* Move main DB::

      tco cp <source_host>/<source_db> <target_host>/<target_db>

* Rename DB on old server::

      $ tco db -t postgres ${installation}
      $ ALTER DATABASE nice_<installation> RENAME TO nice_<installation>_moved;

  .. note::

     Safety mesure to ensure the old DB cannot be written to accidentally.

* Update OpenShift config::

     $ ansible-playbook playbook.yml --skip-tags ingress

  .. note::

     Skipping ingress to prevent reverting changes made by ``tocco-mntnc``.

* Verify deployment succeeded::

      $ oc project nice-${installation}
      $ oc get pods

* Stop maintenance page::

     tocco-mntnc --auto-scale stop

* Verify installation is back online


Post-Move
=========

* (prod-only) On the **next day** complete move history DB::

      copy_history_db --skip-init ${source_host} ${source_db} ${target_host} ${target_db}

  .. note::

     Do not do this overnight as we run into issues before. Do not bother copying DB for
     test systems unless explicitly requested.

* Remove old DBs **four days** later::

      tco dbs unused --ask-delete
