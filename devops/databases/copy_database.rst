Copy/Dump/Restore Database
==========================

.. hint::

        If you want restore a backup have a look at :doc:`../backups/database`.


.. index:: DB; copy
.. _copy-db:

Copy Database
-------------

.. hint::

    * For the special host ``"localhost"`` to work properly, Postgres
      must be set up as described in :ref:`setup-postgres`.

**Examples:**

a) Copy DB of installations *abc* to server where *abctest* resides
   naming the new db *nice_abctest_new*::

       tco cp abc abctest/nice_abctest_new

b) Copy prod DB of *bbg* to *localhost*::

       tco cp bbg

   *localhost* is default if no target is specified.

See ``tco --help`` for more examples and additional information.

.. index:: DB; dump

Dump Database
-------------

.. code-block:: bash

    pg_dump -Fc -f ~/_to_delete/nice2_${CUSTOMER}_$(date +"%Y_%m_%d").psql ${DATABASE};


.. index:: DB; restore
.. _restore-database:

Restore Database
----------------

Restore dump:

#. Create an empty database:

   .. parsed-literal::

       CREATE DATABASE **${DB_NAME}** WITH OWNER **${DB_USER}**;

#. Restore database

   Regular \*.psql/\*.dump files and directories:

   .. parsed-literal::

        pg_restore -j 4 --role ${DB_USER} --no-owner --no-acl -d ${DB_NAME} ${DUMP_FILE_OR_DIRECTORY}

   \*.zstd files:

   .. parsed-literal::

        zstd -qcd ${DUMP_FILE} | pg_restore --role ${DB_USER} --no-owner --no-acl -d ${DB_NAME}

#. run ANALYZE [#f1]_::

       ANALYZE

.. note::

   If needed, adjust owner of DB and content:

   .. parsed-literal::

        REASSIGN OWNED BY **${OLD_DB_USER}** TO **${NEW_DB_USER}**;

   .. warning::

      ``REASSIGN OWNED`` will change the owner of all objects in the DB connected to
      matching ``BY ${ROLE_NAME}`` and the **owner of all DBs** matching. Be careful when
      there are other DBs that have ``${ROLE_NAME}`` as owner!


.. index:: DB; copy

Copy database using WITH TEMPLATE
---------------------------------

This is the fastest way to copy a database. Alternatively, you can dump and then restore the database.

.. parsed-literal::

    CREATE DATABASE **${TARGET_DB}** WITH TEMPLATE **${SOURCE_DB}**;

.. warning::

    This requires that no one is connected to the database. Consequently, it isn't possible to copy a database of
    a running system.

Example
^^^^^^^

#. switch to the right project

    .. code-block:: bash

        oc project nice-${INSTALLATION}

#. check how many instance are running

    .. code-block:: bash

        oc get dc/nice -o go-template='{{.spec.replicas}}{{"\n"}}'

#. stop instance (if required)

    .. code-block:: bash

        oc scale --replicas=0 dc/nice

#. copy database

    .. parsed-literal:: sql

        CREATE DATABASE **${NAME_OF_DB_COPY}** WITH TEMPLATE **${SOURCE_DB_NAME}**;

    .. hint::

        If you get "source database '…' is being accessed by other users", try :ref:`killing the connections to the
        database <force-close-db-connection>` first.

    .. note::

        By convention, databases not used by a test or production systems should follow this naming pattern:
        ``nice_${CUSTOMER}_${YOUR_SHORT_NAME}_${YEAR}${MONTH}${DAY}``

5. restart instances (if previously stopped)

    .. parsed-literal::

        oc scale --replicas=\ **${N}** dc/nice

    Start **${N}** instances.


.. rubric:: Footnotes

.. [#f1] ANALYZE ensures internal statistics used by the query planner are updated. Without it,
         it's possible that queries are much slower, particularly, on large tables.

         See also `ANALYZE`_ documentation upstream.


.. _ANALYZE: https://www.postgresql.org/docs/current/sql-analyze.html
.. _tocco_hieradata repository: https://git.vshn.net/tocco/tocco_hieradata/-/blob/master/database/
