.. index:: DB; connect

###################
Connect to Database
###################

.. _connect-to-db-via-openshift:

Option a) via OpenShift
=======================

Connect to the database used by the current project::

    $ oc rsh -c nice dc/nice psql
    psql (9.6.20, server 9.5.24)

    nice_master=>


.. note::

   Use option b) if you require superuser privileges.


Option b) via ssh
=================

#. Identify DB server:

   Look for *db_server*  in :term:`config.yml`. This
   is the DB server used by an installation.

#. ssh into machine::

       $ ssh ${DB_SERVER}

#. connect to Postgres::

       $ psql ${db_name}

   Alternatively, if you don't know the DB name, connect to DB `postgres`::

       $ psql postgres
