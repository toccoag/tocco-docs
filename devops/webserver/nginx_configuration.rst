.. index:: Nginx

Nginx Configuration
===================

This document describes the available configuration for the `Docker image used on OpenShift <https://gitlab.com/toccoag/openshift-nginx>`__.

Log Levels
----------

The env. variable ``NGINX_LOG_LEVEL`` allows picking from three levels:


=============  ===================================================================================
 Level          Description
=============  ===================================================================================
 ``default``    Some requests are not logged like request fonts, css, icons, etc. Failed requests
                (=code >=300) are logged unconditionally.
 ``verbose``    All requests but requests to status pages are logged.
 ``all``        All requests are logged.
=============  ===================================================================================


.. index:: Nginx; HTTP headers

Custom HTTP Headers
-------------------

Custom HTTP headers can be configured using ``NGINX_HEADER_*`` env. variables.

Set header for responses with **2XX status codes**::

    NGINX_HEADER_Content-Security-Policy="default-src 'none'; block-all-mixed-content; connect-src 'self' example.net"

This creates a header called ``Content-Security-Policy`` with the value ``default-src 'none'; block-all-mixed-content; connect-src 'self' example.net``.

Set header on **all** responses regardless of status code::

    oc set env -c nginx dc/nice NGINX_ALWAYS_HEADER_Strict-Transport-Security='max-age=62208000'

This creates a header called ``Strict-Transport-Security`` with the value ``max-age=62208000``.
