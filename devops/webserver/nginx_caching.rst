.. index::
   pair: Nginx; caching

Nginx Caching
=============

What data is cached?
--------------------

Nginx uses the ``Cache-Control``, ``Expires`` and ``Pragma`` headers (in that order) to determine if and for how long a
resource may be cached.

The `HTTP/1.1 standard`_ pretty clearly explains when an entry may be cached. Our setup adheres to this standard.

.. _HTTP/1.1 standard: https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.9.1


How do I see if a resource was cached by Nginx?
-----------------------------------------------

Search for the ``X-Cache`` header on the response. (See screenshot below.)

You'll usually see one of these values [#f1]_:

============= ==================================================================
 Value        Description
============= ==================================================================
 HIT           Content has been served from cache.
 MISS          Content has not been served from cache.
 REVALIDATED   Content has been served from cache after revalidating with Nice.
               Nice returned code 304 — not modified — as result of
               `If-Modified-Since`_ or `If-Match`_ being included in the
               request.
============= ==================================================================

.. figure:: resources/nginx_x_cache.png

    **Network** tab in **Inspector**


Bypassing Cache
===============

Curl can be used to bypass the cache and fetch a resource from Nice directly.

Example fetching https\://www.tocco.ch/example from Nice2 directly::

    oc project ${PROJECT}
    oc exec -c nginx dc/nice -- curl -vSL -H 'Host: www.tocco.ch' -H 'X-Forwarded-Proto: https' http://localhost:8080/example

Nice is reachable at localhost:8080 while Nginx is on localhost:8081.

HTTP requests are redirected to their HTTPS equivalent by OpenShift and
Nginx will never see such requests.


View Cached Resource
====================

Cache is stored in */var/cache/nginx/proxy_cache*. You can use grep to find the cache item
belonging to https\://www.tocco.ch/example like this::

    oc project ${PROJECT}
    # Note the omission of '//'
    oc exec -c nginx dc/nice -- grep -Pr 'https:www.tocco.ch/example'

Fetch cache file::

    oc exec -c nginx dc/nice -- cat ${PATH}


.. rubric:: Footnotes

.. [#f1] This value corresponds to the Nginx variable `$upstream_cache_status`_

.. _$upstream_cache_status: https://nginx.org/en/docs/http/ngx_http_upstream_module.html#var_upstream_cache_status
.. _If-Modified-Since: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/If-Modified-Since
.. _If-Match: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/If-Match
