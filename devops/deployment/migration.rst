.. meta::
   :keywords: drittinstanz

.. index:: migration, instructions

Migration
=========

.. tip::

    See :doc:`intro` for a simpler introduction to migrations.

.. sidebar:: Legend

   .. graphviz::
   
       digraph {
           rankdir=LR
   
           db1 [ shape=point, color=invis, label="" ]
           db2 [ shape=point, color=invis, label="" ]
           db3 [ shape=none, label="copy/rename DB" ]
           depl1 [ shape=point, color=invis, label="" ]
           depl2 [ shape=point, color=invis, label="" ]
           depl3 [ shape=none, label="possible deployment\n(outside migration)" ]
           migration1 [ shape=point, color=invis, label="" ]
           migration2 [ shape=point, color=invis, label="" ]
           migration3 [ shape=none, label="deployment / migration" ]
           removed1 [ label="", style=dotted ]
           removed2 [ shape=none, label="" ]
           removed3 [ shape=none, label="removed installation" ]
           created1 [ label="", color=green ]
           created2 [ shape=none, label="" ]
           created3 [ shape=none, label="created installation" ]
           install1 [ label="" ]
           install2 [ shape=none, label="" ]
           install3 [ shape=none, label="installation" ]
           branch1 [ label="", shape="hexagon" ]
           branch2 [ shape=none, label="" ]
           branch3 [ shape=none, label="Git branch" ]
   
           db1 -> db2 [ color=blue ]
           db2 -> db3 [ style=invis ]
           depl1 -> depl2 [ style=dotted ]
           depl2 -> depl3 [ style=invis ]
           migration1 -> migration2 [ color=red, fontcolor=red ]
           migration2 -> migration3 [ style=invis ]
           removed1 -> removed2 [ style=invis ]
           removed2 -> removed3 [ style=invis ]
           created1 -> created2 [ style=invis ]
           created2 -> created3 [ style=invis ]
           install1 -> install2 [ style=invis ]
           install2 -> install3 [ style=invis ]
           branch1 -> branch2 [ style=invis ]
           branch2 -> branch3 [ style=invis ]
       }

This document describes the DevOps part of updating from one major or minor
version to another.


Pre-Migration / Non-Migration State
-----------------------------------

When no migration is in progress, there is exatly one *production* and
one *test* system.

.. graphviz::

    digraph {
        label="Normal Operations (v3.0)"

        subgraph cluster_git {
            label="Git repository"
            r300 [ label="releases/3.0", shape="hexagon" ]
        }
        prod [ label="prod (v3.0)" ]
        test [ label="test (v3.0)" ]

        r300 -> test [ style=dotted ]
        test -> prod [ style=dotted ]
    }

|

.. _test-migration:

Migration of Test
-----------------

First, a *testnew* system is created and upgraded. During these phase
hotfixes can still be deployed independently by deploying them to *test*
first and then *production*.

.. graphviz::

    digraph {
        label="Migration of Test System (v3.0 → v3.5)"

        subgraph cluster_git {
            label="Git repository"
            r300 [ label="releases/3.0", shape="hexagon" ]
            r305 [ label="releases/3.5", shape="hexagon" ]
        }
        prod [ label="prod (v3.0)" ]
        test [ label="test (v3.0)" ]
        testnew [ label="testnew (v3.0 → v3.5)", color=green ]

        { rank=same test testnew }

        r300 -> test [ style=dotted ]
        r305 -> testnew [ color=red, fontcolor=red, label=deploy ]
        test -> prod [ style=dotted ]

        prod -> testnew [ color=blue, fontcolor=blue, label="copy DB" ]
    }

Instructions
^^^^^^^^^^^^

#. Create an installation named *${customer}testnew* via Ansible.
   Copy the config from *${customer}test* to ensure custom
   configurations are preserved. You should end up with something like this
   in :term:`config.yml`:

   .. code-block:: yaml
      :emphasize-lines: 13-

       example:  # <-- customer
       # ...
       installations:
         example:  # <-- production
           db_server: db1.example.net             # <-- ${prod_db_server}
           # ...
         exampletest:  # <-- pre-existing test
           branch: releases/1.0
           env:                                    #
             custom_option: example                #        COPY THIS
           db_server: db1.example.net              #
         exampletestnew:  # <-- new test system
           branch: releases/1.0                    # <-- set this to correct (target) version
           env:
             custom_option: abc
           db_server: db1.example.net              # <-- ${testnew_db_server}

   For details see :doc:`/devops/app_management/new_customer`.

#. If you don't want to replace ${customer}, ${prod_db_server} and
   ${test_db_server} in the following steps manually, set them as
   variables in your terminal::

       customer=${customer}                #
       prod_db_server=${prod_db_server}    # Replace ${…}
       test_db_server=${test_db_server}    #

#. Check if DNS record exists::

       dig ${customer}testnew.tocco.ch

   If it doesn't (no output from ``dig``), create records for both,
   ${customer}newtest and ${customer}testold [#f1]_. See
   :ref:`dns-installation.tocco.ch`.  

#. Create installation ${customer}testnew::

       cd ${GIT_ROOT}/tocco
       ansible-playbook playbook.yml -l ${customer}testnew --skip-tags monitoring

   .. note::
      If you see errors that don't make sense to you, make sure that your local ansible setup is according to the latest tocco docs ansible setup guide.

#. Copy prod database::

       tco cp ${prod_installation} ${test_installation}/nice_${customer}testnew

#. Deploy installation ${customer}testnew

    .. warning::
        If the customer has a customer specific action or widget,
        it is recommended to create the "testnew" installation in the back office already one day earlier.
        Then the customer bundle is automatically release for the new version during the night. 
        Else you must :ref:`manual-trigger-client-releasing` with the variable ``CI_RUN_AUTO_RELEASE`` set to ``1`` and 
        ``CI_RELEASING_PACKAGES`` set to ``CUSTOMER-bundle`` (e.g. for the test customer it would be ``test-bundle``)

    See :ref:`cd-simple`

#. Enable monitoring::

       cd ${GIT_ROOT}/tocco
       ansible-playbook playbook.yml -l ${customer}testnew -t monitoring

Migration of Production
-----------------------

During this phase, the old *test* system is renamed *testold* and
*testnew* renamed *test*. Once this is done, *production* can
be upgraded to the new version.

.. graphviz::

    digraph {
        label="Migration of Production System (v3.0 → v3.5)"

        subgraph cluster_git {
            label="Git repository"
            r305 [ label="releases/3.5", shape="hexagon" ]
        }
        prod [ label="prod (v3.0 → v3.5)" ]
        test [ label="test (v3.5)" ]
        testnew [ label="testnew", style=dotted ]
        testold [ label="testold (v3.0)", color=green ]

        { rank=same test testnew testold }

        r305 -> test [ style=dotted ]
        r305 -> testnew [ style=invis ]
        test -> prod [ color=red, fontcolor=red, label=deploy ]
        r305 -> testold [ style=invis ]

        testnew -> test [ label="rename DB", color=blue, fontcolor=blue ]
        test -> testold [ label="rename DB", color=blue, fontcolor=blue ]
    }


Instructions
^^^^^^^^^^^^

#. Update :term:`config.yml`:

   a) Rename ${customer}test to ${customer}testold
   b) Rename ${customer}testnew to ${customer}test
   c) Update or add ``branch`` for production
   d) Add ``branch`` for testold if missing

   Example:

   .. code-block:: yaml
      :emphasize-lines: 5,7,9-10

      example:
        # ...
        installations:
          example:
            branch: releases/1.0    # <-- set to target version
             # ...
          exampletestnew:    # <-- rename to 'exampletest'
            # ...
          exampletest:       # <-- rename to 'exampletestold'
            # ...
            branch: release/1.0     # <-- add if missing
            # ...

#. If you don't want to replace ${customer} and ${db_server} in the following
   steps manually, set them as variables in your terminal::

       customer=${customer}      # Replace ${…}
       db_server=${db_server}    #

#. Create ${customer}testold and update configs of ${customer}test and
   ${customer}::

       cd ${GIT_ROOT}/tocco
       ansible-playbook playbook.yml -l ${customer}test,${customer}testold,${customer} --skip-tags monitoring

#. Set downtime for ${customer}test and ${customer}testnew.

   See :ref:`vshn-schedule-downtime`

#. Stop test systems::

       oc -n nice-${customer}test scale --replicas 0 dc/nice
       oc -n nice-${customer}testnew scale --replicas 0 dc/nice

#. Rename databases::

       tco db-rename ${customer}test ${customer}testold
       tco db-rename ${customer}testnew ${customer}test

#. Start ${customer}test again::

       oc -n nice-${customer}test scale --replicas 1 dc/nice

#. Remove ${customer}testnew

   Skip removal of DNS record [#f1]_.

   See :doc:`/devops/app_management/remove_customer`

#. Deploy installation ${customer}test

   See :ref:`cd-simple`

#. Deploy installation ${customer} (*production*)

   See :ref:`cd-simple`

#. Deploy installation ${customer}testold

   See :ref:`cd-simple`

#. Enable monitoring for testold::

       cd ${GIT_ROOT}/tocco
       ansible-playbook playbook.yml -l ${customer}testold -t monitoring

Complete Migration
------------------

Weeks later, when sure it isn't needed anymore, *testold*
can be removed.

.. graphviz::

    digraph {
        label="Returning to Normal Operations (v3.5)"

        subgraph cluster_git {
            label="Git repository"
            r305 [ label="releases/3.5", shape=hexagon ]
        }
        prod [ label="prod (v3.5)" ]
        test [ label="test (v3.5)" ]
        testold [ style=dotted, label="testold (v3.0)" ]

        { rank=same test testold }

        r305 -> test [ style=dotted ]
        test -> prod [ style=dotted ]
    }


Instructions
^^^^^^^^^^^^

#. Remove ${customer}testold

   Skip removal of DNS record [#f1]_.

   See :doc:`/devops/app_management/remove_customer`


.. rubric:: Footnotes

.. [#f1] DNS records for ${customer}testnew and ${customer}testold are needed
         on every migration. To avoid additional work, they are created but once
         and kept.
