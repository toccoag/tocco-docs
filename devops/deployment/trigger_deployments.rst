Automatically Trigger Deployments
=================================
.. attention::

    You need to be a Teamcity administrator to set up triggers.

Time-Based Triggers
-------------------

.. note::

   Time-based triggers for continuous delivery can and, generally, should be
   configured via Ansible. See *scheduled_cd_\** variables in Ansible.

Open Settings
`````````````

.. figure:: trigger_deployments_static/open_settings.png

Go to `Continuous Delivery project settings`_, find the right customer and open the *Settings* menu.


Create Trigger
``````````````

.. _Continuous Delivery project settings: https://dev.tocco.ch/teamcity/project.html?projectId=ContinuousDeliveryNg&tab=projectOverview

.. figure:: trigger_deployments_static/cron_trigger.png

1. Go to *Triggers* settings
2. *Add new Trigger*
3. Select trigger type *Schedule Trigger*
4. Select desired schedule
5. Save
