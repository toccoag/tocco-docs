Introduction to Deployments and Migrations
==========================================

.. index:: migration; deployment vs migration, deployment; deployment vs migration

Deployment vs Migration
-----------------------

In Tocco terminology, deployment and migration are two distinct
concepts:

A **deployment** is an update within the same minor version. For
instance, an update within 3.0.

A **migration** is an update across a major or minor version. For
instance, an update from 3.0 to 3.1.


Deployment
----------

During regular operations, there is one *production* and one *test*
system. When **test** is deployed, a new Docker image is built and
pushed to OpenShift. When **production** is deployed, the image
from *test* is copied and reused.

.. graphviz::

    digraph {
        label="Deployment"

        subgraph cluster_git {
            label="Git repository"

            r300 [ label="releases/3.0", shape=hexagon ]
        }
        prod [ label="prod (v3.0)" ]
        test [ label="test (v3.0)" ]

        r300 -> test [ label="build and push\nDocker image" ]
        test -> prod [ label="reuse image" ]
    }

|

A deployment of a change looks like this:

* Wait for your change to be merged to the release branch
  (e.g. *releases/3.0*).

* Deploy your change to *test* (:ref:`cd-simple`)

  A new Docker image is built from source and pushed
  onto OpenShift.

* Change is tested by our testers, project managers
  and/or customer. Fixes are deployed as needed.

* Changes are approved: deploy to *production*.
  (:ref:`cd-simple`) happens.

  The Docker image from *test* is copied to *production*. Thus,
  *production* and *test* will be identical after this deployment.


.. _migration-intro:

Migration
---------

During a migration, an additional **testnew** system is created
and updated to the desired version. Only once the update
is complete and testing is done, is the installation
renamed from **testnew** to **test** and a **production**
deployment happens..

.. graphviz::

    digraph {
        label="Secondary test system used during migration."

        subgraph cluster_git {
            label="Git repository"

            r300 [ label="releases/3.0", shape=hexagon ]
            r305 [ label="releases/3.5", shape=hexagon ]
        }
        prod [ label="prod (v3.0)" ]
        test [ label="test (v3.0)" ]
        testnew [ label="testnew (v3.5)" ]

        { rank=same test testnew }

        r300 -> test [ label="build and push\nDocker image" ]
        r305 -> testnew [ label="build and push\nDocker image" ]
        test -> prod [ label="reuse image" ]
    }

|

Main reason for creating an additional *test* system is to allow
easy deployment of hotfixes to *test* and *production* during an
ongoing migration.

A migration looks like this:

* Create a *testnew* installation, a copy of
  *production*.

* Update *testnew* to the desired version.

  This is done by deploying the corresponding Git
  branch (e.g. *releases/3.0*).

* Change is tested by our testers, project managers
  and/or customer. Fixes are deployed as needed.

* *testnew* is renamed *test*

  The old *test* system is temporarily renamed *testold*. [#f1]_

* Production is deployed

Reality is a bit more complex and fully described in :doc:`migration`.


.. rubric:: Footnotes

.. [#f1] It's kept in case the customer reports any regressions. In such a
         case, we can verify, and possibly dispute, the claim on *testold*.
