Continuous Delivery (CD)
========================

Teamcity Project "Continuous Delivery NG"
-----------------------------------------

The `Continuous Delivery project`_ is the entry point to CD.

.. _Continuous Delivery project: https://tc.tocco.ch/project/ContinuousDeliveryNg

.. figure:: deploy_and_basics_static/tc_main.png
   :scale: 60%
   :name: main page

   *Continuous delivery* project on TC's main page


.. index:: deployment; simple CD

.. _cd-simple:

Deliver (Simple)
----------------

.. note::

   Use `Deliver (Advanced)`_ if you need to deploy …
      #. … a test system with a custom Git branch, tag or commit
      #. … you want to enable (default for prod) or disable (default
         for test) DB dumps

.. figure:: deploy_and_basics_static/tc_run_menu.png		
   :name: run menu

   *Run* menu

#. Click on **Run** in the `main page`_
#. (optional) adjust the `dump mode`_ in the `run menu <#run-menu>`__
#. Click **Run Build** in the `run menu <#run-menu>`__


.. index:: deployment; advanced CD

.. _cd-advanced:

Deliver (Advanced)
------------------

.. figure:: deploy_and_basics_static/tc_parameters_tab_with_ellipsis.png
   :name: run menu advanced

   Full **Parameters** menu as shown when opening via ellipsis (...)

.. figure:: deploy_and_basics_static/tc_run_changes_tab.png

   *Changes* tab in *Run* menu

#. Click on **Run** in the `main page`_.
#. (optional) Adjust the `dump mode`_ in the `run menu <#run-menu>`__.
#. (optional) Select a particular `Git tag or branch <#deploy-a-specific-git-tag>`_ or deploy a particular `Docker image
   <#deploy-a-specific-docker-image>`_.
#. (optional) Select a particular commit in branch/tag using field "Includes changes".
#. Click **Run Build** in the `run menu <#run-menu>`__.


Dump Mode 
---------

.. figure:: deploy_and_basics_static/tc_dump_modes_dropdown.png

   **Dump Mode** dropdown on **Parameters** tab in **Run** menu

=========================  =============================================================================================
do not dump database       Deploy without creating a dump first (default for test systems.)
dump database              Create a dump and only then deploy (default for production systems.)
=========================  =============================================================================================


.. index:: deployment; deploy tag, deployment; deploy custom branch

Deploy a Specific Git Branch, Tag or Commit
-------------------------------------------

.. figure:: deploy_and_basics_static/tc_changes_tab_dropdown.png

   **Build branch** dropdown on **Changes** tab in **Run** menu

**Build branch** allows you to specify to deploy an arbitrary Git branch or tag.

.. note:: There might be a situation where you want to deploy a tag directly on production. 
          In that case remove the CD parameter "DOCKER_PULL_URL". `See deploy a Specific Docker Image <#deploy-a-specific-docker-image>`_.
