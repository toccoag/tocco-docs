.. meta::
   :keywords: thread dump, stack trace, memory dump, heap dump

Thread and Memory Dumps
=======================

.. index:: Nice2 debugging; thread dump
.. _create-thread-dump:

Creating a Thread Dump
----------------------

#. find pod

    .. parsed-literal::

        $ oc get pods -l run=nice --show-all=false
        NAME           READY     STATUS    RESTARTS   AGE
        **nice-3-2nl1q**   2/2       Running   0          49m

#. create thread dump

   .. parsed-literal::

        $ oc exec -c nice **nice-3-2nl1q** -- stack-trace
        Full thread dump OpenJDK 64-Bit Server VM (25.151-b12 mixed mode):

        "pool-7-thread-1182" #4356 prio=5 os_prio=0 tid=0x00007f9b2c040000 nid=0x12ec waiting on condition [0x00007f9a765fa000]
          java.lang.Thread.State: TIMED_WAITING (parking)


.. index:: Nice2 debugging; memory dump
.. _create-memory-dump-manually:

Creating a Memory Dump Manually
-------------------------------

#. find pod

   .. parsed-literal::

       $ oc get pods -l run=nice --show-all=false
       NAME           READY     STATUS    RESTARTS   AGE
       **nice-3-2nl1q**   2/2       Running   0          49m

#. create dump

   .. parsed-literal::

       $ oc exec -c nice **nice-3-2nl1q** -- memory-dump
       1:
       Dumping heap to /app/var/heap_dumps/manual-dump-2023-08-30T09:18:33.hprof ...
       Heap dump file created [664273346 bytes in 2.475 secs]
       Uploading '/app/var/heap_dumps/manual-dump-2023-08-30T09:18:33.hprof.zst' to '\ :green:`s3://memory-dumps/nice-demo/`\ :blue:`manual-dump-2023-08-30T09:18:33.hprof.zst`\ '.

#. copy dump to local machine

   .. parsed-literal::

      s3cmd get :green:`s3://memory-dumps/nice-demo/`\ :blue:`manual-dump-2023-08-30T09:18:33.hprof.zst`

   .. tip::

      ``s3cmd`` has to be set up in accordance with :ref:`machine-setup-s3cmd`.

#. Decompress:

   .. parsed-literal::

       zstd --rm -d :blue:`manual-dump-2023-08-30T09:18:33.hprof.zst`


Creating Memory Dump on OOM
---------------------------

.. note::

   Memory dumps are always created when an OutOfMemoryError is thrown (see `NICE2_DUMP_ON_OOM`_) and this
   is logged:

   .. parsed-literal::

      java.lang.OutOfMemoryError: Java heap space
      Dumping heap to **/app/var/heap_dumps/memory-dump-hlediiivhc.hprof** ...

   An easy way to see if any such dumps where created is to `list all dumps <list dumps>`_.

#. enable automatic memory dumps

    .. parsed-literal::

       oc set env dc/nice NICE2_MEMORY_DUMP_ON_TERMINATE=true

    .. warning::

       This will restart Nice automatically!

   =================================== =========================================================
    _`NICE2_MEMORY_DUMP_ON_TERMINATE`   Create a memory dump on every :term:`pod`
                                        termination. This does not only include
                                        pod termination as result of a pod restart
                                        triggered by the :term:`livenessProbe` as
                                        result of an OOM-condition. Consequently,
                                        **some dumps may not be related to an
                                        OOM-condition at all**.

    _`NICE2_DUMP_ON_OOM`                **This is enabled by default.**

                                        Tell the Java JVM to create a memory dump when
                                        an OutOfMemoryError is thrown. When memory is
                                        raising slowly, the application is often
                                        terminated due to a failing
                                        :term:`livenessProbe` before such an exception
                                        is thrown.
   =================================== =========================================================


#. wait for OOM crash


#. list dumps

   All dumps:

   .. parsed-literal::

       $ s3cmd ls -r s3://memory-dumps
       2023-08-30 07:18    127709619  s3://memory-dumps/nice-demo/manual-dump-2023-08-30T09:18:33.hprof.zst
       2023-08-30 07:57    101418514  :green:`s3://memory-dumps/nice-`:red:`test301`\ \/:blue:`automatic-dump-2023-08-30T09-56-41-nice-558-qrw8c.hprof.zst`

   Or just for a specific installation:

   .. parsed-literal::

       $ s3cmd ls s3://memory-dumps/nice-\ :red:`test301`
       2023-08-30 07:57    101418514  :green:`s3://memory-dumps/nice-`:red:`test301`/:blue:`automatic-dump-2023-08-30T09-56-41-nice-558-qrw8c.hprof.zst`

   .. tip::

      ``s3cmd`` has to be set up in accordance with :ref:`machine-setup-s3cmd`.

#. copy dump to local machine:

   .. parsed-literal::

      $ s3cmd get :green:`s3://memory-dumps/nice-`:red:`test301`/:blue:`automatic-dump-2023-08-30T09-56-41-nice-558-qrw8c.hprof.zst`

#. Decompress:

   .. parsed-literal::

       zstd --rm -d :blue:`automatic-dump-2023-08-30T09-56-41-nice-558-qrw8c.hprof.zst`


#. disable automatic dumps

    .. code::

        $ oc set env dc/nice NICE2_MEMORY_DUMP_ON_TERMINATE-

    .. warning::

        This will restart Nice automatically!
