Debugging via JMX
=================

:abbr:`JMX (Java Management Extensions)` provides basic debugging
capabilities, is shipped with Java and enabled by default.

.. tip::

   This is enabled for most Java applications, not just Nice,
   including our address provider and commit info service.

When connecting to JMX using *VisualVM* or *JConsole* it possible,
to:

* show application configuration
* monitor cpu, memory and threads
* profile cpu and memory
* obtain core, heap and thread dumps

.. note::

   For local debugging, `YourKit`_ is generally superior. While
   it is also possible to use it for remote debugging, the required
   agent is not currently included and configured on OpenShift.
   Licenses are available.

Forward JMX Port
----------------

Locally, JMX uses an auto-discover mechanism. On OpenShift, this
isn't possible and port 30200 is hardcoded instead.

#. Get pod name:

   .. parsed-literal::

       $ oc get pods
       NAME             READY   STATUS    RESTARTS   AGE
       **nice-380-gtt9g**   2/2     Running   0          2d

#. Forward connection:

   .. parsed-literal::

       oc port-forward **nice-380-gtt9g** 30200


Connect via VisualVM
---------------------

Install VisualVM::

    apt install visualvm

Connect via VisualVM::

    visualvm --openjmx localhost:30200

Screenshots:

.. figure:: resources/vvm-overview.png

   VisualVM - Overview

.. figure:: resources/vvm-monitor.png

   VisualVM - Monitor

.. figure:: resources/vvm-threads.png

   VisualVM - Threads

.. figure:: resources/vvm-sampler-cpu.png

   VisualVM - Sampler - CPU

.. figure:: resources/vvm-sampler-memory.png

   VisualVM - Sampler - Memory

See also:

* `VisualVM Features`_
* `VisualVM Plugins`_


Connect via JConsole
--------------------

Connect via JConsole::

    jconsole service:jmx:rmi:///jndi/rmi://localhost:30200/jmxrmi

Screenshot:

.. figure:: resources/jconsole-overview.png

   JConsole - Overview


.. _VisualVM Features: https://visualvm.github.io/features.html
.. _VisualVM Plugins: https://visualvm.github.io/plugins.html
.. _YourKit: https://www.yourkit.com/java/profiler/
