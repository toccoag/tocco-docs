.. index:: Nice2 debugging; remote code debugging

Remote Debugging of Nice2
=========================

.. warning::

   Always make sure you select **Suspend -> Thread** for breakpoints in IDEA,
   so that you don't freeze the whole application during your debugging session.

#. forward the debugging port to your machine

    #. switch project

        .. parsed-literal::

            $ oc project nice-**${INSTALLATION}**

    #. find running pod

        .. parsed-literal::

            $ oc get pods -l run=nice --show-all=false
            NAME           READY     STATUS    RESTARTS   AGE
            **nice-3-2nl1q**   2/2       Running   0          49m


    #. forward port

        .. parsed-literal::

            $ oc port-forward **nice-3-2nl1q** 40200

#. Now Set Up Remote Debugging in IDEA

    .. figure:: remote_debugging/remote_debugging.png
        :scale: 60%

        Add debug configuration in IDEA.
