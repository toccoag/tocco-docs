6 weeks before release: Create tasks
====================================

To create release branch and test installation
----------------------------------------------

- :ref:`backend_release`
- :ref:`client_release`
- :ref:`documentation_release`

To update outdated dependencies
-------------------------------

It's important to keep external dependencies up to date and it makes sense to update them at the very beginning
of a release development cycle (to be able to spot problems early during the development cycle).

Therefore, **create one task for the backend** and **create one task for the client** to update the outdated
dependencies in one of the first sprints.

See chapter :ref:`update_backend_dependencies_on_a_regular_basis` and
:ref:`update_client_dependencies_on_a_regular_basis` to learn where you get the list of outdated dependencies from.

Also, the NodeJS version in the root **npm.gradle** and in **preprocessor/build.gradle** as well as
the **dependencies in preprocessor/package.json** should be updated. Therefore **create one task for the preprocessor** as well.

Additionally **create one task for the external services** to update the :ref:`address_provider`, :ref:`commit_info_service`,
:ref:`atlassian_connect_integration` and :ref:`image_service`.

The dependency update can be done as soon as the release branch(es) were created.

To update Hibernate documentation
---------------------------------

A new JIRA task should be created to keep the Hibernate documentation up to date.
All changes in the ``persist/core`` module since the last release should be reviewed
and the documentation should be adjusted if necessary.

For `toccotest.tocco.ch`_ migration
-----------------------------------

`toccotest.tocco.ch`_ should be migrated to the new version as soon as possible after this branch has been created.
This is done by the Tocco Dev team (not by the Business Services).

A new JIRA task should be created in the `TOCBO`_ project and assigned to the Dev team.

.. _TOCBO: https://toccoag.atlassian.net/projects/TOCBO
.. _toccotest.tocco.ch: https://toccotest.tocco.ch

toccotest should be updated approximately 4 weeks before release. Usually it is done directly after creating the relase
branch.

For `www.tocco.ch`_ migration
-----------------------------

Approximately one week before the release date, our Tocco Backoffice should be updated to the new version.
This is done by the Tocco Business Services.

A new JIRA task should be created in the `TOCBO`_ project and assigned to the Business Services team.

.. _www.tocco.ch: https://www.tocco.ch

For `demo.tocco.ch`_ migration
------------------------------

Approximately one week before the release date, our demo installation `demo.tocco.ch`_ should be updated to the new
version. This is done by the Tocco Business Services.

A new JIRA task should be created in the `TOCBO`_ project and assigned to the Business Services team.

.. _demo.tocco.ch: https://demo.tocco.ch

For `integration.tocco.ch`_ migration
-------------------------------------

Approximately one week before the release date, our integration installation `integration.tocco.ch`_ should be updated to the new
version. This is done by the Tocco Business Services.

A new JIRA task should be created in the `TOCBO`_ project and assigned to the Business Services team.

.. _integration.tocco.ch: https://integration.tocco.ch

To update image service
-----------------------

Check https://github.com/h2non/imaginary if a new version is available or other adjustments on our end are necessary. If
there are, create a task to deploy a new version, first in test and then in production as usual.

See :ref:`image_service` for details on the service.

To check fields where xss-filter is deactivated
-----------------------------------------------

Create a task to update the excel list of xss-filter="false" usages:
https://tocco.sharepoint.com/:x:/r/sites/Technik/_layouts/15/Doc.aspx?sourcedoc=%7B709afbb5-88e1-497b-90f3-85c83b6425f1%7D

In this task these usages should be checked and adjusted if necessary.

To update address provider
--------------------------

Create a task to update address provider:

1. Update the data source as described here https://gitlab.com/toccoag/address-provider#update-data-source (if nothing changed you can skip the remaining steps)
2. Create a merge request
3. After the merge request is merged the test address provider is automatically deployed (takes a few minutes)
4. Check on all test systems such as https://master.tocco.ch/, https://test229.tocco.ch/ etc. if the location suggestion on the address entity is still working
5. Deploy the production address provider via GitLab by finding the play button for the *prod_deploy* job belonging to the current `pipeline`_.

.. _pipeline: https://gitlab.com/toccoag/address-provider/-/pipelines

To update SonarQube
-------------------

Create a task to update SonarQube, check any newly added rules and discuss our new targets.

1. Update SonarQube as described in ``SonarQube Updates`` of the DevOps manual.
2. Check all newly added rules by using the ``Available Since`` search field in SonarQube itself. Ignore any rules for languages we don't use.
3. Activate any rules that seem sensible to you. When in doubt, ask the other developers.
4. Run the `tagging tool`_ to migrate old severities to tags for newly activated issues or rules.
5. Schedule a meeting with all members of the developer team to discuss our targets for the new version.

.. _tagging tool: https://gitlab.com/toccoag/sonarqube-tag-bot

To check for new camt.053 / camt.054 versions
---------------------------------------------

Check the `iso20022`_ website for new versions of the ``camt.053`` and ``camt.054`` message standard. If there are
versions that are not yet covered by a respective ``ImportEsrFileServiceContribution``, download the missing XSDs and
add them. See `example commit`_ for an example of how to add new XSDs and their respective contributions.

.. _iso20022: https://www.iso20022.org/
.. _example commit: https://git.tocco.ch/c/nice2/+/52310