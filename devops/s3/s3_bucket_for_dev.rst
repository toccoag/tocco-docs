.. index:: S3; users

S3 User for Developers
======================

This document describes how to create a user account for a developer
and how to set ACLs and policies properly.

Naming Convention for Developers
--------------------------------

============== ==================================================================
 S3 user name   dev-**${EMPLOYEE_SHORT_NAME}** (see :term:`Employee Short Name`)
============== ==================================================================

.. note::

    The prefix ``dev-`` is used by Ansible to recognize developers and grant
    permissions accordingly.


Create User
^^^^^^^^^^^

..parsed-literal::

    tco create-user **${NAME}**


Remove User
^^^^^^^^^^^

..parsed-literal::

    tco remove-user **${NAME}**


Show User Details
^^^^^^^^^^^^^^^^^

..parsed-literal::

    tco show-user **${NAME}**


The *access_key* and *secret_key* values correspond to the ``s3.main.accessKeyId`` and
``s3.main.secretAccessKey``, respectively, within the ``s3.properties`` file.

See also :doc:`/framework/architecture/s3/s3`.

Set Permissions via Ansible
---------------------------

Permissions are set by Ansible automatically. It detects user accounts of developers
based on the ``dev-`` prefix in their name::

    $ cd ${ANSIBLE_GIT_REPO}/tocco
    $ ansible-playbook playbook.yml -t s3

See also: :doc:`/devops/app_management/ansible_usage`
