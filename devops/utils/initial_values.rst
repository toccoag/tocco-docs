.. _initial-values:

#######################
Initial Value Generator
#######################

Country Initial Value Generator
===============================

This script generates the Tocco initial values for countries.

* The data source is https://mledoze.github.io/countries
* The BFS data source is https://www.bfs.admin.ch/bfs/de/home/grundlagen/stgb.html

Execute Script
^^^^^^^^^^^^^^

**Prerequisite:** PyYAML is installed (``pip install PyYAML``)

1. Clone the generator from https://gitlab.com/toccoag/country-initial-value-generator
2. Go to the data source on GitHub and download the latest version of the ``countries.json`` file
3. Copy the ``countries.json`` file to the ``input`` directory
4. Go to the BFS data source and download the data in XML format
5. Extract the ``.xml`` file from the downloaded ZIP file. Rename it to ``bfs_countries.xml`` and copy it to ``input/bfs_countries.xml`` directory
6. Run the script with ``python3 CountryImporter.py``
7. Copy the existing initial value file (from the Nice repository ``optional/address/resources/db/initialvalues/Country.yaml``) to ``input/Country.yaml``
8. Run the script with ``python3 CountryDiff.py``
9. For each country code in the *Country code no longer exists* list do the following in the Nice repository (change set location should be ``optional/address/resources/db/initialvalues/2.xx_update_country.xml``):

    * If a country still exist with another iso3 code (new iso3 code must be listed in *New country code added*), then add the following change set:

    .. code-block:: XML

        <changeSet author="anonymous" dbms="postgresql" id="ID" runOnChange="true">
            <preConditions onFail="MARK_RAN">
               <tableExists tableName="nice_country"/>
            </preConditions>
            <update tableName="nice_country">
               <column name="iso3" value="NEW_VALUE"/>
               <where>iso3 = 'OLD_VALUE'</where>
            </update>
        </changeSet>


   Set ``ID`` to something like ``updateCountry_XKV_to_UNK/2.27``, ``OLD_VALUE`` is the old iso3 code and ``NEW_VALUE`` is the new iso3 code

   Additionally, update ``input/{licence_plate, sorting}.csv`` in the generator repository if necessary

    * Else, write a change set to set the country to inactive:

    .. code-block:: XML

        <changeSet author="anonymous" dbms="postgresql" id="ID" runOnChange="true">
            <preConditions onFail="MARK_RAN">
              <tableExists tableName="nice_country"/>
            </preConditions>
            <update tableName="nice_country">
              <column name="active" value="false"/>
              <where>iso3 = 'ISO_CODE'</where>
            </update>
        </changeSet>

    Set ``ID`` to something like ``updateCountry_remove_ANT/2.27`` and ``ISO_CODE`` is the iso3 code

10. Copy the output files (in ``output``) to the Nice repository:

    * Override the existing ``Country.yaml`` file in ``optional/address/resources/db/initialvalues``
    * Override the country name section in the language file ``language_XX.properties`` which is located at ``optional/address/resources/model/textresources``
11. Check the git diff to verify the data source quality

Additional Information
^^^^^^^^^^^^^^^^^^^^^^

* The ``licence_plate`` field is not part of the data source. There is a static file under ``input/licence_plate.csv`` as data source in the format ``Iso3,Licence_plate_code``
* The ``sorting`` field is an internal Tocco field. Per default a country obtains the value ``100``. If a non-default value is required there is a static file under ``input/sorting.csv`` in the format ``Iso3,number`` to define the value per country
* Some countries have multiple currency codes and calling codes. In such a case the values are comma-separated written into the text field
