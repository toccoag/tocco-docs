Utilities
=========

Nice2
-----

========================= ====================================================
 Name                      Description
========================= ====================================================
 n2customer-details        Script printing details about customer-specifc
                           adjustment in the *nice2* repository.

                           Script is in :term:`tocco-dotfiles` and should
                           be in *$PATH* of all developers. ``n2customer-details
                           --help`` for details.
========================= ====================================================


OpenShift / Ansible
-------------------

========================= ====================================================
 Name                      Description
========================= ====================================================
 memory_usage.yml          Generate CSV report containing current memory
                           settings of all installations.

                           See :doc:`installaton_memory_usage`

 storage_usage.yml         Generate a CSV report containing current storage
                           usage for all installations.

                           See :doc:`installaton_storage_usage`
========================= ====================================================


Postgres
--------

* :doc:`../databases/sql_snippets_postgres_script`
* :doc:`../databases/sql_snippets_postgres`
* :doc:`../databases/sql_snippets_nice2`


Full Index
----------

.. toctree::
   :maxdepth: 2
   :glob:

   *
