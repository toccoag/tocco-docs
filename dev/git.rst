===
Git
===

This is a short introduction on how to use Git/Gerrit in the nice-backend.

Gerrit creates a "Pull request" for every commit made.
When you need to fix something in that pull request you will most likely be able to do it with a simple ``git commit --amend``
That command lets you combine staged changes with the previous commit instead of creating an entirely new commit. 
For example add additional files to it, make additional changes in the files or change the commit message.
This can be done in the command line or also very easily in IntelliJ via the ``Amend`` checkbox in the commit window.

When you have made more than one commit on the same branch you need to use ``git rebase -i``.
This begins an interactive rebasing session. Interactive rebasing gives you the opportunity to alter individual commits in the process. 
In the editor that was opened you can use commands to determine how individual commits will be transferred to the new base.
The available commands are as follows:

.. code-block:: sh

    pick 2231360 some old commit
    pick ee2adc2 Adds new feature


    # Rebase 2cf755d..ee2adc2 onto 2cf755d (9 commands)
    #
    # Commands:
    # p, pick = use commit
    # r, reword = use commit, but edit the commit message
    # e, edit = use commit, but stop for amending
    # s, squash = use commit, but meld into previous commit
    # f, fixup = like "squash", but discard this commit's log message
    # x, exec = run command (the rest of the line) using shell
    # d, drop = remove commit

With these 2 commands, most issues should be fixable.
More options can be found here for example: `Atlassian docs <https://www.atlassian.com/git/tutorials/rewriting-history>`_

Gerrit
------

Every commit needs to be looked at by another person. A reviewer can be set on Gerrit inside the commit or when pushing the commit directly inside IntelliJ.
A reviewer can set a Code-review status: 

.. code::

  -2: Do not submit
  -1: I would prefer that you didnt submit this
  +1: Looks good to me, but someone else must approve
  +2: Looks good to me, approved

A commit needs to have a +2 so that it can be submitted (merged)
It also needs a +1 from the Teamcity verification job. This one starts automatically (as soon as a Teamcity agent is idle).
When the verification returns a -1, the jobs needs to be checked for the cause. Most often its failing unit-tests or errors in the language files.

Once a commit has been submitted, additional jobs will be started to merge the changes into the releases branch and to higher versions (if
the changes were made on an older version). Merging to higher versions can sometimes lead to merge conflicts which then need to be fixed.
