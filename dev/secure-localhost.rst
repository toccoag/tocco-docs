Running Widgets over SSL on localhost
=====================================

This setup is using ``mkcert`` and is based on these resources:

- https://web.dev/how-to-use-local-https/
- https://shekhargulati.com/2019/01/19/enabling-https-for-local-spring-boot-development-with-mkcert/

.. warning::
   Never export or share the file rootCA-key.pem mkcert creates automatically!

.. warning::
   For Firefox you need to install ``brew install nss`` as well.

1. Install ``mkcert``: https://github.com/FiloSottile/mkcert#installation
2. Install root certiciate authority: ``mkcert -install``
3. Add your domains to your ``hosts`` file (e.g. ``tocco.customer.localhost`` for nice2 and ``customer.localhost`` for the website)
4. Create certiciate for the website ``mkcert customer.localhost``
5. Create certiciate for nice2 ``mkcert -pkcs12 tocco.customer.localhost``
6. Copy the created ``tocco.customer.localhost.p12`` certificate to ``/customer/test/src/main/resources``
7. Add following properties to nice2

  .. code::

    server.ssl.enabled=true
    server.ssl.protocol=TLS
    server.port=8080
    server.ssl.key-store=classpath:tocco.customer.localhost.p12
    server.ssl.key-store-type=PKCS12

8. Add created certificate to website server ``/scripts/widgets/server.js``

  .. code::

    const fs = require('fs')
    const https = require('https')

    ...

    const privateKey = fs.readFileSync('/<...>/customer.localhost-key.pem')
    const certificate = fs.readFileSync('/<...>/customer.localhost.pem')

    https
      .createServer(
        {
          key: privateKey,
          cert: certificate
        },
        app
      )
      .listen(3000)

9. Set correct backend url to website server ``/scripts/widgets/server.js``

 .. code::

    const backendUrl = process.env.BACKEND_URL || 'https://tocco.customer.localhost:8080'
    ...
    <script>window.toccoBackendUrl = "https://tocco.customer.localhost:8080";</script>
    ...
    <script src="https://tocco.customer.localhost:8080/js/tocco-widget-utils/dist/bootstrap.js"></script>

10. Run nice2: https://tocco.customer.localhost:8080
11. Run website server ``yarn widgets:serve``: https://customer.localhost:3000?key=<widget-config-key>

    The website server simulates a website which embeds a widget. By passing a valid widget config key on the query parameter ``key`` it will automatically embed this widget.


.. tip::

  When having a login widget configured you can simulate to login on the website via the login widget by passing the login widget key to the website url.
  After successful login the cookie is set and you can open any another widget by passing another widget config key.


