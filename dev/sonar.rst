SonarQube
=========

SonarQube is a static code analysis tool. It reports common errors and code smells, as well as statistics about a code
base. At Tocco, we analyse our backend and frontend code with SonarQube. Our self-hosted instance of SonarQube
can be found at https://sonar.tocco.ch. For each version we set new targets to reach in our code as a team. These
targets can be found in our SharePoint under ``Technik/Quality Assurance/Sonar``.

.. warning::

   At the time of writing, SonarSource have deprecated the older severities with new impacts.
   These are completely static and have no granularity in how pressing they are to fix.
   Therefore, we still use the severities by migrating them to tags via a custom `tagging tool`_.

.. _tagging tool: https://gitlab.com/toccoag/sonarqube-tag-bot

Analyses
--------

We run an analysis for each of our codebases each day. We only analyse the current master branch of each codebase. Most
Quality Gates are turned off currently. This way SonarQube currently just provides a view of the current state of our
code and a history how it developed.

Backend
^^^^^^^

The job and schedule can be found at https://tc.tocco.ch/buildConfiguration/Nice2Sonar_Backend. See the root
``build.gradle`` for the SonarQube configurations used. The project in SonarQube is called ``Nice2 Backend`` and can be
found at https://sonar.tocco.ch/dashboard?id=ch.tocco.nice2%3Abackend.

Client
^^^^^^

The schedule can be found at https://gitlab.com/toccoag/tocco-client/-/pipeline_schedules/193879/edit. See the
``.gitlab-ci.yml`` file for the CI job and the ``sonar.properties`` file for the SonarQube configurations. The project
in SonarQube is called ``Nice2 Client`` and can be found at
https://sonar.tocco.ch/dashboard?id=ch.tocco.nice2%3Aclient.

User Accounts
-------------

To access SonarQube, you need a user account. At time of writing, we have no automated process to create user accounts,
so you will have to create your own. The password for the admin account can be found in :term:`secrets2.yml`. Create a
new user account under https://sonar.tocco.ch/admin/users and feel free to give yourself admin rights. Please do not
continue to use the admin account for yourself, since we would like to know who changed what and be able to assign
issues.

SonarLint
---------

A companion plugin to SonarQube is available for many different editors, including Jetbrains IDEs and VSCode. It can
be used in standalone mode, in which it simply informs you about errors and code smells as you have configured it, or
in connection with our SonarQube instance. When connected to our instance, it will check our settings and report
problems in-line with how we have configured them.

Dealing with issues
-------------------

There are several different ways how to handle issues.

Fix them
^^^^^^^^

The easiest is to simply fix them. Read their description and adjust the affected code like you would any other task.

Ignore them
^^^^^^^^^^^

Some issues are false positives, others we can't or don't want to fix. These issues can be marked as such directly in
SonarQube. Previously, we would use annotations and comments in the code itself to mark these issues. This is something
we want to get away from and you are welcome to remove any such markings when you encounter them.

Remove them
^^^^^^^^^^^

The rules SonarQube uses to create issues are configurable and have in fact been heavily adjusted by us already. If a
rule does not make sense in your opinion start a discussion with the developer. If a consensus is reached the rule can
be deactivated in SonarQube itself. Check the documentation or ask someone else about how to deactivate the rule.
