Working with Version Branches
=============================

When you have an issue that requires you to fix something on a specific integration/releases/X.X branch, then get started with this guide.

Branch preparation
------------------

Checkout the version branch that you want to work on. The branch is normally called something like integration/releases/X.X where X.X is e.g. "3.4".
Open the branch in IntelliJ.

.. hint::

    It is recommended that you create a new git checkout location for each branch so that you can easily switch between branches without forcing IDEA to index the project for 10 minutes after a branch switch.

Database preparation
--------------------

If you don't have the customer database copied to your local machine yet, check out :ref:`copy-db` to see how to set up a local database.

Module preparation
------------------

Depending on the module you are working in (test, customer-name, etc.), you might need to create a new application-development.properties file in folder customer/[customer-name]/src/main/resources and define the database connection to your local or remote database.

IntelliJ IDEA preparation
-------------------------

Depending on the customer you are working on, you might also need to switch the module in IDEA that will be started when you run the backend. To do this, edit your preferred run configuration in IDEA and change the gradle module to the customers module you are trying to run.