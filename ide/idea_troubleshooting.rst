IDEA Troubleshooting
====================

Idea Performance
----------------

If your Idea isn't running smoothly or becomes unresponsive when you open multiple projects, try increasing memory like this:

**Help** → **Edit Custom VM Options…** and then set max. memory higher (**-Xmx…**). Don't forget to restart Idea


"Too many open files"
---------------------

With the introduction of Java 11, far more files are opened at once during building sometimes leading to the
error "Too many open files". Increase the the max. number of open files in such a case.

Increasing the limit on Linux:

    Create ``/etc/security/limits.d/open_file_limit.conf`` with this content::

        *                -       nofile          1000000

    **Log out and in again** for this to become effective. Use ``ulimit -n`` to show the current limit.
