Database
========

Can't Change Permissions on Extension
-------------------------------------

Error
^^^^^

.. code::

    … could not execute query: ERROR:  must be owner of extension lo
    Command was: COMMENT ON EXTENSION …

Cause
^^^^^

``pg_restore`` skips creation of the object itself, in this case **lo**, because it already exists. However, it still
tries to change the comment but doesn't have the permissions.

Solution - Remove Comment
^^^^^^^^^^^^^^^^^^^^^^^^^

Remove the comments from the affected extensions to ensure future dumps won't contain **COMMENT ON …** statements.

Usually these two are affected:

.. code:: sql

    COMMENT ON EXTENSION lo IS NULL;
    COMMENT ON EXTENSION plpgsql IS NULL;

.. hint::

    Remove the comment in database **template1** to ensure new databases don't contain it. (A ``CREATE DATABASE xy``
    copies that DB.)

Workaround - Don't Restore Comments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use ``pg_restore``'s ``--no-comments`` to skip restore of comments.


Postgres Connection Timeout
---------------------------

Error
^^^^^

.. code::

    HikariPool-1 - Connection is not available, request timed out after 30001ms.

often seen in connection with:

.. code::

    PersistenceException: org.hibernate.exception.JDBCConnectionException: Unable to acquire JDBC Connection


Cause
^^^^^

HikariCP, the connection pool used by Nice, has a fixed number of connections available. The error tells us that no
connection could be obtained within the given time. This could be because all connections are in use, because
connecting to the server failed, or some other other issue lead to delays.


Analysis
^^^^^^^^

Low Memory
``````````

In the vast majority of cases, this error is printed because the application
is low on memory. In such a scenario, the GC is using most of the available
resources, slowing down everything to a near-halt delaying the return of
DB connections.

Solutions:

* Create a memory dump for later analysis
* :ref:`create-memory-dump-manually`


Too Few Connections
```````````````````

#. First, check how the connections are used …::

       $ tco db-queries ${installation}

#. … and check how many DB connections are available::

       $ tco config ${installation}
       14: agogis:
       …
       34:   installations:
       35:     agogis:
       36:       branch: releases/3.6
       37:       env: !merge
       38:         hibernate.main.hikari.maximumPoolSize: 15    # <---
       41:       db_server: db3.tocco.cust.vshn.net
       42:       enable_performance_monitoring: true
       …

   `hibernate.main.hikari.maximumPoolSize` is the maximum number of
   connections available. The value currently defaults to 6.

   Take a look at `HikariCP's github page`_.

#. Take a look at how the connections are used

.. parsed-literal::

   tco db-queries **${installation}**


.. _HikariCP's github page: https://github.com/brettwooldridge/HikariCP
