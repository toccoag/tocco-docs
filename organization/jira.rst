JIRA
============

Ticket Types
------------

When creating new tickets in JIRA, it is important to pick the right type so that our backlog stays nice and clean. Please use the different types of tickets as follows:

 - "Improvement" & "New Feature"
    - The ticket should provide mainly business value. Everything that is visible (except bugs) to the customer should be this type of ticket. Also, "Improvement" & "New Feature" both have a green icon, so it's also a quick visual indicator when browsing the backlog.
    - Examples: New widget, improvement on the styling of the UI, new actions
 - "Bug"
    - Everything that is a bug. It does not matter if the bug is visible to the customer or not.
 - "Task"
    - A mainly technical ticket that is not directly visible for the customer and is not a defect.
    - Examples: Spring migration, update dependencies, write technical documentation