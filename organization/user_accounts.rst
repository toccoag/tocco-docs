User Accounts
=============

Who get what Account?
---------------------

======================= ======================== ===== ============
 Where                   Role / Group             Dev   Ops/Admins
======================= ======================== ===== ============
 Gerrit                  \-                        x
 Gerrit                  *Administrators*                   x
 Teamcity                *developers*              x
 Teamcity                *admins*                           x
 SSH Server Access       *@user*                   x
 SSH Server Access       *@root*                            x
 SSH Emergency Access    *@root*                            x
 VSHN                    \-                        x        x
 VSHN SSH                *tocco*                   x
 VSHN SSH                *toccoroot* and                    x
                         *tocco*
 GitHub                  *Member*                  x
 GitHub                  *Owner*                            x
 GitLab                  *Maintainer*              x
 GitLab                  *Owner*                            x
 Docker Hub              *Owner*                            x
 S3                      \-                        x        x
 Nine Cockpit            *Cockpit Access*          x
 Nine Cockpit            *Administrator*                    x
 Wireguard VPN           \-                        x        x
 Bitwarden               *User*                    x        x
 Sonar                   *User*                    x        
======================= ======================== ===== ============

Gerrit
------

Create User
^^^^^^^^^^^

**${USERNAME}** is the username (e.g. *jdoe* for Jane Doe)

#. Generate password::

       pwgen -s 20 1

#. Create user::

       ssh -t tadm@git.tocco.ch sudo htpasswd /etc/nginx/.htpasswd ${USERNAME}

#. If admin access should be granted (see table above), go to go to the group
   `Administrators <https://git.tocco.ch/admin/groups/1,members>`__ and add the
   user as a member.


Deactivate User
^^^^^^^^^^^^^^^

.. code::

    ssh -p 29418 ${OWN_USERNAME}@git.tocco.ch "gerrit set-account --inactive ${USERNAME}"
    ssh -t tadm@git.tocco.ch sudo htpasswd -D /etc/nginx/.htpasswd ${USERNAME}

TeamCity
--------

Permissions are granted via the *admins* and *developers* groups. New permission
should generally be granted to those groups rather than individual users.

Create User
^^^^^^^^^^^

#. Go to `Users <https://tc.tocco.ch/admin/admin.html?item=users>`__.
#. *Create User Account*
#. Enter *Username* (e.g. *jdoe* for Jane Doe)
#. Enter full name as *Name*
#. Enter *Email address* (e.g. jdoe@tocco.ch)
#. Generate a password (``pwgen -s 20 1``)
#. Select newly created user (click on username)
#. Select *Groups* tab.
#. Add user to group *admins* or *developers* according to the table
   above.

Remove User
^^^^^^^^^^^

#. Go to `Users <https://tc.tocco.ch/admin/admin.html?item=users>`__.
#. Remove user


.. _ssh-server-access-ansible:

SSH Server Access / Emergency Access (Ansible)
----------------------------------------------

Allow Access
------------


    Access can be granted via `roles/ssh-key-sync/files/ssh_keys`_ in the Ansible repository.
    For DevOps / Admin employees also add the key to the emergency key file `roles/ssh-server/files/authorized_keys_emergency`_.

    Changes can be deployed via Ansible::

        cd ${ANSIBLE_GIT_ROOT}/servers
        ansible-playbook -i inventory playbook.yml -t ssh-keys

    .. hint::

        Users with role ``@user`` have access as user *tocco* on some hosts. User with role ``@root`` have access as
        *tadm* and *tocco* on all hosts. For users that should only have access to :doc:`/ide/pseudo-vpn`, use role
        ``@remote``.


Revoke Access
-------------

    See *Allow Access* above and do the opposite.


VSHN
----

Create Account
^^^^^^^^^^^^^^

*Customer Admins* can create accounts at https://control.vshn.net/users/tocco

Select the following *Portal Roles*:

* *Customer Admin* (ops)
* *Customer Developer* (dev)
* *Customer Ticket* (everybody)

And the following *Service Aceess*:

* *Customer Portal* (everybody)
* *GitLab* (ops / root user)
* *Icinga Web 2* (everybody)
* *Tocco OpenShift OCP4* (dev/ops)

OpenShift:

* Have user added to group *tocco-dev* (dev)
* Have user added to group *tocco-admin* (ops / root users)

Create a ticket to have user added to group. See :vshn:`TOCO-575`.

Remove Account
^^^^^^^^^^^^^^


*Customer Admins* can remove accounts at https://control.vshn.net/users/tocco


.. _vshn-ssh-access:

VSHN SSH
--------

Grant Access
^^^^^^^^^^^^

    Puppet configuration can be found in the `tocco_hieradata repository`_. Access is defined in
    the ``users`` section within the different config files (e.g. in ``database.yml`` for
    database servers and ``infrastructure/solr.yml`` for Solr servers).

    .. hint::

        Users that are part of the group ``toccoroot`` can use sudo to obtain root priviledges.

    Once the user has been created on the server — this can take up to 1h  — roles on Postgres
    have to be updated::

        $ cd ${ANSIBLE_REPO}/servers
        $ ansible-playbook playbook.yml -t postgres


Revoke Access
^^^^^^^^^^^^^

    To remove an account, add an ``ensure: absent``.


GitHub
------

Add User to Organization
^^^^^^^^^^^^^^^^^^^^^^^^

Go to the `People page`_ and *Invite member*.

Remove User from Organization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Go to the `People page`_ and select *Convert to outside collaborator…*.


GitLab
------

Add User to Group
^^^^^^^^^^^^^^^^^

Go to the `Members page`_ and add the user.


Remove User from Group
^^^^^^^^^^^^^^^^^^^^^^

Go to the `Members page`_ and remove the user.


Docker Hub
----------

Add User to Organization
^^^^^^^^^^^^^^^^^^^^^^^^

Go to the `Docker Hub's Members page`_ and add the user.

Remove User from Organization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Go to the `Docker Hub's Members page`_ and remove the user.


S3
--

.. _s3-user-creation:

Create User
^^^^^^^^^^^

#. User name should be *dev-${SHORT_NAME}*. For Instance, the user
   name for Jane Doe would be *dev-jado*::

       tco s3 create-user dev-${SHORT_NAME}

   Printed *access_key* and *secret_key* need to be handed to user and
   configured in :ref:`~/.aws/credentials <machine-setup-s3-access-key>`

#. Grant permissions::

       cd ${ANSIBLE_GIT_REPO}/tocco
       ansible-playbook playbook.yml -t s3

Remove User
^^^^^^^^^^^

.. code-block::

   tco s3 remove-user dev-${SHORT_NAME}


Nine Cockpit
------------

Add User
^^^^^^^^

#. Login at https://cockpit.nine.ch
#. Click "tocco" in *Switch Account* section
#. Open *Account* → *Contacts*
#. Send invite to @tocco.ch mail address

   Select *Administrator* (ops/admin) or *Cockpit Access* (dev) as role.

Remove User
^^^^^^^^^^^

#. Login at https://cockpit.nine.ch
#. Click "tocco" in *Switch Account* section
#. Open *Account* → *Contacts*
#. Open *Permissions* for user
#. Remove all permissions


.. _accounts-wireguard:

Wireguard VPN
-------------

Add User / Device
^^^^^^^^^^^^^^^^^

============= =======================================================
 $group        "admin" for operations, "user" for everyone else.
 $name         User's name (i.e. *jado* for *Jane Doe*).
 $public_key   Public key supplied by user.
============= =======================================================

#. Get public key from user

#. Generate config::

       cd ${ANSIBLE_REPO}/servers/
       ./gen-wg-key $group $name $public_key

#. Edit inventory *inventory* and :term:`secrets.yml` as suggested by script.

#. Apply settings::

       ansible-playbook playbook.yml -t wireguard

#. Provide user config printed by script to user.

Remove User / Device
^^^^^^^^^^^^^^^^^^^^

#. Remove from *inventory*

   Remove user/device from section *wireguard_peer_groups*.

#. Remove from :term:`secrets.yml`

   Remove PSK from section *wireguard_psk*.

#. Apply settings::

       ansible-playbook playbook.yml -t wireguard


.. _accounts-bitwarden:

Bitwarden 
---------

Invite new User
^^^^^^^^^^^^^^^

#. Login to `Webvault <https://vault.bitwarden.eu/#/organizations/4c45c269-7d78-45f5-8fad-b09100e5c856/members>`__ as admin@tocco.ch
#. Click on the green "Invite member" banner at the right upper screen
#. Enter the users mail address, choose the member role "User" and add it to the corresponding groups
#. Wait until the user accepts the invite which switches its state to "needs confirmation"
#. Confirm the user in the members overview via the three dots on the right side of the screen

Remove User
^^^^^^^^^^^

#. Login to `Webvault <https://vault.bitwarden.eu/#/organizations/4c45c269-7d78-45f5-8fad-b09100e5c856/members>`__ as admin@tocco.ch 
#. Click the three dots on the right side of the screen, next to the user that needs to be removed
#. Choose the "remove" option

.. _accounts-sonar:

Sonar 
-----

Create User
^^^^^^^^^^^

#. Login at https://sonar.tocco.ch/
#. Go to Administration > Security > Users
#. Choose Create User

Remove User
^^^^^^^^^^^

#. Login at https://sonar.tocco.ch/
#. Go to Administration > Security > Users
#. Deactivate user

.. _roles/ssh-key-sync/files/ssh_keys: https://gitlab.com/toccoag/ansible/-/blob/master/roles/ssh-key-sync/files/ssh_keys
.. _roles/ssh-server/files/authorized_keys_emergency: https://gitlab.com/toccoag/ansible/-/blob/master/roles/ssh-server/files/authorized_keys_emergency
.. _tocco_hieradata repository: https://git.vshn.net/tocco/tocco_hieradata/tree/master
.. _People page: https://github.com/orgs/tocco/people
.. _Members page: https://gitlab.com/groups/toccoag/-/group_members
.. _Docker Hub's Members page: https://hub.docker.com/orgs/toccoag
.. _Read the Docs' Users page: https://readthedocs.org/dashboard/tocco-docs/users/
