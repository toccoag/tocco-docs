Repositories
============

List of all important repositories that require long term maintenance.

+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| Name                      | Link                                                            | Responsible  | Description                                                        |
+===========================+=================================================================+==============+====================================================================+
| nice2                     | https://git.tocco.ch/#/admin/projects/nice2                     | Dev          | Main repository for the Nice2 project                              |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| nice2_documentation       | https://git.tocco.ch/#/admin/projects/nice2_documentation       | Tec. authors | Content for the manual and the specification of the Nice2 project  |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| nice2_documentation_build | https://git.tocco.ch/#/admin/projects/nice2_documentation_build | DevOps       | Build tool for the documentation (manual and specification)        |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| heretto-pdf-template      | https://gitlab.com/toccoag/heretto-pdf-template                 | Dev          | Template for the PDF version of our manual                         |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| docs-dita-ot-html-plugin  | https://gitlab.com/toccoag/docs-dita-ot-html-plugin             | Dev          | Customize generation of HTML version of manual in Heretto CMS      |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| docs-publish              | https://gitlab.com/toccoag/docs-publish                         | Dev          | Deployment and hosting of the manual managed in tocco.easydita.com |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| tocco-client              | https://gitlab.com/toccoag/tocco-client                         | Dev          | New React client                                                   |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| tocco-storybook           | https://gitlab.com/toccoag/tocco-storybook                      | Dev          | Storybook of the new client                                        |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| tocco-docs                | https://gitlab.com/toccoag/tocco-docs                           | DevOps/Dev   | DevOps/Dev documentation on https://docs.tocco.ch                  |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| ansible                   | https://gitlab.com/toccoag/ansible                              | DevOps/Dev   | DevOps documentation on https://devops-docs.tocco.ch/              |
|                           |                                                                 |              | Automation scripts for the infrastructure                          |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| openshift-nginx           | https://gitlab.com/toccoag/openshift-nginx                      | DevOps       | Nginx OpenShift Docker image                                       |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| tocco-dotfiles            | https://gitlab.com/toccoag/tocco-dotfiles                       | DevOps       | Tocco-specific application configuration and scripts               |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| maintenance-page          | https://gitlab.com/toccoag/maintenance-page                     | DevOps       | Temporary placeholder container during migrations                  |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| test-after-deployment     | https://gitlab.com/toccoag/test-after-deployment                | Dev          | Automated Cypress test to check basic functions after a deployment |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| commit-info-service       | https://gitlab.com/toccoag/commit-info-service                  | Dev          | Service on https://commit-info-service.tocco.ch to retrieve        |
|                           |                                                                 |              | information about commits                                          |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| atlassian-connect-        | https://gitlab.com/toccoag/atlassian-connect-integration        | Dev          | Service on https://commit-info-service.tocco.ch/jira/* for our     |
| integration               |                                                                 |              | plugins which are integrated in our Jira cloud service (see        |
|                           |                                                                 |              | https://commit-info-service.tocco.ch/atlassian-connect.json)       |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| image-service             | https://gitlab.com/toccoag/image-service                        | Dev          | Service for thumbnail generation                                   |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| address-provider          | https://gitlab.com/toccoag/address-provider                     | Dev          | Service for zip/city auto completion                               |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| wkhtmltopdf-binary        | https://gitlab.com/toccoag/wkhtmltopdf-binary                   | Dev          | wkhtmltopdf-binary provides wkhtmltopdf binary packaged in a Jar   |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
| country-initial-value     | https://gitlab.com/toccoag/country-initial-value-generator      | Dev          | Script generates/updates the Tocco initial values for countries    |
+---------------------------+-----------------------------------------------------------------+--------------+--------------------------------------------------------------------+
