
.. contents::
    :depth: 4
    :class: tocco-doc-contents

.. _cypress-tests:

Cypress (End-to-End-Test)
=========================

Local Cypress Setup
-------------------

  - Setup Nice2 (See :ref:`Nice2 Setup <clone_and_build_nice2>`)

  - Setup local postgres database (See :ref:`Postgres Setup <setup-postgres>` and `Postgres User`_)

  - Setup ``.env`` file (See :ref:`Setup <setup-node-version>` and `Environment variables`_)

Postgres User
^^^^^^^^^^^^^
To ensure that Cypress scripts are able to log into the postgres server without using a password prompt one of the three settings has to be set up:

1. set *trust* authentication method

2. create ``~/.pgpass`` for postgres database and postgres user

3. set ``POSTGRES_PASSWORD`` in ``.env`` (see `Environment variables`_)

Authentication Trust Method
"""""""""""""""""""""""""""

- set ``trust`` method in ``pg_hba.conf`` for local

- restart ``postgresql``

.pgpass file
""""""""""""

.. _pgpass file: https://www.postgresql.org/docs/current/libpq-pgpass.html

- create ``~/.pgpass`` file (see: `pgpass file`_)

- add entry for postgres user on postgres database

  .. code-block:: text

    # hostname:port:database:username:password
    localhost:5432:postgres:postgres:postgres

- set correct file permission

  .. code-block:: sh

    chmod 0600 ~/.pgpass

Environment variables
^^^^^^^^^^^^^^^^^^^^^

A .env file in the root folder is needed so that test can authenticate to the nice2 backend.

+----------------------------+-----------------------------------------------------+
| Variable                   | Description                                         |
+============================+=====================================================+
| CYPRESS_USER               | Tocco user name                                     |
+----------------------------+-----------------------------------------------------+
| CYPRESS_PASSWORD           | Password for Tocco user                             |
+----------------------------+-----------------------------------------------------+
| CYPRESS_USER_PASSWORD_HASH | The hash for the ``CYPRESS_PASSWORD``               |
+----------------------------+-----------------------------------------------------+
| CYPRESS_API_KEY            | Tocco API Key (needed for test-data setup via REST) |
+----------------------------+-----------------------------------------------------+
| CYPRESS_USER_API_KEY_HASH  | The hash for the ``CYPRESS_API_KEY``                |
+----------------------------+-----------------------------------------------------+

.. note::
  The values for the Cypress environment variables can be found in the ansible vault.

Furthermore there are **optional environment variables** which can be set to overwrite some default values.

+------------------------------+-----------------------------------------------------+-----------------------------+
|   Variable                   | Description                                         |   Default value             |
+==============================+=====================================================+=============================+
| POSTGRES_USER                | postgres superuser (for test db restore and setup)  | *none* (login user is taken)|
+------------------------------+-----------------------------------------------------+-----------------------------+
| POSTGRES_PASSWORD            | pwd for postgres superuser [#fpgpas]_               | *none*                      |
+------------------------------+-----------------------------------------------------+-----------------------------+
| HIBERNATE_MAIN_SERVERNAME    | postgres hostname                                   | ``localhost``               |
+------------------------------+-----------------------------------------------------+-----------------------------+
| HIBERNATE_MAIN_USER          | postgres user for test database [#fuser]_           | ``nice``                    |
+------------------------------+-----------------------------------------------------+-----------------------------+
| HIBERNATE_MAIN_PASSWORD      | postgres user password for test database            | ``nice``                    |
+------------------------------+-----------------------------------------------------+-----------------------------+
| HIBERNATE_MAIN_DATABASENAME  | postgres test database name [#fdb]_                 | ``test_cypress``            |
+------------------------------+-----------------------------------------------------+-----------------------------+
| HIBERNATE_MAIN_SSLMODE       | nice2 ssl mode                                      |  ``disable``                |
+------------------------------+-----------------------------------------------------+-----------------------------+
| BACKEND_URL                  | nice2 hostname                                      | ``http://localhost:8080``   |
+------------------------------+-----------------------------------------------------+-----------------------------+
| CYPRESS_BASE_URL             | nice2 hostname (officical Cypress env)              | ``http://localhost:8080``   |
+------------------------------+-----------------------------------------------------+-----------------------------+

.. rubric:: Explanation

.. [#fpgpas] when authentication method is not ``trust`` and no ``~/.pgpass`` file is set up properly the ``POSTGRES_PASSWORD`` has to be provided for the ``POSTGRES_USER``

.. [#fuser] user is created if it does not exist and set as owner of the test database

.. [#fdb] test database is created from scratch everytime and does not have to exist

Run Cypress
-----------

.. warning::
  Before you start ensure that you've properly set up Cypress locally (`Local Cypress Setup`_).

.. code-block:: shell

  # start nice2 via gradle and widget-server on localhost:3000
  yarn cypress:nice2

  # or run both separately
  yarn nice2:run
  yarn widgets:serve

  # deploy local package to nice2 for testing purposes (OPTIONAL)
  yarn  nice2:deploy-package --package=admin

  # run cypress
  yarn cypress:run
  # or open cypress dashboard
  yarn cypress:open

.. note::
  Per default a database with name ``test_cypress`` is created right before running nice2.
  To overwrite the database name set ``HIBERNATE_MAIN_DATABASENAME`` environment variable (see `Environment variables`_).

Writing Cypress Tests
---------------------

With Cypress the admin and the widgets can be tested. But for the widgets there are some special helpers.

Do's and Dont's
^^^^^^^^^^^^^^^

.. _Cypress Best Practices: https://docs.cypress.io/guides/references/best-practices

See also `Cypress Best Practices`_

data-cy attribute
"""""""""""""""""

Use ``data-cy`` attribute to select a elements.
Using keys or unique identifiers helps to get the correct element out of many (e.g. ``data-cy="list-row-1229"``).
Do not use CSS classes for element selection except for external libraries.

**data-cy naming**

+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| Prefix                     | Description                                                                     | Examples                             |
+============================+=================================================================================+======================================+
| btn                        | Button                                                                          |  ``btn-confirmation``                |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| infobox                    | Infobox                                                                         |   ``infobox-newregistrations``       |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| export-item                | For different export items in the export action                                 |  ``export-item-phonelist``           |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| communication              | For different communication items in the communication action                   |  ``communication-sms``               |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| action                     | For the different action groups like new, delete, output, etc.                  |  ``action-delete``                   |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| action-item                | For the different actions                                                       | ``action-item-createTodo``           |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| list-row                   | Row in the table. Use the Entity-Id as the name                                 | ``list-row-1``                       |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| header-cell and list-cell  | Are used in the tables. The name should be the relation or field name.          | ``header-cell-firstname``            |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| form-field                 | Input fields in the admin. The name should be the relation or the field name.   | ``form-field-firstname``             |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| box                        | For the boxes in the detail-form                                                | ``box-masterdata``                   |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+
| admin-menuitem             | For the items in the admin menu                                                 | ``admin-menuitem-address``           |
+----------------------------+---------------------------------------------------------------------------------+--------------------------------------+

Create Cypress Tasks for seeding
""""""""""""""""""""""""""""""""

Only seed data via Cypress Tasks and do not run REST APIs inside a test directly.
Use the returned data for better testing:

.. code-block:: js

    cy.task('db:seed:entity-browser').then(response => {
      const {pk} = response
      visitEntityBrowser(pk)
    })

    cy.task('db:seed:user').then(user => {
      const {firstname} = user
      ...
      cy.getByAttr('input-detailForm-firstname').type(user.firstname)
      ...
    })

See `Test data`_ for more information.

Use Custom Commands
"""""""""""""""""""

Use custom commands instead implement common actions on each test (e.g. ``cy.getTableRow(0)``).

See `Custom Commands / Tasks`_ for more information.

Widgets
^^^^^^^

Widgets are running on ``localhost:8080/widget-server``.

Use following helpers for widgets in order to manage the origin handling in Cypress.

visitWidget
"""""""""""

Do not use ``visit`` in widgets. Do use ``visitWidget`` to open a widget by a widget config key.

.. code-block:: js

      cy.visitWidget(widgetConfigKey)

Test data
^^^^^^^^^

.. _Cypress Tasks: https://docs.cypress.io/api/commands/task

Test data should be added via REST API inside a `Cypress Tasks`_.

- Each test data task should be prefixed with ``db:seed:``.
- A test data task can be named and created for generic purposes or for single tests/specs only.
- If possible the task should return the created IDs and as much data as possible.

Test data recoder
"""""""""""""""""
.. _Tocco Chrome Extension: https://gitlab.com/toccoag/tocco-browser-extension

To be more efficient in creating the database seed task there is a test data recoder in the `Tocco Chrome Extension`_.
The recorder can be used to create REST API calls easily. It also transforms references to ids into unique_ids if available.


Custom Commands / Tasks
^^^^^^^^^^^^^^^^^^^^^^^

.. _Custom Cypress Commands: https://gitlab.com/toccoag/tocco-client/-/tree/master/cypress/support

`Custom Cypress Commands`_ can be added for generic Tocco functionality (e.g. login or breadcrumbs validation).
Each command should be listed in the ``index.d.ts`` file to enable code intellisense for those commands.

db:empty-Task
"""""""""""""

Every test should be independent and not rely on any data inside the database.
Before each test the database should be emptied to start on a fresh db.

.. code-block:: js

  cy.task('db:empty')

login
""""""

A test should not log in via UI. Use login helper instead.

.. code-block:: js

  cy.login()

logout
""""""

A test should not log out via UI. Use logout helper instead.

.. code-block:: js

  cy.logout()

getByAttr
"""""""""

To select HTML element ``data-cy`` attributes should be used. CSS classes are not allowed except for external libraries.
The ``getByAttr`` helper creates a query selector for the ``data-cy`` attribute.

.. code-block:: js

  cy.getByAttr('admin-nav')

Cypress Report
^^^^^^^^^^^^^^

The Cypress report is created during a test run and is saved on ``cypress/export/testcases.md`` locally.
On Gitlab the Cypress report is publishd through the Gitlab pages after each run (see https://toccoag.gitlab.io/tocco-client).

.. note::
  Each highest level ``describe`` and each ``it`` or ``test`` are reported to the Cypress report and should be in German.


logStep
"""""""

On the test itself the individual steps should be reported as well.

.. code-block:: js

  cy.logStep('Aktion "E-Mail" ausführen')


Cypress on CI
-------------

.. _Nice2 Registry: https://gitlab.com/toccoag/nice2/container_registry/2866479
.. _Postgres Registry: https://gitlab.com/toccoag/nice2/container_registry/2866474

On CI (gitlab) the nice2 and postgres instances are running in Docker: `Nice2 Registry`_, `Postgres Registry`_.

A test run saves the generated video artefacts for one week. Those artefacts can be found on the executed pipeline for further failure investigation.

Debugging on Gitlab
^^^^^^^^^^^^^^^^^^^

Some Cypress issues only occur on Gitlab. Sometimes the nice2 log contains valuable information on how to fix the issue.
However per default the nice2 log is not visible. Gitlab offers a way to show logs generated by applications
running in service containers in the normal job log. This option should only be used for debugging as it has
negative storage and performance consequences. To enable service logging, add the ``CI_DEBUG_SERVICES`` variable
to the project's ``.gitlab-ci.yml`` file or directly as variable during manually triggering a pipeline.
See https://docs.gitlab.com/ee/ci/services/#capturing-service-container-logs for more details.

Process
-------

Run Nice2
^^^^^^^^^

#. Restore DB from existing DB dump (create empty DB when no dump is available)
#. Run DB refactoring
#. Run Nice2
#. Initialize DB

   #. Crate Tocco User and API Key for Cypress (See `Environment variables`_)
   #. Create DB dump

Test Run
^^^^^^^^

#. Empty DB

   #. Forcefully restore DB from existing DB dump (created on nice2 start up)

#. Seed DB via REST API
#. Login
#. Run Test

