Client
======

.. toctree::
   :maxdepth: 2
   :glob:

   project-structure
   development
   cypress
   npm-registry
   dependencies
   contribute
   changelog
   code-styleguide
   custom-widgets
   custom-actions
   customer-actions-widgets
   styling
   ci
   webpack
   architecture/index
