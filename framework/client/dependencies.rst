External Dependencies
=====================

NPM dependencies should only be added to the root ``package.json`` if they are required in multiple packages. Otherwise,
add them in the package where they are used.

Fullcalendar
------------

The ``scheduler`` package is using the ``fullcalendar`` package (`https://fullcalendar.io/ <https://fullcalendar.io/>`_).

This package will be renewed every year around 12. January.

To renew the license key execute following steps:

- update ``fullcalendar_license_key`` variable in ansible ``secrets2.yaml`` and set current version in above comment
- update ``FULL_CALENDAR_LICENCE`` variable in CI/CD Pipelines on `gitlab <https://gitlab.com/toccoag/tocco-client/-/settings/ci_cd>`_
- inform other devs (e.g. inside #technick slack channel) to update their ``.env`` file as well

.. _update_client_dependencies_on_a_regular_basis:

Update client dependencies on a regular basis
---------------------------------------------

The command ``yarn outdated`` lists all dependencies with new versions available. Run this for all of our packages and
update each dependency that gets listed to its latest version. Run a quick test to see if there are any obvious errors
after the upgrade and commit each upgrade on its own. Feel free to group the different commits into a few sensible
merge requests. Any upgrade that takes considerable time to either implement due to API changes or fixing bugs after
testing should be shelved and an issue in Jira should be created.
