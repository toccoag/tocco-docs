Project Structure
==================
Monorepo
--------
tocco-client is a monorepo and uses `Lerna`_ and `Yarn Workspaces`_.

.. _Lerna: https://lerna.js.org/
.. _Yarn Workspaces: https://classic.yarnpkg.com/lang/en/docs/workspaces/

All packages are located within the ``packages`` folder. Some export an independent react app, others act as a util package with components and helper functions.
A package that export a self containing react app is called a tocco-app. These tocco-apps, in contrast to the util ones, can be released and are easily identified by the absent `private` flag in the ``package.json``.
Every package maintains its own dependencies and can be re-used in other packages. With all due caution against introducing circular dependencies!
A description of each package can be found in its ``README.md``.

Package Folder Structure
^^^^^^^^^^^^^^^^^^^^^^^^

- core

  - unpublished packages and utils

  - core packages should not have dependencies to other packages (apps, actions or widgets)

- actions

  - unpublished and published actions, only published when used in legacy client

  - action packages can only have dependencies to widget and core packages

- widgets

  - published widgets, can be used on external sites

  - widget packages can have dependencies to action and core packages

- apps

  - published standalone apps (tocco-app)

  - app packages can have dependencies to all other packages

- bundles

  - published package containing different packages

  - only dependencies to apps or widgets



Package Naming
^^^^^^^^^^^^^^
Please ensure that every package is prefixed with ``tocco-``

``tocco-...`` naming is used in ``package.json``; in folder structure ``tocco-`` prefix can be omitted.


Create New Package
^^^^^^^^^^^^^^^^^^
A plop template can be used to create the essentials of a new package.

.. code-block:: console

 yarn plop Package {PACKAGE_NAME}


Add / Remove local or remote package as dependency:

.. code-block:: console

 yarn workspace tocco-{PACKAGE_NAME} add {DEPENDENCY}

 yarn workspace tocco-{PACKAGE_NAME} remove {DEPENDENCY}
 