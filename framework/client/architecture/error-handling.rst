Error Handling
==============

React Error Boundaries
----------------------

For further information see `official documentation <https://reactjs.org/docs/error-boundaries.html>`_.

- Only the bigger/core UI sections are wrapped in error boundaries (e.g. Navgiation, Searchpanel).
- The user should be able to partially use Tocco when an error occured.

When an error occured during render cycle the error boundaries will catch those and

- log to console
- log to remote
- show notification to user
- show fallback component instead

Usage
^^^^^

.. warning::
  Ensure that ``errorLogging.addToStore`` is properly set up in ``main.js`` of your app.

.. warning::
  The error boundary is in the ``errorLogging`` module in ``tocco-app-extensions`` and can therefore not be used inside ``tocco-ui``.


.. code-block:: jsx

  import {errorLogging} from 'tocco-app-extensions'

  const MyComp = () => (
    <errorLogging.ErrorBoundary>
      ...
    </errorLogging.ErrorBoundary>
  )

Currently we use the error boundaries in following scenarios:

- Navigation
- Breadcrumbs
- Entity List

  - List
  - Searchpanel
  - Action Bar

- Entity Detail

  - Detail
  - Relation

- Dashboard

  - Info Box

- Resource Calendar

  - Calendar View
  - Sidepanel


