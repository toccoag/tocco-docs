Caching
=======

There are two types of caches:

- Object cache

- Long term cache (``localestorage``)

The cache gets invalidated as soon as one of the dependencies has changed or a force invalidation takes place.

Only when "reload configuration action" has been invoked the cache gets forcefully invalidated.

The cache gets initialised on app mount and only re-initialised on:

- business unit change
- login via username/password
- login via SSO

Object cache
------------

Data that depends on:

- user / username
- business unit
- locale
- revision

Object cache is **isolated per app** instance and will get lost on page refresh.

Object cache contains:

- entity model
- form model
- displays

Long term cache
---------------

Data that depends on:

- revision

Long term cache is **shared across apps** within the same domain.

Long term cache contains:

- text resources by locale
- sso available indicator
- available locales
