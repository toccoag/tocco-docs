Architecture
============

.. toctree::
   :maxdepth: 1
   :glob:

   *

app-extensions vs. tocco-util
-----------------------------

Both are core packages that provide helper- and util-functions for other packages.

- app-extensions 

  - bigger features that are used in multiple apps

    - e.g. form, notifications

  - has dependency to ``redux``, ``tocco-util`` and ``tocco-ui``

- tocco-util

  - small independent helper functions

    - e.g. cache, logger

  - has no dependencies

.. _add_to_store:

addToStore
----------


.. list-table::
   :header-rows: 1
   :widths: 5 20 5

   * - Name
     - Description
     - Docs
   * - **actionEmitter**
     - Dispatches actions to the parent app via ``emitAction`` event.
     - `README actionEmitter <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#actionEmitter>`_
   * - actions
     - For rendering action buttons and invoke actions.
     - `README actions <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#actions>`_
   * - cache
     - Clears cache on initialization. (Usually in use for standalone apps, widgets and actions.)
     - `README cache <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#cache>`_
   * - errorLogging
     - To log errors and use ``ErrorBoundary``.
     - `README errorLogging <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#errorLogging>`_
   * - **externalEvents**
     - Dispatches external events / callbacks (such as ``onSuccess``, ``onError``, ``onCancel``).
     - `README externalEvents <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#externalEvents>`_
   * - form
     - To use detail form actions / sagas for `redux-form`.
     - `README form <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#form>`_
   * - formData
     - To use form data and form features such as remote fields.
     - `README formData <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#formData>`_
   * - keyDown
     - To use ``KeyDownWatcher`` and register shortcuts.
     - `README keyDown <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#keyDown>`_
   * - login
     - Checks session and connect to socket.
     - `README login <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#login>`_
   * - **notification**
     -  Dispatch actions to show notifications and confirmation dialogs.
     - `README notification <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#notification>`_
   * - reports
     - To load reports.
     - `README reports <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#reports>`_
   * - templateValues
     - To use template values to prefill form.
     - `README templateValues <https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/app-extensions/README.md#templateValues>`_

