.. |br| raw:: html

    <br>


Development
===========

.. _setup-client:

Setup
-----
.. _setup-node-version:

- Install Node

  .. warning::
    **Node version per release**

      ============== ===============
      Node Version   Nice Versions
      ============== ===============
      v16            3.1 - 3.5
      v20            3.6 - 3.10
      v22            3.11 -
      ============== ===============

    You can use Node v20/v22 for <= 3.5 if you pass ``--openssl-legacy-provider`` as argument (e.g. ``yarn start --package=admin --openssl-legacy-provider``.)

- Install and enable `Node Corepack`_.

  .. warning::
    Yarn is installed via package manager ``Corepack`` from Node. Therefore do **not** install Yarn with a os package manager.
    
- Setup environment variables

  These environment variables are used in the ``.yarnrc.yml`` file:

  =============================== ================================================================= ================================================
  Key                              Value                                                             Usage
  =============================== ================================================================= ================================================
  ``FONTAWESOME_NPM_AUTH_TOKEN``    ansible vault                                                    **Mandatory** - used for installing packages
  =============================== ================================================================= ================================================

  With osx the variables can be added with:

  .. code-block:: console
    
    launchctl setenv FONTAWESOME_NPM_AUTH_TOKEN {TOKEN}


  or with linux: 

  .. code-block:: console
    
    FONTAWESOME_NPM_AUTH_TOKEN={TOKEN}


  Make sure to check if the variable is available. Maybe a restart is required.

- Clone ``tocco-client``:

  .. code-block:: console

    git clone git@gitlab.com:toccoag/tocco-client.git
    cd tocco-client
    yarn install

- Setup ``.env`` file:

  .. code-block:: console

    yarn plop Env


.. _Node Corepack: https://yarnpkg.com/corepack#installation


- Install VS Code on your machine. Useful plugins which you can install:

  - Prettier (:ref:`more details <prettier>`)
  - ESLint (:ref:`more details <eslint>`)
  - Jest


Code Styleguide
----------------

See :ref:`CodingStyleguide` 


Start a package
----------------

.. warning::
  :ref:`Check if you have the correct node version for the current release. <setup-node-version>`

.. note::
  Whenever you change branches you need to run ``yarn install``.


.. code-block:: console

  yarn start --package={package-name} --backend={backend-url}

  yarn start-widget --key={widget-config-key} --backend={backend-url}

Open http://localhost:3000 and wait for a finished build.

.. hint::

  When starting a widget with ``yarn start-widget`` you can navigate to http://localhost:3000/wp for WordPress simulation or to http://localhost:3000/legacy for legacy cms simulation.

.. note::

  ``start-widget`` command will retrieve the widget config and start the widget package automatically.
  Otherwise the widget can be started with ``yarn start --package={widget-package-name}`` as well. But then de ``./dev/input.json`` has to be filled in with dummy config data.

.. note::

  Hot reloading will allow parts of the application to be live reloaded when the source code changes. 
  Keep in mind that hot reloading will not work for sagas.



Optional parameters


=========================== ============================
``--backend={BACKEND_URL}``  Enable an alternative backend. E.g. master deployment of Tocco. (default: ``http://localhost:8080``)
=========================== ============================

This command will locally start the admin and uses the master deployment as backend.

.. code-block:: console

  yarn start --package=admin --backend=https://master.tocco.ch

Make sure that the selected backend has both application properties ``nice2.web.cookie.sameSite=None`` and ``nice2.web.allowedRequestOrigins=http://localhost:3000`` set.
Otherwise you will get CORS errors when trying to connect to it. By default, these properties are only set for our test customer installations.

Deploy package to local backend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For testing purposes it's useful to test a package (admin, action or widget) in production mode.
Therefore it's easiest to deploy the package to the locally cloned nice2.

.. code-block:: bash

  yarn nice2:deploy-package --package={package-name}

This script gets the nice2 repository folder with the same tocco version and copies the generated production build inside the repo folder.

.. warning::

  This script needs the environment variable ``NICE2_REPO_BASE_PATH``. The ``NICE2_REPO_BASE_PATH`` can be added to the ``.env`` file inside the tocco-client root folder and has to be set to the parent folder of all nice2 repositories.

.. note::

  The deploy package script for bundles can be extended with ``--app`` argument to define which apps should be bundled. The build time can be significantly decreased and it's useful for testing purposes when only working on one or a few apps.
  
  Example:

  .. code-block:: bash

    yarn nice2:deploy-package --package=widget-bundle --app=entity-browser --app=docs-browser

Storybook
---------
It might be helpful to start up Storybook locally to test the current state of development. 
Most of the components or packages have a dedicated story to run them isolated.
Storybook can be started with the following 
command:

.. code-block:: bash

  # run against local nice2
  yarn storybook

  # run against https://master.tocco.ch
  yarn storybook:master

Use ``BACKEND={BACKEND_URL} yarn storybook`` to enable an alternative backend.

Add new story file
^^^^^^^^^^^^^^^^^^

 - suffix filename with ``*.stories.js``

 - add package in ``stories`` list in ``storybook/main.js``

Unit Tests
----------

Tests are using following tools and libraries:

* `Jest`_
* `Sinon`_
* `Chai`_
* `ReactTestingLibrary`_

.. _Jest: https://jestjs.io/
.. _Sinon: http://sinonjs.org/
.. _Chai: http://chaijs.com/
.. _ReactTestingLibrary: https://testing-library.com/docs/react-testing-library/intro


Run unit tests with Jest via lerna

.. code-block:: console

  yarn test


Run all unit tests directly (not via lerna)

.. code-block:: console

  yarn test:jest


Optional parameters

======================================= ============================
``--watch``                              Run jests watch mode
======================================= ============================


.. note::
 If working with IntelliJ single tests or test-suites can be run in the IDE directly. Just set the jest.config.js file in the Jest run configuration. 

Chai Assertions
^^^^^^^^^^^^^^^

.. _Chai Assertions: https://www.chaijs.com/api/bdd/

We use BDD (Behaviour Driven Development) assertions with the base of ``expect``.

See: `Chai Assertions`_ for the API


React Testing Library
^^^^^^^^^^^^^^^^^^^^^

The React Testing Library (RTL) only provides a full render function.
In addition the RTL operates on the outputted html and React components cannot be used for any assertion or prop updates.


Render without redux
""""""""""""""""""""

.. code-block:: jsx
  
  import {screen} from '@testing-library/react'
  import {testingLibrary} from 'tocco-test-util'

  it(() => {
    testingLibrary.renderWithIntl(<MyComp />)
    expect(screen.getAllByText('client.comp.textid')).to.exist
  })


Render with redux
"""""""""""""""""

.. code-block:: jsx

  import {testingLibrary} from 'tocco-test-util'

  import reducers, {sagas} from '../../modules/reducers' // import reducers and sagas from package

  it(() => {
    const input = {}
    const store = appFactory.createStore(reducers, sagas, input)
    store.dispatch(...) // dispatch actions to prefill state
    testingLibrary.renderWithStore(<MyComp />, {store})
  })

Prevent render with mocking
"""""""""""""""""""""""""""
When a child component should not render itself it can be mocked.
This can be useful if only the existens of a component is important but not its behaviour itself.

.. code-block:: jsx

  import {screen} from '@testing-library/react'
  import {testingLibrary} from 'tocco-test-util'

  jest.mock('tocco-entity-list/src/main', () => () => <div data-testid="entity-list" />)
  
  it(() => {
    testingLibrary.renderWithIntl(<MyComp />)
    expect(screen.getByTestId('entity-list')).to.exist
  })


End-to-End Tests
----------------

.. _Cypress: https://www.cypress.io/

End-to-End (e2e) tests are written and run with `Cypress`_.

See :ref:`Cypress (End-to-End-Tests) <cypress-tests>` for more detailed information.

Code Generators
---------------
The project provides some code generators. Generators are developed with `Plop`_ and can be executed with:

.. code-block:: console

  yarn plop

There are the following generators:

- Package

  - used for new packages, creates all file structure for a package

- Env

  - used initially for set up the environment, initiate a .env file with your environment variables

- Bundle app

  - used for adding a package to a bundle (e.g. a widget to the ``widget-bundle``)


.. _Plop: https://github.com/amwmedia/plop


Build bundle
------------
Sometimes it's desired to only build a package for testing purposes.

.. code-block:: console

    yarn compile:dev --package={package-name}
    yarn compile:prod --package={package-name}

Parameters

=========================== ============================
``--bundle-analyzer``        Opens BundleAnalyzerPlugin to investigate the bundle sizes.
``--backend={BACKEND_URL}``  To enable an alternative backend.
=========================== ============================

