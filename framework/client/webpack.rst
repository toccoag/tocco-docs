Webpack
========
The tocco-client uses webpack to bundle the code. 
All packages use the same webpack configuration that is located in the *build* folder. But its possible to add a custom config enhancer for each package. As of now this is only used by sso-login.
The main property is the package name and each tocco-app can be bundled separately. 
The other important property is the mode. Since we have to differ between production and development mode. For example is there no need to add hot-reloading for production.

Some code of the original boilerplate is still in use because there was never a need to update it. 
Nevertheless keeping webpack up to date is important to keep the repo vital. When changes are made to this rather fragile setup, the following things should be checked:

* Is it possible to build (compile:prod) all packages. Admin and resource-scheduler are good tests.
* Is the build time still ok
* Is code splitting still working and are the bundle sized roughly the same or smaller 
* Is is possible to start a package locally. With a real backend and a mocked one.
* Is hot reloading still working. Changing the tocco-ui browser and check if it gets reloaded automatically is a good test.
* Is it possible to start storybook 
* Do all tests still run successfully 
* Is cypress running
* Does the CI run successfully
* Do the apps still load when integrated into the old admin 
