Customer widgets or actions (dynamic actions)
=============================================

- Customer widgets are widgets that are only available for specific customer(s).
- Customer actions are custom actions that are only available for specific customer(s). Therefore the action itself is not bundled into the admin or any widget.
  
  - These actions are called `dynamic actions` because they get loaded on runtime dependent on the current customer.
  - A customer action, which is only available inside a customer widget, does not have to be implemented via dynamic action and can be added to the customer widget directly.

The widget or action itself can be implemented as any other custom widget/action.

- See :ref:`Client - Custom widget <custom_react_widgets>` for further information.
- See :ref:`Client - Custom actions <custom_react_actions>` for further information.

.. warning::

    A customer widget should **not** be added to the ``tocco-widget-bundle``.

    A dynamic action should **not** be added to the ``tocco-admin``.

Folder structure
----------------
Each customer should have its own folder inside ``customers`` with subfolder such as:

- ``bundles``
- ``actions``
- ``widgets``

**The folder name of the customer itself should have the same name as the customers folder name in nice2.**

Example:

- packages

  - customers

    - *<customer>*

      - bundles
      - actions
      - widgets

Customer bundle
---------------
Each customer has its own bundles (usually one: ``<customer>-bundle``). All customer actions and widgets should be added to this bundle.
The bundle will be publish and added as a dependency in the customers ``package.json`` in nice2 so that it gets deployed on the customers tocco instance.

Bundle Package
^^^^^^^^^^^^^^
If there is no existing bundle package for the customer it can be created with ``plop``

.. code-block:: bash

  yarn plop Bundle-Package

The name of the bundle package should always be like ``<customer>-bundle``.

.. warning::
  
  **CKEditor error handling**

  When the bundle does not include the CKEditor code the ``CKEditorWebpackPlugin`` has to be removed from the webpack config.

  Following compile error is an indication for this situation:
  
  .. code-block:: bash

    HookWebpackError: No file was matching the `translationsOutputFile` option.

  To remove the plugin adjust the ``/build/webpack.js`` file in your bundle package and add the ``removeCKEditor`` helper function:

  .. code-block:: jsx
    
    import {adjustConfigForBundles, removeCKEditor} from '../../../../../../build/lib/webpack'

    export const adjustConfig = (webpackConfig, ...args) => adjustConfigForBundles(removeCKEditor(webpackConfig), ...args)

Add bundle to customers dependencies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Add ``customer/<customer>/resources/resources/webapp/package.json`` (if customer is on nice version 3.5 set ``nice35`` as ``<version>``):

.. code-block:: json

  {
    "dependencies": {
      "tocco-<customer>-bundle": "<version>",
    }
  }

Add action/widget to bundle
^^^^^^^^^^^^^^^^^^^^^^^^^^^
The action/widget should be added to the customers bundle via ``plop``.

.. code-block:: bash

  yarn plop Bundle app

Dynamic action
--------------
A dynamic action gets loaded on runtime via ``nice2/rest/client/actionPackages`` endpoint.

Add a contribution in the ``Nice2Application.java`` (e.g. ``<bundle_name>``: ``customer-bundle`` and ``<package_name>``: ``customer-my-action``):

.. code-block:: java

    @Bean
    public ClientActionPackageContribution <package_name>ClientActionPackageContribution() {
        return new ClientActionPackageContribution("<package_name>", "<bundle_name>");
    }

Prefix the contribution with the ``<package_name>``.

If necessary add ``implementation project(":core:rest:client")`` to ``build.gradle`` and ``requires nice.core.rest.client;`` to ``module-info.java``.
