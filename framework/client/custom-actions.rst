.. _custom_react_actions:

Custom Actions
==============
Custom actions are independent applications, tocco-apps, that can be executed in a modal window, as fullscreen actions or standalone (e.g. old client).
The API should be consistent and is explained here.

Action package
--------------

Create a new package for your action in the ``actions`` folder:


.. code-block:: bash

  yarn plop Package

The ``src/main.js`` is the entry point of the action.

Input / PropTypes
-----------------
Define the input prop types for the action.

.. note::

  The ``appContext`` prop type, which has been added automatically, can be removed for actions.

Example:

.. code-block:: jsx
    :caption: src/main.js

    import PropTypes from 'prop-types'
    import {selection} from 'tocco-app-extensions'
    import {navigationStrategy} from 'tocco-util'

    const EXTERNAL_EVENTS = ['onError', 'onCancel', 'onSuccess', 'emitAction']


    const initApp = (...) => {
      ...
    }

    ...

    const ExampleActionApp = props => {
      ...
    }

    ExampleActionApp.propTypes = {
        selection: selection.propType.isRequired,
        actionProperties: PropTypes.shape({
            someProperty: PropTypes.string.isRequired
        }),
        context: PropTypes.shape({}),
        navigationStrategy: navigationStrategy.propTypes.isRequired,
        ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
          propTypes[event] = PropTypes.func
          return propTypes
        }, {}),
    }


.. list-table::
   :header-rows: 1
   :widths: 5 15 30

   * - Name
     - Description
     - Example
   * - selection
     - Selected entities.
       Can either be an array of keys (type = id) or a query.

       The selection object will be passed automatically to the action.
     - keys

       .. code-block:: json

          {                                                         
            "count": 2,                                              
            "entityName": "User",                                    
            "ids": [                                                 
              "11081",                                                
              "11092"                                                 
            ],                                                       
            "length": 2,                                             
            "type": "ID"                                             
          }       

       query

       .. code-block:: json                                        

        {                                                         
          "count": 77,                                             
          "entityName": "User",                                    
          "query": {                                               
            "filter": ["user_active"],                              
            "where": "(fulltext(\"Tocco\") or fulltext(\"Tocco*\"))",
            "hasUserChanges": true                                  
            },                                                      
          "type": "QUERY"                                          
        }

   * - actionProperties
     - Can be passed from the form definition to the action. 
     - actionProperties

       .. code-block:: json

         {
           "widgetKey": "12",  
           "eventKey": "2"
         }


   * - context
     -  Can be passed by the app calling the action to provide some context information.

        As of now the viewName and formName are passed. But this object can be extended as needed.
     -  entity-list

        .. code-block:: json

          {
            "viewName": "list",  
            "formName": "UserSearch"
          }

        entity-detail

        .. code-block:: json

          {
            "viewName": "detail",
            "formName": "UserSearch"
          }
   * - navigationStrategy
     - An object containing utils to add navigation functionality. See `Navigation Strategy`_ on Gitlab for more details.
     -

.. _Navigation Strategy: https://gitlab.com/toccoag/tocco-client/blob/master/packages/core/tocco-util/src/navigationStrategy/navigationStrategy.js

app-extensions
--------------

If you need to communicate to the outside app, the packages ``actionEmitter``, ``externalEvents`` and ``notification`` from ``app-extensions``
will probably be useful to you.

See :ref:`"addToStore" section <add_to_store>` for a list of all pluggable ``app-extensions``.

Add all the needed extensions to your ``store`` in ``main.js``.

Example:

.. code-block:: jsx
    :caption: src/main.js

    import {appFactory, actionEmitter, externalEvents, notification} from 'tocco-app-extensions'

    const packageName = 'example-action'
    const EXTERNAL_EVENTS = ['onSuccess', 'emitAction']

    const initApp = (...) => {
      const content = <div />

      const store = appFactory.createStore(reducers, sagas, input, packageName)
      actionEmitter.addToStore(store, state => state.input.emitAction)
      notification.addToStore(store, false)
      externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

      return appFactory.createApp(packageName, content, store, {
        input,
        events,
        actions: [],
        publicPath,
        textResourceModules: ['component', 'common', packageName]
      })
    }


.. warning::

    Notifications will not be visible when starting the action itself. They are only visible inside the ``admin``.

   
Callback Events
---------------
The action get invoked with callback functions:

.. list-table::
   :header-rows: 1
   :widths: 5 30

   * - Name
     - Params object 
   * - onSuccess
     - *message {string}* optional string that is shown in an successful info box. 
       *remoteEvents {array}* List of remote events :ref:`Remote Events`    
   * - onError
     - *message {string}* optional string that is shown in an error info box.    
   * - onCancel
     -


.. _Remote Events:

Remote Events
^^^^^^^^^^^^^
Indicates what changed within the execution of an action. This helps the executing app to e.g. refresh or redirect.
At the moment a remote event consists of one of three types and the affected entities.

Types: 'entity-create-event', 'entity-delete-event', 'entity-update-event'

 .. code-block:: json          

  {
   "type": "entity-update-event",
   "payload": {
    "entities": [
       {"entityName": "User", "key": "1"}
      ]
    }
  }


