NPM Registry
==============

.. _npm-registry:


Authentication
---------------
We use the default `NPM Registry`_ to upload the packages. All developers use the same user on npm (user "tocconpm" with
email admin@tocco.ch). To setup the authentication execute following command:

.. code-block:: console

  npm adduser

The password/OTP can be found on in the ansible vault.

.. _NPM Registry: https://www.npmjs.com/

Auth Token
^^^^^^^^^^

.. _npm-registry-auth-token:

To be able to publish a package locally/remote you need to add an `Automation NPM Auth Token`_.

.. _Automation NPM Auth Token: https://docs.npmjs.com/requiring-2fa-for-package-publishing-and-settings-modification

Set the generated token as environment variable ``NPMJS_AUTH_TOKEN``. The ``.yarnrc.yml`` file is referencing this variable.
