Changelog
=========

.. _client_changelog:

3.11
----

- Yarn V4 update

  - package.json
  
    - ``prepublish`` script renamed to ``prepack``
    - use ``yarn run ...`` instead of ``npm run``


