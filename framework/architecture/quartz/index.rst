Quartz Scheduler
================

Manually scheduling a task
--------------------------

While batch-jobs are scheduled automatically, other background tasks (like sending mails) need to be
scheduled using the :nice:`TaskSchedulingService <ch/tocco/nice2/tasks/api/quartz/TaskSchedulingService>`.

    * ``jobClass`` is the class of the task that should be executed
    * ``taskName`` is the name of the job (used to be ``TaskData#setName()``)
    * ``taskType`` should be a valid ``Callable_type``
    * ``jobData`` is the equivalent of ``TaskData``. This data is persisted and available
      during task execution. The data is serialized using Xstream (as before) and supports
      custom data (``putString()`` and ``putObject()``) and also contains some methods to configure
      the environment (``putPrincipal()`` and ``putBusinessUnit()``).
    * ``executionDate`` point of time when the job should be started

Tests
-----

Tests for batch-jobs can be easily migrated to :nice:`EasyBatchjobTestCase <ch/tocco/nice2/tasks/test/easytest/testcase/EasyBatchjobTestCase>`.

See :ref:`Batchjob Testing <batchjob-test>` section.

Startup behaviour
-----------------

ch.tocco.nice2.enableUpgradeMode=true
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

During the database upgrade, the scheduler is completely disabled. It is not possible execute any tasks
and batch jobs will not be synchronized with the database.

UPDATE run environment
^^^^^^^^^^^^^^^^^^^^^^

During the UPDATE environment, the scheduler is started, but all batch-job triggers will be paused.
However explicitly submitted jobs will still be executed. The same behaviour can be achieved using
the ``ch.tocco.nice2.tasks.disable.persistent.task.scheduling`` property.
