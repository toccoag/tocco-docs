External Dependencies
=====================

Dependency management in root build.gradle
------------------------------------------

All external dependencies should be added to the ``dependencyManagement`` block in the root build.gradle. This way,
they can be managed in one single place which makes it much easier to keep them up to date.

Each dependency is added with the desired version number to the ``dependencyManagement`` block. Once defined there,
there is no version number needed in the dependency declaration in the actual module where the dependency is included.

Example
~~~~~~~

Imagine we need the dependency ``org.jsoup:jsoup`` in the module ``:optional:address``. We further assume,
that the current version of JSOUP is ``1.10.3``.

The dependency would be included as follows:

1. Add the dependency ``dependency "org.jsoup:jsoup:1.14.3"`` to the root build.gradle of the Nice2 project in the ``dependencyManagement`` block


2. Include the dependency ``implementation "org.jsoup:jsoup"`` in the ``build.gradle`` of the ``:optional:address`` module (don't specify a version).
   If the dependency is used in the api or spi package of the module the dependency should be added as api dependency (``api "org.jsoup:jsoup"``).

.. hint::

   If you need to manage a set of dependencies which share the same version number, it makes sense to define a
   variable for that version number ``gradle.properties`` (e.g. ``jsoupVersion=1.10.3``) which can be used for all
   dependencies in the ``dependencyManagement`` (e.g. ``dependency "org.jsoup:jsoup:${jsoupVersion}"``).

.. _update_backend_dependencies_on_a_regular_basis:

Update backend dependencies on a regular basis
----------------------------------------------

It's important to keep all dependencies up to date. Therefore, there's a weekly job on GitLab which generates a
report of all dependencies which are out-of-date. The job runs every Saturday.

Find the latest report:

#. Find *check for outdated dependencies* on the `Schedules`_ page.
#. Open the last pipeline (link in column *Last Pipeline*).
#. Open job *list-outdated-dependencies*
#. *Browse* job artifacts (right panel)
#. Open *report.txt*

.. _Schedules: https://gitlab.com/toccoag/nice2/-/pipeline_schedules
