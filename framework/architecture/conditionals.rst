Conditionals
============

.. note::

    This is mostly a direct copy from an old ``package-info.java`` file from the ``ch.tocco.nice2.conditionals.api.tree`` package. Feel free to
    rewrite or expand this documentation.

An API for tree-based conditionals. The classes in this package typically represent the `AST <http://en.wikipedia.org/wiki/Abstract_syntax_tree>`__ of
a conditional expression. It's currently used by both the query language and the ACL policy compiler.

Tree Processing
---------------

This API also contains a builtin facility for tree processing, which also allows to completely rewrite the whole tree. The entry point into this
processing facility is the :nice:`Node#process <ch/tocco/nice2/conditionals/api/tree/Node#process(ch.tocco.nice2.conditionals.api.tree.processing.NodeVisitor)>`
method and the :nice:`NodeVisitor <ch/tocco/nice2/conditionals/api/tree/processing/NodeVisitor>` class.

The :nice:`Cursor <ch/tocco/nice2/conditionals/api/tree/processing/Cursor>` is used to both control how the tree is being visited, including skipping
or re-visiting nodes or trees, and to add/remove/replace nodes or trees.


Annotations
-----------

Another important concept of this API are :nice:`annotations <ch/tocco/nice2/conditionals/api/tree/Node#annotate(java.lang.Class,java.lang.Object,T)>`.
Annotations are heavily used during analysis of the AST to keep track of various additional data and aspects. These annotations are usually very
specific to the language. A path or identifier e.g. may be with the object it references, the type of a resolved value etc.


Other Utilities
---------------

This API further provides facilities for error reporting and keeping track of the source code. See the classes 
:nice:`Problem <ch/tocco/nice2/conditionals/api/tree/problems/Problem>`,
:nice:`ProblemSink <ch/tocco/nice2/conditionals/api/tree/problems/ProblemSink>`,
and :nice:`ProblemFormatter <ch/tocco/nice2/conditionals/api/tree/problems/ProblemFormatter>`
for more information.