DMS
===

Security
--------

There is a permission matrix to grant/deny access to a domain/folder/resource. During saving a ``Node_right`` entity 
is created (except for the implicit grant access) which relates to a ``Node_permission`` (e.g. read, edit, delete, publish, etc.) 
and a ``Role``. Implicit permissions are light green in the permission matrix. 

For example, if no role has a delete permission for a folder, all roles implicitly get a delete permission. 
However, as soon as any role has a delete permission, the other roles no longer has an implicit delete permission.

Some logic is divided into regular documents (non entity docs) and entity docs. Next these cases are explained.

Regular documents
^^^^^^^^^^^^^^^^^

For each permission (except read) an ACL rule exists to grant access if the logged in principal has at least one role where access is granted.

For the read permission exists two PolicyProcessors, :nice:`FolderReadPermissionForNonAnonymous <ch/tocco/nice2/dms/impl/security/policyprocessor/FolderReadPermissionForNonAnonymous>` 
and :nice:`ResourceReadPermissionForNonAnonymous <ch/tocco/nice2/dms/impl/security/policyprocessor/ResourceReadPermissionForNonAnonymous>`, to efficiently grant access.

Entity docs
^^^^^^^^^^^

There are no ACL rules for entity docs. Access is granted via :nice:`FolderGuardFactory <ch/tocco/nice2/dms/impl/security/guard/FolderGuardFactory>` and 
:nice:`ResourceGuardFactory <ch/tocco/nice2/dms/impl/security/guard/ResourceGuardFactory>`.
If a principal has read access to the related entity (e.g. ``Classroom``), he has automatically read access to all published documents.
Read access cannot be denied (A document can only be set to offline). In the permission matrix is no read permission column. It is removed as the contributed :nice:`NodePermissionsContribution <ch/tocco/nice2/dms/api/security/NodePermissionsContribution>`
has the :nice:`NotEntityDocumentNodePredicate <ch/tocco/nice2/dms/impl/security/NotEntityDocumentNodePredicate>` predicate for the read permission.

If a principal has write access to the related entity (e.g. ``Classroom``), he has automatically write access to all (not only published) documents. If a principal has 
no write access for a document (but read access on the related entity), in the permission matrix write access can be granted. If a principal already has 
write access on the related entity, the write access cannot be denied in the permission matrix.

To be more precise the read/write access is checked on the relation of the related entity (``relResource_entitydocs`` or ``relFolder_entitydocs``).
It is possible to add additional ACL rules. For example an ordermanager can normally not edit entity docs on orders which are already booked. However with the ACL rule
``entityPath(Order, relResource_entitydocs, relFolder_entitydocs): grant access to ordermanager;`` the ordermanager can always edit these documents.    

The :nice:`ResourceContentGuardFactory <ch/tocco/nice2/dms/impl/security/guard/ResourceContentGuardFactory>` delegates the evaluation  of the ``Resource_content``
to the related ``Resource`` entity. If the ``Resource``, ``Resource_content`` or ``Folder`` is in the entity state ``CONCEPTION`` the :nice:`ConceptionResourceGuardFactory <ch/tocco/nice2/dms/impl/security/guard/ConceptionResourceGuardFactory>`,
:nice:`ConceptionResourceContentGuardFactory <ch/tocco/nice2/dms/impl/security/guard/ConceptionResourceContentGuardFactory>` or
:nice:`ConceptionFolderGuardFactory <ch/tocco/nice2/dms/impl/security/guard/ConceptionFolderGuardFactory>` is used.

If a entity is loaded, normally the security is checked as part of the query. However for entity docs a special :nice:`EntityDocsPrimaryKeyLoader <ch/tocco/nice2/dms/impl/entitydocs/interceptor/EntityDocsPrimaryKeyLoader>`
exists which checks the security throught the guards.

There are several custom implementations for loading entity docs :nice:`EntityDocsCollectionInitializer <ch/tocco/nice2/dms/impl/entitydocs/interceptor/EntityDocsCollectionInitializer>`,
:nice:`EntityDocsFolderCollectionInitializer <ch/tocco/nice2/dms/impl/entitydocs/interceptor/EntityDocsFolderCollectionInitializer>` and :nice:`EntityDocsRootFolderCollectionInitializer <ch/tocco/nice2/dms/impl/entitydocs/interceptor/EntityDocsRootFolderCollectionInitializer>`.
They manually check the permission and privileged load the entities (or primary keys). See :ref:`collection_initializer` on how they work.

Relation tab count
------------------

Normally just a count query is executed to get the number of related entities. :nice:`DmsRelationCounter <ch/tocco/nice2/rest/documents/impl/resources/DmsRelationCounter>` counts the number of elements
on the top level of the dms tree (so it's the same number as the dms list count).
