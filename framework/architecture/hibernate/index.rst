Hibernate Integration
=====================

.. toctree::
   :maxdepth: 2

   introduction
   session-factory-provider
   entity-class-generation
   abstract-pojo-entity
   collections
   java-types
   entity-persister
   custom-bytecode-provider
   generated-values
   session-lifecycle
   transaction-lifecycle
   entity-transaction-context
   listeners
   query-builder
   new-api
   large-objects
   memory-management
   sql-logging
