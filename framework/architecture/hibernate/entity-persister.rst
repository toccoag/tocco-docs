.. _persister:

Custom persisters
=================

Custom entity persister
-----------------------

The :java-hibernate:`EntityPersister <org/hibernate/persister/entity/EntityPersister>` interface contains information how to
map an entity to the database table. There is one instance per mapped class.
We extend Hibernate's default implementations to achieve some custom behaviour (:nice:`CustomEntityPersister <ch/tocco/nice2/persist/core/impl/hibernate/CustomEntityPersister>`).

.. _persister-entity-instantiation:

Entity instantiation
^^^^^^^^^^^^^^^^^^^^

By default Hibernate instantiates entity classes by invoking their default constructor.
Entity instantiation is intercepted by overriding ``EntityPersister#instantiate()``; the instantiation itself is then
delegated to the :nice:`EntityFactory <ch/tocco/nice2/persist/core/impl/hibernate/pojo/EntityFactory>`.
The :nice:`EntityFactory <ch/tocco/nice2/persist/core/impl/hibernate/pojo/EntityFactory>` injects services into
the created entities, tracks new entities and invokes listeners.

Before the entity factory is called it is verified whether the entity to be created belongs to the current
Session/Context. This is important as otherwise the wrong Session/Context would be injected by the entity factory.

UpdateCoordinator
^^^^^^^^^^^^^^^^^

Hibernate has a default implementation :java-hibernate:`UpdateCoordinatorStandard <org/hibernate/persister/entity/mutation/UpdateCoordinatorStandard>`.
Since hibernate 6 there is an optimization that ``handlePotentialImplicitForcedVersionIncrement`` checks 
if it is a simple version update and no other attributes than the version is changed.
However, in such a case the version is incremented but the update user and timestamp was not changed (see cases in ``UpdatingVersionAndUpdateUserTest``).
:nice:`ToccoUpdateCoordinatorStandard <ch/tocco/nice2/persist/core/impl/hibernate/ToccoUpdateCoordinatorStandard>` is a custom implementation
which always return null so that the optimization is disabled, and it works as before with hibernate 5.6.
 
Custom collection persister
---------------------------

There is an instance of a :java-hibernate:`CollectionPersister <org/hibernate/persister/collection/CollectionPersister>` for
every collection. Like with the entity persister, a customized implementation is used.

Filtered collections
^^^^^^^^^^^^^^^^^^^^

By always returning true from ``isAffectedByEnabledFilters()`` Hibernate assumes that the collection might be filtered.
Even though we do not use Hibernate's filter feature we use a similar concept (see :ref:`collections`).
When filters are enabled certain shortcuts cannot be used by Hibernate (for example removing all entries in a n:n
mapping table, which might wrongfully remove filtered data from the database).

Lazy initialization of CollectionLoader
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The :java-hibernate:`CollectionLoader <org/hibernate/loader/ast/spi/CollectionLoader>` instances are also lazily
initialized in :nice:`CustomBatchLoaderFactory <ch/tocco/nice2/persist/core/impl/hibernate/persister/CustomBatchLoaderFactory>`
for the same reasons as above (memory usage and startup performance).
