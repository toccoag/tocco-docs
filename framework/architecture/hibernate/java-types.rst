.. _java-types:

Custom java types
=================

Per default Hibernate can map all primitive types (and its wrapper classes) as well as references to other entities.
For all other classes that need to be mapped to the database a :java-hibernate:`JavaType <org/hibernate/type/descriptor/java/JavaType>`
must be implemented. The java type contains the logic how a specific object should be read and written.

For example the :nice:`Login <ch/tocco/nice2/types/api/Login>` class is mapped with a custom java type (:nice:`LoginJavaType <ch/tocco/nice2/persist/core/impl/hibernate/javatype/LoginJavaType>`).

*There are two ways to register a new java type:*

It can be registered with a bootstrap contribution (see :ref:`bootstrap`). This way should be used if
there is a distinct object which should be mapped (for example :nice:`Login <ch/tocco/nice2/types/api/Login>`):

.. code-block:: java

    classLoaderService.addContribution(TypeContributor.class, ((typeContributions, serviceRegistry) -> {
        JdbcTypeRegistry jdbcTypeRegistry = typeContributions.getTypeConfiguration().getJdbcTypeRegistry();
        typeContributions.getTypeConfiguration().getBasicTypeRegistry().register(
            new StandardBasicTypeTemplate<>(
                jdbcTypeRegistry.getDescriptor(Types.VARCHAR), new LoginJavaType(), Login.class.getName()
            )
        );
    }));

The alternative is to bind the java type to a field type (like ``phone``) using a contribution:

.. parsed-literal::

    @Component
    public class PhoneJavaTypeContribution extends JavaTypeFieldContribution<PhoneJavaType> {

        private final L10N l10n;
        private final PhoneFormatter phoneFormatter;

        public PhoneJavaTypeContribution(L10N l10n,
                                         PhoneFormatter phoneFormatter) {
            this.l10n = l10n;
            this.phoneFormatter = phoneFormatter;
        }

        @Override
        public String getTypeName() {
            return "phone";
        }

        @Override
        protected Class<PhoneJavaType> javaTypeClass() {
            return PhoneJavaType.class;
        }

        @Override
        protected PhoneJavaType javaType() {
            return new PhoneJavaType(l10n, phoneFormatter);
        }
    }

Simple java types
-----------------

Most java types are relatively simple and only map between a string or number and an object:

    * :nice:`EncodedPasswordJavaType <ch/tocco/nice2/persist/security/impl/hibernate/javatype/EncodedPasswordJavaType>` maps
      instances of :nice:`EncodedPassword <ch/tocco/nice2/types/spi/password/EncodedPassword>` from/to a string.
    * :nice:`LoginJavaType <ch/tocco/nice2/persist/core/impl/hibernate/javatype/LoginJavaType>` maps
      instances of :nice:`Login <ch/tocco/nice2/types/api/Login>` from/to a string.
    * :nice:`UuidJavaType <ch/tocco/nice2/persist/core/impl/hibernate/javatype/UuidJavaType>` maps
      instances of :java:`UUID <java.base/java/util/UUID>` from/to a string.
    * :nice:`GeolocationTypesContribution <ch/tocco/nice2/optional/geolocation/impl/type/GeolocationTypesContribution>` contains
      java types that support :nice:`Latitude <ch/tocco/nice2/types/spi/geolocation/Latitude>` and :nice:`Longitude <ch/tocco/nice2/types/spi/geolocation/Longitude>` objects.

``phone`` type
--------------

The :nice:`PhoneJavaType <ch/tocco/nice2/entityoperation/impl/phone/PhoneJavaType>` is applied for all field
of the virtual ``phone`` type.
This java type does not convert between different objects, but formats the phone number using the
:nice:`PhoneFormatter <ch/tocco/nice2/toolbox/api/phone/PhoneFormatter>` whenever a ``phone`` value
is written to the database.

``html`` type
-------------

Like the :nice:`PhoneJavaType <ch/tocco/nice2/entityoperation/impl/phone/PhoneJavaType>`, the
:nice:`HtmlJavaType <ch/tocco/nice2/persist/core/impl/hibernate/javatype/HtmlJavaType>` does not convert between different objects
but does some string formatting for ``html`` fields.

The formatting behaviour can be contributed using a :nice:`HtmlJavaTypeExtension <ch/tocco/nice2/persist/core/api/hibernate/javatype/HtmlJavaTypeExtension>`.
Currently there is only the :nice:`PreserveFreemarkerOperatorsHtmlJavaTypeExtension <ch/tocco/nice2/templating/impl/freemarker/javatype/PreserveFreemarkerOperatorsHtmlJavaTypeExtension>`
which handles escaping in freemarker expressions.

``binary`` type
---------------

The :nice:`BinaryJavaType <ch/tocco/nice2/persist/core/impl/hibernate/javatype/BinaryJavaType>` handles the :nice:`Binary <ch/tocco/nice2/persist/core/api/entity/Binary>`
class. The column of a ``binary`` field contains the hash code of the binary and references the ``_nice_binary`` table.

In addition to the mapping of the hash code this java type also calls the configured :nice:`BinaryAccessProvider <ch/tocco/nice2/persist/core/spi/binary/BinaryAccessProvider>`
which stores the binary data if necessary.

Java types are also used to map query parameters. If a :nice:`Binary <ch/tocco/nice2/persist/core/api/entity/Binary>` object is
used as a query parameter, it should obviously not be attempted to write it to the binary data store!
Therefore the binary content is only saved if ``Binary#mayBeStored()`` returns true.
If a hash code is used as a query parameter for a binary field, the string is converted to a :nice:`BinaryQueryParameter <ch/tocco/nice2/persist/core/impl/hibernate/legacy/BinaryQueryParameter>`
by the :nice:`StringToBinaryParameterConverter <ch/tocco/nice2/persist/core/impl/hibernate/legacy/StringToBinaryParameterConverter>`.
``BinaryQueryParameter#mayBeStored()`` returns false so it can safely be used in queries.

See chapter :ref:`large_objects` for more details about large objects.

``compressed-text`` type
------------------------

The :nice:`CompressedTextJavaType <ch/tocco/nice2/persist/core/impl/hibernate/javatype/CompressedTextJavaType>` is a sub-type of
the ``string`` type. It compresses and decompresses the string data when writing and reading the field from
the database. Zstd compression is used, the compression level can be configured using the ``persist.core.zstd.compression.level``
property (default value is 19).

.. note::

    This is useful for storing large text fields, but keep in mind that the content of the string cannot be used
    in a query, as only the compressed data is available on the database.


