.. meta::
   :keywords: CORS, cross-origin resource sharing, same-origin policy
.. index:: CORS

Cross-Origin Resource Sharing (CORS)
====================================

By default, the REST resources cannot be accessed from another domain outside the domain from which the REST API is
served (forbidden by the `same-origin security policy`_).

`Cross-Origin Resource Sharing (CORS)`_ defines a way to allow those cross-origin requests which are forbidden by default.

This works by setting some HTTP response headers on our REST responses. The easiest way to achieve this is by adding
them to the ``nice2.web.core.ResponseHeaders`` configuration point (while restricting the headers to the URLs which start
with `nice2/rest/`).

.. _same-origin security policy: https://en.wikipedia.org/wiki/Same-origin_policy
.. _Cross-Origin Resource Sharing (CORS): https://en.wikipedia.org/wiki/Cross-origin_resource_sharing

Headers
-------

The HTTP headers that relate to CORS are

Request headers
^^^^^^^^^^^^^^^

- Origin
- Access-Control-Request-Method
- Access-Control-Request-Headers

Response headers
^^^^^^^^^^^^^^^^

- Access-Control-Allow-Origin
- Access-Control-Allow-Credentials
- Access-Control-Expose-Headers
- Access-Control-Max-Age
- Access-Control-Allow-Methods
- Access-Control-Allow-Headers

Example configuration
---------------------

The following simple example allows all kinds of REST requests from the domain `www.someotherdomain.ch`, while
cross-origin requests from all other domains are still forbidden.

To allow only a subset of the HTTP methods or request headers, simply adjust the corresponding ``<header>``
contribution according to your needs.

Put the following code into the configuration class of the customer if you want to enable cross-origin requests:

.. code-block:: java

    private static final String URL_PATTERN = "/nice2/(log|login|rest|session|textresource|upload|username|dwr).*|/img/.*";

    @Bean
    public HeaderContribution testAccessControlAllowOriginHeaderContribution(@Qualifier("accessControlAllowOriginHeaderSupplier") HeaderValueSupplier valueSupplier) {
        HeaderContribution contribution = new HeaderContribution();
        contribution.setName("Access-Control-Allow-Origin");
        contribution.setUrlPattern(URL_PATTERN);
        contribution.setHeaderValueSupplier(valueSupplier);
        return contribution;
    }

    @Bean
    public HeaderContribution testVaryHeaderContribution() {
        HeaderContribution contribution = new HeaderContribution();
        contribution.setName("Vary");
        contribution.setUrlPattern(URL_PATTERN);
        contribution.setValue("Origin");
        return contribution;
    }

    @Bean
    public HeaderContribution testAccessControlAllowCredentialsHeaderContribution() {
        HeaderContribution contribution = new HeaderContribution();
        contribution.setName("Access-Control-Allow-Credentials");
        contribution.setUrlPattern(URL_PATTERN);
        contribution.setValue("true");
        return contribution;
    }

    @Bean
    public HeaderContribution testAccessControlAllowHeadersHeaderContribution() {
        HeaderContribution contribution = new HeaderContribution();
        contribution.setName("Access-Control-Allow-Headers");
        contribution.setUrlPattern(URL_PATTERN);
        contribution.setValue("Authorization,Content-Type,X-Business-Unit,X-Origin-Id,X-Client-Questions,X-Enable-Notifications");
        return contribution;
    }

    @Bean
    public HeaderContribution testAccessControlExposeHeadersHeaderContribution() {
        HeaderContribution contribution = new HeaderContribution();
        contribution.setName("Access-Control-Expose-Headers");
        contribution.setUrlPattern(URL_PATTERN);
        contribution.setValue("Location");
        return contribution;
    }

    @Bean
    public HeaderContribution testAccessControlAllowMethodsHeaderContribution() {
        HeaderContribution contribution = new HeaderContribution();
        contribution.setName("Access-Control-Allow-Methods");
        contribution.setUrlPattern(URL_PATTERN);
        contribution.setValue("GET,POST,PUT,DELETE,OPTIONS,PATCH");
        return contribution;
    }

Additionally, add the following application property:

.. code-block:: Properties

    nice2.web.allowedRequestOrigins=https://www.someotherdomain.ch

Do so by setting this in :term:`config.yml`:

.. code-block:: yaml

    additional_request_origins:
    - https://www.someotherdomain.ch

Sites added to ``wordpress`` in config.yml are added automatically.

.. note::

   The ``nice2.web.core.ResponseHeaders`` configuration point can be used for all HTTP response headers you want to set
   on certain HTTP responses provided by the Nice2 application. Setting the CORS headers is just one possible use case.

AccessControlAllowOriginValueSupplier
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You might have noticed that the ``<header>`` contribution for the header ``Access-Control-Allow-Origin`` uses the
attribute ``value-supplier``, while all other contribution use ``value`` with a hardcoded string value.

If you intend to enable CORS only for one domain, you'd be perfectly fine by hardcoding that single domain.
The header contribution for ``Access-Control-Allow-Origin`` could be replaced with the following
contribution in this case:

.. code-block:: java

    @Bean
    public HeaderContribution testAccessControlAllowMethodsHeaderContribution() {
        HeaderContribution contribution = new HeaderContribution();
        contribution.setName("Access-Control-Allow-Origin");
        contribution.setUrlPattern("/nice2/rest/.*");
        contribution.setValue(""https://www.someotherdomain.ch"");
        return contribution;
    }

Also, you could remove the ``nice2.web.allowedRequestOrigins`` application property.

However, as soon as cross-origin requests should be allowed for more than one domain or for all domains, we need a
more dynamic response header value. In this case, the ``Origin`` request header has to be set as value for the
``Access-Control-Allow-Origin`` response header. And that's exactly what the ``AccessControlAllowOriginValueSupplier``
is here for. It sets the value of the ``Origin`` request header as value of the response header, if the origin
is allowed according to the ``nice2.web.allowedRequestOrigins`` application property.

Enable CORS for multiple domains
''''''''''''''''''''''''''''''''

Let's say, we'd like to enable CORS for the origins `https://www.someotherdomain.com` and `https://www.example.com`.
In this case, we'd have to use the ``AccessControlAllowOriginValueSupplier`` and set the application property as follows:

.. code-block:: Properties

    nice2.web.allowedRequestOrigins=https://www.someotherdomain.ch,https://www.example.com

Do so by setting this in :term:`config.yml`:

.. code-block:: yaml

   additional_request_origins:
   - https://www.someotherdomain.ch
   - https://www.example.com

Sites added to ``wordpress`` in config.yml are added automatically.
