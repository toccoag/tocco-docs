Removing Legacy Code
====================

Removing ExtJS Flow
-------------------

As an example here the flow with the id ``licenceoverview`` is deleted

- remove visibility status file ``FLOW_ID.xml`` (e.g. ``licenceoverview.xml``)
- remove file ``FLOW_ID.ftl`` (e.g. ``licenceoverview.ftl``)
- in the deleted file there is a line like ``nice2.flows.publicflows.PublicFlow.run(..., cfg);``. search for the flow id (e.g. ``nice2.optional.licence.publicflows.licenceoverview.LicenceOverviewFlow``)
- delete the javascript file which contains the flow id
- for each deleted javascript file the ``JavaScriptContribution`` must be adjusted
- if a DWR remote service is called (e.g. ``nice2_optional_licence_AssociationMutationActionService``) check if you can also delete the Java code (some services are used in multiple flows)
- a javascript file often contains text resources and forms. these can be deleted if there are not used in other places in the code
- sometimes a flow is split up in multiple files (often they are in the same directory). check if you can delete more javascript files
- for each deleted form, check if

    - you can delete text resources (e.g. search for ``forms.LicenceOverview_``)
    - a ``CustomDavaProvider`` is only used here and can be deleted
    - a constriction is only used here and can be deleted
    - a custom remote field form is only used here (e.g. ``form="..."``) and can be deleted

- check if there are ACL rules only used for this flow which can be deleted (often there is a comment like ``/* ---licensing flow--- */`` which helps finding the ACL rules)
- check if there is cms conf entity (e.g. ``Licenceoverview_conf``). If yes delete the entity with all relations (the foreign key to ``nice_content_reference`` must be manually dropped)::

    <changeSet author="swuersten@tocco.ch" context="schemaDefinition" dbms="postgresql" id="drop_cms_reference_fk_association_mutation_conf_fkey-3wMMeyDE/3.13.1">
        <preConditions onFail="MARK_RAN">
            <foreignKeyConstraintExists
                foreignKeyTableName="nice_content_reference"
                foreignKeyName="nice_cms_reference_fk_association_mutation_conf_fkey"/>
        </preConditions>
        <dropForeignKeyConstraint
            baseTableName="nice_content_reference"
            constraintName="nice_cms_reference_fk_association_mutation_conf_fkey"/>
    </changeSet>
    <changeSet author="swuersten@tocco.ch" context="schemaDefinition" dbms="postgresql" id="drop_table_nice_association_mutation_conf-3wMMeyDE/3.13.1">
        <preConditions onFail="MARK_RAN">
        <tableExists tableName="nice_association_mutation_conf"/>
        </preConditions>
        <dropTable tableName="nice_association_mutation_conf"/>
    </changeSet>
    <changeSet author="swuersten@tocco.ch" context="schemaDefinition" dbms="postgresql" id="drop_column_fk_association_mutation_conf-wwTUC71X/3.13.1">
        <preConditions onFail="MARK_RAN">
        <columnExists columnName="fk_association_mutation_conf" tableName="nice_content_reference"/>
        </preConditions>
        <dropColumn columnName="fk_association_mutation_conf" tableName="nice_content_reference"/>
    </changeSet>

- clean up some cms tables before deleted the cms template and the legacy widget::

    <changeSet author="stwu" dbms="postgresql" id="remove_association_mutation_flow/3.13">
        <preConditions onFail="MARK_RAN">
            <tableExists tableName="nice_content_reference"/>
            <tableExists tableName="nice_widget_legacy_to_section"/>
            <tableExists tableName="nice_widget_legacy"/>
            <tableExists tableName="nice_cms_template_to_content_status"/>
            <tableExists tableName="nice_cms_template"/>
        </preConditions>
        <update tableName="nice_content_reference">
            <column name="fk_association_mutation_conf" valueComputed="NULL"/>
            <where>fk_association_mutation_conf IS NOT NULL</where>
        </update>
        <delete tableName="nice_content_reference">
            <where>fk_cms_template = (select pk from nice_cms_template where unique_id = 'association_mutation')</where>
        </delete>
        <delete tableName="nice_widget_legacy_to_section">
            <where>fk_widget_legacy IN (SELECT id FROM nice_widget_legacy
            WHERE fk_cms_template = (SELECT pk FROM nice_cms_template WHERE unique_id = 'association_mutation')
            )</where>
        </delete>
        <delete tableName="nice_widget_legacy">
            <where>fk_cms_template = (SELECT pk FROM nice_cms_template WHERE unique_id = 'association_mutation')</where>
        </delete>
        <delete tableName="nice_cms_template_to_content_status">
            <where>fk_cms_template = (SELECT pk FROM nice_cms_template WHERE unique_id = 'association_mutation')</where>
        </delete>
        <delete tableName="nice_cms_template">
            <where>unique_id = 'association_mutation'</where>
        </delete>
    </changeSet>

- delete text resource ``template.FLOW_ID`` (e.g. ``template.licenceoverview``)
