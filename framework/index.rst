Tocco Framework
===============

.. toctree::
   :maxdepth: 2

   architecture/index
   build
   client/index
   configuration/index
   rest/index
   modules/index
   contributing-guidelines
   removing-legacy-code
