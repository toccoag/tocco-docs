.. meta::
    :keywords: gradle, build, gradle build

Build
=====

.. note::

    This documentation is currently limited to terminal commands. When using IDEA,
    feel free to use the built-in tools.


Development
-----------

Basic build command::

    ./gradlew build -x test

Not all changes can be built incrementally. A clean build is needed in such cases::

    ./gradlew clean build -x test

In particular, this is likely needed when changing to a different branch. ``clean`` will
also ensure NPM dependency are refetched.


.. _gradle-build-production:

Production
----------

Production builds:

.. parsed-literal::

    export CI=1
    export ORG_GRADLE_PROJECT_nice2_repo_username=\ **${USERNAME}**
    export ORG_GRADLE_PROJECT_nice2_repo_password=\ **${PASSWORD}**
    ./gradle clean customer:\ **${CUSTOMER_NAME}**\ :build -x test -P buildForProduction=true

.. note::

    ``CI=1``

        When this env. var. is set, ``npm`` is instructed to use the checked-in
        lock file. This prevents updated dependencies from breaking builds. See
        :ticket:`WEB-138`.

        **⚠️ Always set this in CI/CD environments.**

    ``buildForProduction=true``

        Build in production mode. As of today, this is used to ensure website
        assets are built in production mode.

        **⚠️ Always set this in CI/CD environments.**

    | ``ORG_GRADLE_PROJECT_nice2_repo_username=${USERNAME}``
    | ``ORG_GRADLE_PROJECT_nice2_repo_password=**${PASSWORD}``

        When building locally, credentials to access the package registry are
        stored in `~/.gradle/grade.properties`.  However, in a CI/CD environment,
        these env. var. may be used instead. See also :ref:`gradle-repository-access`.

    ``customer:${CUSTOMER_NAME}:build``

        Limit build to given customer. For CI/CD, in particular, use this
        to reduce build time. Use ``build`` to build all customer modules.


Docker
------

See :doc:`/devops/openshift/nice_docker_image`


Running
=======

Development
-----------

Start Nice2:

.. parsed-literal::

    ./gradlew customer:\ **${CUSTOMER}**\ :bootRun -PmaxHeapSize='4G' --args='--spring.profiles.active=development --spring.output.ansi.enabled=ALWAYS'


Docker
------

See :ref:`docker-nice2-run`


Schema Upgrade (AKA DB Refactoring)
===================================

Development
-----------

Run schema upgrade::

    ./gradlew bootRun --build-cache -PmaxHeapSize='4G' -PjvmArgs='-Dch.tocco.nice2.disableSchemaModelStartupCheck=true' --args='--spring.profiles.active=update --spring.output.ansi.enabled=ALWAYS --ch.tocco.nice2.enableUpgradeMode=true --ch.tocco.nice2.enterprisesearch.disableStartup=true --ch.tocco.nice2.disableRoleSync=true --ch.tocco.nice2.disableLanguageSync=true --ch.tocco.nice2.disableBusinessUnitSync=true'

Docker
------

See :ref:`docker-nice2-schema-upgrade`


Tests
=====

Run tests:

.. parsed-literal::

    export CI=1
    export ORG_GRADLE_PROJECT_nice2_repo_username=\ **${USERNAME}**
    export ORG_GRADLE_PROJECT_nice2_repo_password=\ **${PASSWORD}**
    ./gradlew clean build

Omit ``-x test`` and test are run as part of the build.

.. note::

    See also note on `Production`_.

    The embedded Postgres used during testing will refuse to run
    as root. In CI environments where tests are executed as root,
    like on GitLab CI, it will be necessary to create a user::

        adduser user --disabled-password </dev/null >/dev/
        su user -c './gradlew clean build'
