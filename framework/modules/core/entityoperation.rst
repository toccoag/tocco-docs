Entityoperation
===============

Merge
-----

Per default all fields and relations from the detail form plus the relations tabs are considered for merging.
If a user does not have write access for a relation (or for some related entities) a to-many relation is automatically moved and is not displayed.
If the moving fails because the current user does not have enough rights to move some related entities
(and the relation is not defined as privileged), the user gets the following hint in the summary screen:
*The following records could not be moved: Salary (2)*.

There are some simple ``RelationMergeHandler`` implementations such as ``ToManyRelationMergeHandler``,
``ToManyRelationMergeHandler`` or ``PrivilegedRelationMergeHandler`` which process the merging.
The handler with the highest ``priority()`` where ``supports(RelationModel)`` returns ``true`` is executed.
However sometimes you must implement a custom relation handler to get a useful result
(e.g. ``EntityDocsRelationMergeHandler`` or ``AddressAddressRelationMergeHandler``).

Some relations should not be visible in the merge action. In the following example the ``relMark`` relation on the ``User`` entity and the
``relDebitor_information`` relation on the ``Address`` entity are not displayed.
But the ``relMark`` is automatically moved because the optional argument ``blindCopy`` is true while the ``relDebitor_information`` is not touched.

.. code-block:: Java
        :caption: Excluded relation

        @Bean
        public ExcludedRelationContribution relMarkExcludedRelationContribution() {
            return new ExcludedRelationContribution("User", "relMark", true);
        }

        @Bean
        public ExcludedRelationContribution relDebitorInformationExcludedRelationContribution() {
            return new ExcludedRelationContribution("Address", "relDebitor_information", false);
        }

Some fields should not be visible in the merge action. In the following example the ``longitude`` field on the ``Address`` entity
is not displayed.

.. code-block:: Java
        :caption: Excluded field

          @Bean
          public ExcludedFieldContribution longitudeExcludedFieldContribution() {
              return new ExcludedFieldContribution("Address", "longitude");
          }

If a user does not have write permission for a relation, moving the related entities to the target entity is not possible.
However there is the option to set a relation as privileged. In such a case the relation is moved without checking the permission of the user.

.. code-block:: Java
        :caption: Privileged relation

        @Bean
        public PrivilegedRelationContribution userCorrespondencePrivilegedRelationContribution() {
            PrivilegedRelationContribution contribution = new PrivilegedRelationContribution();
            contribution.setModelName("User");
            contribution.setRelationName("relCorrespondence");
            return contribution;
        }

For some relations either all or no related entities should be moved.
In the following example the relation ``relRegistration`` on the entity ``User`` is defined as an "all-or-nothing-relation".
Additionally the entity ``Resource`` is always an "all-or-nothing" relation.
So during merging ``User``, ``Address`` etc. entities a single resource can never be selected/deselected.
Either all resources of a target entity are move or nothing is moved.

.. code-block:: Java
        :caption: All or nothing relation

        @Bean
        public AllOrNothingContribution userAllOrNothingContribution() {
            AllOrNothingContribution contribution = new AllOrNothingContribution();
            contribution.setEntityModel("User");
            contribution.setRelation("relRegistration");
            return contribution;
        }

        @Bean
        public AllOrNothingContribution resourceAllOrNothingContribution() {
            AllOrNothingContribution contribution = new AllOrNothingContribution();
            contribution.setEntityModel("Resource");
            return contribution;
        }

Per default nothing happens with the source entity. However there is an option to delete the source entity.
In the following example all ``Address`` source entities are deleted after merging.

.. code-block:: Java
        :caption: Delete source entity

        @Bean
        @DeleteSourceEntityStrategyDescriptor
        public String addressDeleteSourceEntityStrategy() {
            return "Address";
        }

Another option is to change a related (lookup) entity.
In the following example the ``relAddress_status`` relation is set to ``archive`` for the source entities.

.. code-block:: Java
        :caption: Archive source entity (set a related entity)

        @Bean
        public SetRelationSourceDescriptor addressSetRelationSourceDescriptor() {
            SetRelationSourceDescriptor descriptor = new SetRelationSourceDescriptor();
            descriptor.setEntityModel("Address");
            descriptor.setRelation("relAddress_status");
            descriptor.setKeyField("unique_id");
            descriptor.setValue("archive");
            return descriptor;
        }

Last there is the option to change a field value.

.. code-block:: Java
        :caption:  Archive source entity (change field value)

        @Bean
        public SetFieldSourceDescriptor addressSetFieldSourceDescriptor() {
            SetFieldSourceDescriptor descriptor = new SetFieldSourceDescriptor();
            descriptor.setEntityModel("Address");
            descriptor.setField("callname");
            descriptor.setValue("archive");
            return descriptor;
        }
