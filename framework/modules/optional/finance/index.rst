Finance-Modules
===============

Action "Create Invoice"
-----------------------
The action "Create Invoice" is highly individualizable by adding "article invoice logic" (least invasive option) or by
replacing parts of the logic by replacing "tasks".

Article Invoice Logic
^^^^^^^^^^^^^^^^^^^^^

Article Invoice Logic is a piece of logic that is attached to an article. This logic must be configured as a
``Article_invoice_logic`` and linked to the Articles for which it shall be applied. Article Invoice Logic can either be
written in groovy directly on the entity or as java class that implements the interface ``InvoiceLogicStrategy``.

Article Invoice Logic have access to the source entity (e.g. the Membership that is being billed), the orderPosition
that is being created and the article. It can be used to alter the order positions or the linked order based on the
given parameters.

To use a java logic, the ``unique_id`` has to match the return value of the method ``getName``.

Check tasks
^^^^^^^^^^^

Check tasks are executed while opening the action.

.. figure:: resources/check_tasks.png

+------------------------+---------------------------------------------------------------------------------------------+
| Task                   | Description                                                                                 |
+========================+=============================================================================================+
| CheckInvoiceRecipients | Checks if the recipient address can be resolved for every selected entity.                  |
+------------------------+---------------------------------------------------------------------------------------------+
| CheckPriceCategories   | Checks if a price category can be resolved for every selected entity.                       |
+------------------------+---------------------------------------------------------------------------------------------+
| CheckBillingPositions  | Checks if every selected entity has billing positions.                                      |
+------------------------+---------------------------------------------------------------------------------------------+

If either the ``CheckInvoiceRecipientsTask`` or the ``CheckPriceCategoriesTask`` fails, the action will be cancelled, if the
``CheckBillingPositionsTask`` fails, the user will be asked whether he wants to continue anyway or not.

These check tasks can be replaced for each source entitiy (e.g. ``Membership`` or ``Registration``) using the
``OrderGeneratorCheckTaskContribution`` as shown below.

.. code-block:: java

   @Bean
   public OrderGeneratorCheckTaskContribution checkPriceCategoriesOrderGeneratorCheckTaskContribution(CheckPriceCategoriesTask task) {
       OrderGeneratorCheckTaskContribution contribution = new OrderGeneratorCheckTaskContribution();
       contribution.setTask(task);
       contribution.setEntity("Membership");
       contribution.setName(OrderGeneratorTaskProvider.TASK_CHECK_PRICE_CATEGORIES);
       return contribution;
   }

Generate tasks
^^^^^^^^^^^^^^

Generate tasks are executed after submitting the action. These tasks create the orders and are executed in a background
task.

.. figure:: resources/generate_tasks.png

+----------------------+------------------------------------------------------------------------------------------------+
| Task                 | Description                                                                                    |
+======================+================================================================================================+
| CreateOrders         | Creates an order for each selected source entity.                                              |
+----------------------+------------------------------------------------------------------------------------------------+
| SetPriceCategories   | Sets the price category on the created order                                                   |
+----------------------+------------------------------------------------------------------------------------------------+
| SetServiceDates      | Sets service start and service end date on the created order.                                  |
+----------------------+------------------------------------------------------------------------------------------------+
| CreateOrderPositions | Creates an order position for each billing position.                                           |
+----------------------+------------------------------------------------------------------------------------------------+
| RunOrderStrategies   | Runs the article invoice logics.                                                               |
+----------------------+------------------------------------------------------------------------------------------------+
| SplitOrders          | Splits the order if a split recipient is configured (e.g. Registration)                        |
+----------------------+------------------------------------------------------------------------------------------------+
| MergeOrders          | Merges all orders where *everything* matches (see ``MergeTasksContribution``)                  |
+----------------------+------------------------------------------------------------------------------------------------+
| MergeOrderPositions  | Merges all order positions where *everything* matches (see ``MergePositionTasksContribution``) |
+----------------------+------------------------------------------------------------------------------------------------+

These tasks can be replaced for each source entitiy (e.g. ``Membership`` or ``Registration``) using the
``OrderGeneratorTaskContribution`` as shown below.

.. code-block:: java

   @Bean
   public OrderGeneratorTaskContribution createOrdersOrderGeneratorTaskContribution(CreateOrdersTask createOrdersTask) {
       OrderGeneratorTaskContribution contribution = new OrderGeneratorTaskContribution();
       contribution.setTask(createOrdersTask);
       contribution.setEntity("Membership");
       contribution.setName(OrderGeneratorTaskProvider.TASK_CREATE_ORDERS);
       return contribution;
   }

Creditslip Reports
------------------

Credit slip reports or correspondences may be created using the Output_template_layout ``header_footer_bill`` this
layout adds either a QR- or an "Old"-Creditslip to the footer of the report or correspondence. The creditslip to be used
can be configured on each business unit (Flag: legacy_creditslip).

There can more credit slips than "content pages" (e.g. multiple payment rates) or more content pages than creditslips
(e.g. long letter, only one credit slip). If there are more credit slips than there are "content pages", empty pages
are appended to print the credit slips. If there are more "content pages" than there are credit slips, "placeholder"
creditslips are added. The placeholder for old credit slips is supposed to nullify the credit slips on the first pages.
The placeholder for qr credit slip is blank.

Please find below an overview over the components that are used for creditslip reports

+------------------------------+------------------------------+--------------------------------------------------------+
| Type                         | Unique id                    | Description                                            |
+==============================+==============================+========================================================+
| Output_template_layout       | header_footer_bill           | Writes the "bill values" for each page into the        |
|                              |                              | corresponding credit slip template using java script.  |
+------------------------------+------------------------------+--------------------------------------------------------+
| Template_snippet             | bill_credit_slip_template    | Layout for the legacy credit slip.                     |
+------------------------------+------------------------------+--------------------------------------------------------+
| Template_snippet             | bill_qr_credit_slip_template | Layout for the qr credit slip.                         |
+------------------------------+------------------------------+--------------------------------------------------------+
| Template_snippet             | bill_values                  | Builds a java script object out of the bill values.    |
|                              |                              | (Bill values are defined as Output_template_fields).   |
+------------------------------+------------------------------+--------------------------------------------------------+
| Template_snippet             | bill_placeholder             | Builds a java script object out of placeholder values  |
|                              |                              | (only used in old credit slip).                        |
+------------------------------+------------------------------+--------------------------------------------------------+
| Template_snippet             | bill_qr_code                 | Assembles the qr code content and builds the qr code   |
|                              |                              | image.                                                 |
+------------------------------+------------------------------+--------------------------------------------------------+
| Template_snippet             | bill_additional_information  | Builds the additional information string for the qr    |
|                              |                              | bill.                                                  |
+------------------------------+------------------------------+--------------------------------------------------------+
| Directive                    | @getQrAddress                | Builds the address in the correct format for qr bills. |
+------------------------------+------------------------------+--------------------------------------------------------+
| Directive                    | @qrCode                      | Generates a qr code.                                   |
+------------------------------+------------------------------+--------------------------------------------------------+

The layout of the credit slip is stored in the two template snippets ``bill_credit_slip_template`` and
``bill_qr_credit_slip_template``.

These template snippets contain empty elements with ``class`` names. The ``innerHtml`` of these elements will be
replaced by the actual bill values that are filled into a java script object in the template snippet ``bill_values``.

The values themselves are configured in output template fields.

+-----------------------+--------+----+--------------------------------------------------------------------------------+
| Field                 | Legacy | QR | Description                                                                    |
+=======================+========+====+================================================================================+
| account               | X      |    | The "old" account number (e.g. 01-200027-1).                                   |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| additionalInformation |        | X  | Additional information string according to swico specification.                |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| address               |        | X  | Recipient address on the qr bill.                                              |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| amountDecimal         | X      |    | Decimal part of the amount (e.g. "after the decimal seperator").               |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| amountInteger         | X      |    | Integer part of the amount (e.g. "before the decimal seperator").              |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| amountQrSlip          |        | X  | Amount for the qr code credit slip (grouping seperator ' ', decimal seperator  |
|                       |        |    | '.').                                                                          |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| benefitOf             | X      |    | Address of the recipient.                                                      |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| billingAddress        | X      | X  | Address of the user who is supposed to pay.                                    |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| codingLine            | X      |    | The coding line of the "old" credit slip.                                      |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| dueDate               |        | X  | Due date (date only).                                                          |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| dueDateLabel          |        | X  | Due date that is written above the qr bill.                                    |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| favourOf              | X      |    | Address of the recipients payment institution.                                 |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| ibanNumber            |        | X  | The qr iban number.                                                            |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| qrCode                |        | X  | The qr code (base64; result of ``@qrCode`` directive).                         |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| referenceNumber       | X      | X  | The order reference number.                                                    |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| ultimateAddress       |        | X  | The ultimate recipient address (currently marked as "for future use").         |
+-----------------------+--------+----+--------------------------------------------------------------------------------+
| useUltimateAddress    |        | X  | Flag to activate the ultimate recipient address (currently marked as "for      |
|                       |        |    | future use").                                                                  |
+-----------------------+--------+----+--------------------------------------------------------------------------------+

QR-Code
^^^^^^^

.. _specification: https://www.paymentstandards.ch/dam/downloads/ig-qr-bill-de.pdf

The qr code contains text according to the specification_. The implementation of the qr code can be found in the
template snippet ``bill_qr_code``. We currently fill the qr code with text as described in the table below.

+---------+--------------------------------------------------------------------------------------------+
| Line(s) | Description                                                                                |
+=========+============================================================================================+
| 1       | Hardcoded "SPC".                                                                           |
+---------+--------------------------------------------------------------------------------------------+
| 2       | Hardcoded "0200" (Version of the QR specification).                                        |
+---------+--------------------------------------------------------------------------------------------+
| 3       | Hardcoded "1" (Coding Type UTF-8).                                                         |
+---------+--------------------------------------------------------------------------------------------+
| 4       | QR IBAN Number.                                                                            |
+---------+--------------------------------------------------------------------------------------------+
| 5-11    | Recipient address (generated with ``@getQrAddress`` directive).                            |
+---------+--------------------------------------------------------------------------------------------+
| 5       | Hardcoded "K" for "Kombinierte Adressfelder".                                              |
+---------+--------------------------------------------------------------------------------------------+
| 6       | Company or person name.                                                                    |
+---------+--------------------------------------------------------------------------------------------+
| 7       | Address-Line 1 (Street / Number / po box).                                                 |
+---------+--------------------------------------------------------------------------------------------+
| 8       | Address-Line 2 (Zip / City).                                                               |
+---------+--------------------------------------------------------------------------------------------+
| 9       | Empty (not used for "Kombinierte Adressfelder").                                           |
+---------+--------------------------------------------------------------------------------------------+
| 10      | Empty (not used for "Kombinierte Adressfelder").                                           |
+---------+--------------------------------------------------------------------------------------------+
| 11      | ISO 3166-1 Country code (e.g. CH).                                                         |
+---------+--------------------------------------------------------------------------------------------+
| 12-18   | Address of "ultimate recipient" (for future use, currently always empty).                  |
+---------+--------------------------------------------------------------------------------------------+
| 19      | Amount. No grouping seperator, '.' as decimal seperator.                                   |
+---------+--------------------------------------------------------------------------------------------+
| 20      | Currency (e.g. CHF).                                                                       |
+---------+--------------------------------------------------------------------------------------------+
| 21-27   | Address of the payer (generated with ``@getQrAddress`` directive).                         |
+---------+--------------------------------------------------------------------------------------------+
| 21      | Hardcoded "K" for "Kombinierte Adressfelder".                                              |
+---------+--------------------------------------------------------------------------------------------+
| 22      | Company or person name.                                                                    |
+---------+--------------------------------------------------------------------------------------------+
| 23      | Address-Line 1 (Street / Number / po box).                                                 |
+---------+--------------------------------------------------------------------------------------------+
| 24      | Address-Line 2 (Zip / City).                                                               |
+---------+--------------------------------------------------------------------------------------------+
| 25      | Empty (not used for "Kombinierte Adressfelder").                                           |
+---------+--------------------------------------------------------------------------------------------+
| 26      | Empty (not used for "Kombinierte Adressfelder").                                           |
+---------+--------------------------------------------------------------------------------------------+
| 27      | ISO 3166-1 Country code (e.g. CH).                                                         |
+---------+--------------------------------------------------------------------------------------------+
| 28      | Hardcoded "QRR" (Type of reference number).                                                |
+---------+--------------------------------------------------------------------------------------------+
| 29      | Reference number generated by tocco (QRR standard).                                        |
+---------+--------------------------------------------------------------------------------------------+
| 30      | Empty (could be used for unstructured message).                                            |
+---------+--------------------------------------------------------------------------------------------+
| 31      | Hardcoded "EPD" (End Payment Data).                                                        |
+---------+--------------------------------------------------------------------------------------------+
| 32      | Additional information (See template snippet ``bill_additional_information``)              |
+---------+--------------------------------------------------------------------------------------------+

Please find an example of a tocco generated qr code content below.

.. code-block:: none

   SPC
   0200
   1
   CH1330700110000555322
   K
   Tocco AG
   Riedtlistrasse 27
   8006 Zürich


   CH







   100.00
   CHF
   K
   Testkunde
   Teststrasse 122
   8000 Zürich


   CH
   QRR
   987654009300000000000091855

   EPD
   //S1/10/9185/11/220609/30/123456789/31/220330220428

Abacus Export
-------------

.. _abaconnect-specification: https://downloads.abacus.ch/fileadmin/htmlfiles/abaconnect/fibu/FIBU__XML%20Buchungen_2021.00_AbaDefault_DE.html

The abacus export is a xml report that exports vouchers into a "FIBU XML Buchungen" abaconnect xml document (see
abaconnect-specification_). Tocco created the XSD ``abacus_export.xsd`` to validate our abacus exports. If the
specification changes the xsd may need amendments as well. Our report actions automatically applies xsd validation if a
xsd file is attached in the document tab of an output template.

.. _VAT (MWST):

VAT (MWST)
^^^^^^^^^^

Whether VAT information will be passed to abacus is controlled by the business unit settings ``VAT mandatory`` and
``Account balance taxation`` if either ``VAT mandatory`` is false or ``Account balance taxation`` is true, VAT
information will never be passed to abacus.

If the business unit uses VAT, there is an additional condition that checks on voucher level if VAT should be passed
to abacus or not. If either the credit or the debit account of the voucher are marked as ``VAT Mandatory``, VAT
information will be passed to abacus. If neither of the accounts is ``VAT Mandatory`` no VAT information will be passed
(this is ofthen the case for incoming payments as usually neither the bank account nor the debitor account are ``VAT
Mandatory``).

.. _Amount / Booking Method:

Amount / Booking Method
^^^^^^^^^^^^^^^^^^^^^^^

There are potentially three amount fields in the abacus export. There is the ``Amount`` section in
``CollectiveInformation`` and ``SingleInformation`` and there is the ``KeyAmount`` in the ``TaxData`` section. The first
two amount fields always contain the same value.

If a customer has no VAT (``VAT mandatory`` is false on business unit) the netto amount is passed in Single- and
CollectiveInformation. If a customer has VAT (``VAT mandatory`` is true) with ``Account balance taxation`` the brutto
amount will be passed in Single- and Collective information. ``TaxData`` is empty in both cases.

If the voucher has normal VAT, the booking method is used to determine the amounts. If ``netto`` booking method is used
the netto amount is passed in Single- and CollectiveInformation. The vat amount is passed in the TaxData block.

**Example netto method:**

.. code-block:: none

   Netto = 100
   VAT = 7.7
   BRUTTO = 107.7

   Amount in Single- / CollectiveInformation: 100
   Amount in TaxData: 7.7

   Example bookings:
   1100 -> 3000: 100
   1100 -> 2200: 7.7

If the booking method is ``brutto``. The brutto amount is passed in Single- and CollectiveInformation. The vat amount is
negative and the accounts are reversed.

**Example brutto method:**

.. code-block:: none

   Netto = 100
   VAT = 7.7
   BRUTTO = 107.7

   Amount in Single- / CollectiveInformation: 107.7
   Amount in TaxData: -7.7

   Example bookings:
   1100 -> 3000: 107.7
   3000 -> 2200: 7.7

Content
^^^^^^^

Please find below a description of what is exported to abacus by tocco standard.

+------------------------+------------------------+--------------------------------------------------------------------+
| Parent                 | Element                | Description                                                        |
+========================+========================+====================================================================+
|                        | AbaConnectContainer    | Root element of abacus export.                                     |
+------------------------+------------------------+--------------------------------------------------------------------+
| AbaConnectContainer    | TaskCount              | Number of Fibu_export entities exported (1 as in standard the      |
|                        |                        | report may only be called from detail).                            |
+------------------------+------------------------+--------------------------------------------------------------------+
| AbaConnectContainer    | Task                   | Root element of abacus task.                                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| Task                   | Parameter              | Root element for task parameter.                                   |
+------------------------+------------------------+--------------------------------------------------------------------+
| Parameter              | Application            | Hardcoded “FIBU”.                                                  |
+------------------------+------------------------+--------------------------------------------------------------------+
| Parameter              | Id                     | Hardocded “XML Buchungen”.                                         |
+------------------------+------------------------+--------------------------------------------------------------------+
| Parameter              | MapId                  | AbaDefault                                                         |
+------------------------+------------------------+--------------------------------------------------------------------+
| Parameter              | Version                | 2021.00                                                            |
+------------------------+------------------------+--------------------------------------------------------------------+
| Parameter              | Mandant                | ``Abacus client no.`` from ``Business Unit``                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| AbaConnectContainer    | Transaction            | Grouping element for entries                                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| Transaction            | Entry                  | Root element for an entry (= Voucher in tocco)                     |
+------------------------+------------------------+--------------------------------------------------------------------+
| Entry                  | CollectiveInformation  | Root element for collective information                            |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | EntryLevel             | Hardcoded “A”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | EntryType              | Hardcoded “S”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | Type                   | Hardcoded “Normal”                                                 |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | DebitCredit            | Hardcoded “D”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | Client                 | ``Abacus client no.`` from ``Business Unit``                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | Division               | Hardcoded “0”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | EntryLevel             | Hardcoded “A”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | KeyCurrency            | Uppercase label of default currency. Standard is “CHF”.            |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | EntryDate              | Date of Voucher in ISO format.                                     |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | AmountData             | Root element for AmountData.                                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| AmountData             | Currency               | Label of the currency linked to the voucher.                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| AmountData             | Amount                 | The voucher amount (see :ref:`Amount / Booking Method`)            |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | KeyAmount              | The voucher amount (see :ref:`Amount / Booking Method`)            |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | Account                | Debit account of the voucher.                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | TaxAccount             | Netto method: Debit account, Brutto method: Account of vat rate    |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | BookingLevel1          | Cost center                                                        |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | Text1                  | ``posting_text`` of voucher                                        |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | Text2                  | ``posting_text`` of voucher                                        |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | DocumentNumber         | Voucher number                                                     |
+------------------------+------------------------+--------------------------------------------------------------------+
| CollectiveInformation  | SingleCount            | Hardcoded “0”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| Entry                  | SingleInformation      | Root element for single information                                |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | Type                   | Hardcoded “Normal”                                                 |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | DebitCredit            | Hardcoded “D”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | EntryDate              | Date of Voucher in ISO format.                                     |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | AmountData             | Root element for AmountData.                                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | Currency               | Label of the currency linked to the voucher.                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | Amount                 | The voucher amount (see :ref:`Amount / Booking Method`)            |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | KeyAmount              | The voucher amount (see :ref:`Amount / Booking Method`)            |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | Account                | Credit account of the voucher.                                     |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | TaxAccount             | Brutto method: Credit account, Netto method: Account of vat rate   |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | BookingLevel1          | Cost center                                                        |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | Text1                  | ``posting_text`` of voucher                                        |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | Text2                  | ``posting_text`` of voucher                                        |
+------------------------+------------------------+--------------------------------------------------------------------+
| SingleInformation      | TaxData                | Root element for tax data, only included if voucher has vat (see   |
|                        |                        | :ref:`VAT (MWST)`)                                                 |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | TaxIncluded            | Netto method: “E”, Brutto method: “I”                              |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | TaxType                | Hardcoded ”1”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | UseCode                | Hardcoded “1”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | AmountData             | Root element for amount data                                       |
+------------------------+------------------------+--------------------------------------------------------------------+
| AmountData             | Currency               | Voucher currency                                                   |
+------------------------+------------------------+--------------------------------------------------------------------+
| AmountData             | Amount                 | Hardcoded “0”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | KeyAmount              | The vat amount (see :ref:`Amount / Booking Method`)                |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | TaxRate                | The vat rate as decimal (e.g. 7.7)                                 |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | TaxCoefficient         | Hardcoded “100”                                                    |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | Country                | ISO 2 code of vat rate                                             |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | TaxCode                | Content of field ``VAT code`` on the related vat code entity       |
+------------------------+------------------------+--------------------------------------------------------------------+
| TaxData                | FlatRate               | Hardcoded “0”                                                      |
+------------------------+------------------------+--------------------------------------------------------------------+
