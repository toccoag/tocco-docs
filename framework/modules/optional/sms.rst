SMS
===

Configuration
-------------

We support the websms implementation. There are two similar plattforms https://account.linkmobility.eu/oauth/login and https://account.websms.com/oauth/login
which have same GUI but are indepentent plattforms!

There are two options to configure the sms module.
First globally via application properties ``sms.provider.*``, ``sms.simulateSms`` and ``sms.warning.limit``.
Second via business unit entity. Then the password is set via action on the business unit.

For the websms/linkmobility account the 2FA must be enabled (else the API password cannot be set).
Open the "API settings" and create an API password. This strong password is used via REST API without 2FA.
Via web browser the normal password with 2FA is still used.

If you have a linkmobility account set ``https://api.linkmobility.eu`` as API base url. 
If nothing is set the default url ``https://api.websms.com`` is used (which is the url for a websms account).

If IP whitlisting is enabled the ip address of our cloud must be added in the websms/linkmobility account.
