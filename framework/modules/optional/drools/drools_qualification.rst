Qualification
=============

We use Drools in the qualification module to calculate averages, rounding values, setting dispenses and implement
promotion rules. Promotion rules are defined in the ``Promotion_rule`` entity, which can be grouped together to create
packages of rules. All our base rules are defined in the ``input-calculate.drl`` file.

.. figure:: resources/drools_qualification.png

    a rough overview how we build our knowledge base and start the evaluation

Facts
-----

A summary of the available and most often used facts in the qualification context. See the integration handbook for
detailed documentation.

Node
^^^^

This represents a node in the qualification structure. It can be either a
:nice:`EvalNode <ch/tocco/nice2/optional/qualification/impl/drools/facts/qs/EvalNode>`, wrapping a
``Evaluation_node``, or a :nice:`RatingNode <ch/tocco/nice2/optional/qualification/impl/drools/facts/qs/RatingNode>`,
wrapping a ``Input_node``.

Grade
^^^^^

This represents a numeric grade, the average of potentially multiple different exams or nodes. Any input with the type
``grades``, ``grades_max``, ``grade_mandatory``, ``grades_drop``, or ``points_threshold`` will produce these facts.

TotalPoints
^^^^^^^^^^^

This represents the sum of potentially multiple different exams where points can be entered. Both the ``points`` and
``points_threshold`` inputs will get summed.

ChoiceRating
^^^^^^^^^^^^

This represents the selection from a list of values from ``choice_rating`` inputs.

Presence
^^^^^^^^

This represents the presence of a student in a given node. This might a manually entered value or automatically
calculated. This is only available if a input of type ``presence`` exists.

IsDispensed
^^^^^^^^^^^

This represents a student being dispensed from a node. Check for the existence of this fact if you want to validate if a
student is dispensed.

Status
^^^^^^

This fact is used to find available status to set on a promotion.

ArrayList
^^^^^^^^^

This is the normal Java ArrayList, but it can be used in Drools together with the ``from collect`` instruction to easily
collect a list of facts.

Sum
^^^

This fact is a modified list that can also calculate sums or averages of its contents. It can be used the same way as
ArrayList and then checked for its calculations.

Conclusion
^^^^^^^^^^

These facts symbolize some rule that got triggered and usually marks a participant as either having passed or not passed, as well
as a potential border case situation. If you want to check for previous conclusions in your own rule, make sure to mark your rule
with a sufficently low ``salience`` (-1000 generally seems to do the trick) and ``no-loop``.

Utils
-----

A summary for the available utility classes in the qualification Drools context. See the integration handbook for
detailed documentation.

ConclusionFactory
^^^^^^^^^^^^^^^^^

This class is used together with ``Status`` facts to set a promotion status for the current evaluation. The final status
is decided by prioritizing all inserted status by their weights, filtering out any that where inserted with a negative
flag and picking the first remaining one.

Examples
--------

Since customers and integrators can write rules as well, these can be found in our regular manual.

Technical setup
---------------

.. note::

    This is mostly a direct copy from an old ``package-info.java`` file from the ``ch.tocco.nice2.optional.qualification.impl.drools`` package. Feel
    free to rewrite or expand this documentation.

Drools Evaluation
^^^^^^^^^^^^^^^^^

This package contains classes related to the drools evaluation for the qualification module. The main entry point to an evaluation is the service
point :nice:`EvaluationFactory <ch/tocco/nice2/optional/qualification/impl/drools/promotion/EvaluationFactory>`. It helps to create a
:nice:`PromotionEvaluation <ch/tocco/nice2/optional/qualification/impl/drools/promotion/PromotionEvaluation>` that can be evaluated yielding a
:nice:`EvaluationResult <ch/tocco/nice2/optional/qualification/impl/drools/promotion/EvaluationResult>`.

Note that this package is not responsible for persisting any of the results from the evaluation. This should be handled by the calling client.

The sub package ``ch.tocco.nice2.optional.qualification.impl.drools.facts`` contains bean classes representing all data relevant to the evaluation.
These are accessible when writing rules as explained in the drools manual.

Evaluation Procedure
^^^^^^^^^^^^^^^^^^^^

The execution of a knowledge session is obviously not procedural, but the following list roughly outlines the concept of the evaluation procedure for
the qualification module.

1. Compute '`Grade`' for all 'Input_node's (for all 'Input_node's where no 'Grade' is yet present): This is usually accomplished in one single rule.
   Depending on the 'Input_type' the 'Rating's are analyzed and a resulting 'Grade' is inserted into the working memory.
2. Round any inserted 'Grade': Taking into consideration an 'Input_node's rounding configuration, any inserted 'Grade' object is immediately rounded
   and updated with the new value.
3. Compute 'ComputedGrade' for 'Input_node': Taking into consideration any possibly present 'Grade' and 'IsDispensed' to update 'ComputedGrade' for an
   Input_node
4. Calculate averages for a parent node: Nodes that are ready to be calculated (depending on the presence of their children's 'Grade's) will collect
   their child node's 'Grade' to calculate the average. Similarly to above, the new 'Grade' object is inserted to the working memory and then rounded
   according to the node's rounding configuration.
5. Evaluate custom promotion rules Using the global variable 'conclusion' custom promotion rules may vote for a particular 'Promotion_status' to be
   set as the result for the evaluation. A set of custom promotion rules should be written in a way such that the rules don't contradict each other
   (i.e. vote for different 'Promotion_status'es).