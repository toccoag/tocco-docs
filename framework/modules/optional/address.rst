Address
=======

Recipient Service
-----------------

.. note::

    This is mostly a direct copy from an old ``package-info.java`` file from the ``ch.tocco.nice2.optional.address.api.recipient`` package. Feel free
    to rewrite or expand this documentation.

The recipient service can be used to do advanced communication targeting. To enable this, the model is implemented as follows:

Selected entity (1:1) ConcernedRecipient (1:n) CommunicationTarget (1:n) EffectiveRecipient

So here is an overview of the magic that happens behind calling RecipientService#getRecipientsByTarget()

ConcernedRecipient
^^^^^^^^^^^^^^^^^^

In the most simple case, a letter simply gets sent to a User oder Address selected in the entity explorer. But if we e.g. select a Registration, we do
not want the Registration to be the recipient, but the User or Address referenced by the Registration.

So for each selected entity, a ConcernedRecipient is created, which can be be based on the selected entity itself (if User or Address) or on the
User/Address related to it. This is evaluated during instantiation of ConcernedRecipient in ConcernedRecipient#evaluateConcernedEntity based on the
action configuration on the details form and the RecipientParameters.

EffectiveRecipient
^^^^^^^^^^^^^^^^^^

By choosing the extended mode of email and correspondence action and using the RecipientAction dialog, communication can not only be targeted to the
ConcernedRecipient as described above, but also be delegated to Users related to the ConcernedRecipient or to a special Address of the
ConcernedRecipient. For example when selecting a Registration in the entity explorer and choosing extended correspondence action, a letter targeted
e.g. to all parents of the User linked to the Registration. So the selected entity (Registration) leads to a ConcernedRecipient (User entity of the
student) and the ConcernedEntity in combination with the choosen delegation leads to two EffectiveRecipients (User entities of mother and father) for
which the letter will actually created. See CommunicationRelationType and RecipientParameters for more information on redirecting communication using
the RecipientAction dialog.

CommunicationTarget
^^^^^^^^^^^^^^^^^^^

As both parents might live at the same address, it does not make sense to send two separate letters for each of them to that same address. Therefore,
the EffectiveRecipients get structured in CommunicationTargets. Lets say do want to send the letter to the registered student's parents who live at
the same place, to his teacher and to the students working address, which all can be selected in the RecipientAction dialog, the structure would look
like this:

.. code-block::

    Selected Entity (Registration entity)
        ConcernedRecipient (based on User entity of the student)
            CommunicationTarget (based on Address entity of the parents)
                EffectiveRecipient (based on User entity of the mother)
                EffectiveRecipient (based on User entity of the father)
            CommunicationTarget (based on Address entity of the teacher)
                Effective Recipient (based on User entity of the teacher)
            CommunicationTarget (based on working Address entity of the student)
                Effective Recipient (based on User entity of the student)

This way, we send letters to three targets, addressing 4 recipients, while the most simple case i.e. sending a letter to a user selected in the entity
explorer, simply generates the following structure:

.. code-block::

    Selected Entity (User entity)
        ConcernedRecipient (based on the User entity)
            CommunicationTarget (based on the Address entity of the User entity)
                EffectiveRecipient (based on the User entity)