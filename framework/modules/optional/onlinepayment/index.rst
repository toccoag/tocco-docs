Onlinepayment-Module
====================

Configuration
-------------

Payrexx_account
^^^^^^^^^^^^^^^

The config entity ``Payrexx_account`` is used to hold the credentials of a payrexx account. One payrexx account must
always be declared as ``default_account``. If no Payrexx_account is explicitly configured (e.g. on a
``Payment_configuration``) the ``default_account`` is used.

The API Key and "Payrexx instance" must be set using the provided action. The action checks the credentials directly
with payrexx and activates the ``Payrexx_account``. It's not possible to activate an account without valid credentials.

Payment_method
^^^^^^^^^^^^^^

The ``Payment_method`` entity is used to store available payment methods (e.g. VISA, Mastercard, ...). The optional
field ``logo_url`` may be used to set a logo url. There is an action to import all active payment methods from the
current standard ``Payrexx_account``.


Payment_type
^^^^^^^^^^^^

``Payment_type`` defines how a configured ``Payment_method`` works. If the ``Payment_type`` is ``online_payment``,
online payment will be initialised. If a ``Payment_type`` is ``create_order``, it will automatically create an order.

Payment_configuration
^^^^^^^^^^^^^^^^^^^^^

The payment configuration is used to simplify the configuration process. They provide a central place to configure
payment methods / related accounts. On events where online payment should be enabled, these payment configurations
can be selected in a dropdown.


Event-Registration Payment-Flow
-------------------------------

.. figure:: resources/payrexx_flow.png

Registration
^^^^^^^^^^^^

The user registers to an event using the corresponding widget. If the user presses the "register" button, the entered
information is submitted and the registration is created.

Add Billing Positions / Create Order
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are two listeners ``RegistrationEntityListener`` and ``CopyBillingPositionEntityListener`` that copy billing
positions from ``Registration_answer_option`` and ``Event`` to the registration.

After those listeners the ``CreateOnlinepaymentOrderListener`` is called. This listener directly creates an order for
a registration. The listener only creates orders if billing position exist (one of the listeners above has copied
billing positions to this registration) and if a ``Payment_configuration`` with at least one ``Payment_method`` with a
``Payment_type`` that has the ``create_order`` flag is linked with the related ``Event``.

The ``Order_source`` of these orders is set to ``online``.

Payment Overview
^^^^^^^^^^^^^^^^

After registering, the frontend checks if e-payment is installed (module check) and it checks whether the
``CreateOnlinepaymentOrderListener`` created an order. If so, a payment overview page is displayed. To display the page
the ``PaymentProviderActionResource`` is called to load order values and a list of all available payment methods.

If the user selects a payment method that is not linked to payrexx (e.g. "Zahlung auf Rechnung"). An empty page with
the visibility state `` success`` is displayed.

If the user selects a payrexx payment method, the payrexx payment is initialised.

Initialise Payment
^^^^^^^^^^^^^^^^^^

.. _gateway_api: https://developers.payrexx.com/reference/create-a-gateway
.. _payrexx_java: https://gitlab.com/toccoag/payrexx-java

To initialise a Payrexx payment the ``PayrexxGatewayService.createGatewayForOrder`` method is used. The method uses an
order and a payment method to create a Payrexx Gateway using the gateway_api_ with the payrexx_java_ library. If the
Payrexx Gateway was created successfully, this method will return the ``link`` to the gateway.

We currently send the following information when creating a gateway:

.. list-table::
   :widths: 15 15 70
   :header-rows: 1

   * - Parameter
     - Source
     - Description
   * - instance
     - Parameter / Default
     - The ``payrexx_instance`` of the related ``Payrexx_account``. The ``Payrexx_account`` given in the param is used
       if available. If no ``Payrexx_account`` is given as a parameter, the ``default_account`` will be used.
   * - amount
     - Order field
     - The amount, the user has to pay (field ``total_gross_mc`` on order). Amounts are sent as integers in cents.
   * - pm
     - Parameter
     - Parameter for payment method. We send the ``unique_id`` of the given ``Payment_method`` wrapped in a list.
   * - currency
     - Order relation
     - Uppercase ``unique_id`` of the linked currency (e.g. CHF)
   * - referenceId
     - Generated
     - A generated uuid, that will be saved on the ``Payrexx_transaction``.
   * - successRedirectUrl
     - Parameter
     - The url where the user will be redirected to when the payment was successful (e.g. an empty page with visibility
       state ``payment-success``)
   * - failedRedirectUrl
     - Parameter
     - The url where the user will be redirected to when the payment fails (e.g. an empty page with visibility state
       ``payment-failed``)
   * - cancelledRedirectUrl
     - Parameter
     - The url where the user will be redirected to when he cancels the payment. This should be the payment page, where
       he can select a different payment method.

.. warning:: Theoretically "basket" items could be sent to payrexx. When this is done, the total amount of the gateway is ignored and payrexx calculates its own total (sum of quantity * amount of all basket items). This is not compatible with tocco data. Payrexx expects a "per piece" amount including vat, which is not available in tocco.

The response will be saved in a ``Payrexx_transaction`` as follows:

.. list-table::
   :widths: 15 15 70
   :header-rows: 1

   * - Field
     - Source
     - Description
   * - relOrder
     - Parameter / Default
     - The order for which the gateway was created.
   * - relPayment_method
     - Parameter
     - The used ``Payment_method`` when creating the gateway.
   * - relPayrexx_account
     - Parameter / Default
     - The used ``Payrexx_account`` when creating the gateway.
   * - state
     - Response
     - The state of the payrexx gateway (when it is created, this will be "waiting")
   * - date
     - Generated
     - Current timestamp
   * - gateway_id
     - Response
     - The id (int) of the created gateway. This can be used to retrieve further information about the gateway.
   * - reference_id
     - Response
     - The reference_id that was passed when creating the gateway (e.g. a random uuid)
   * - amount
     - Response
     - The amountn that should be payed. This is transformed back to ``BigDecimal`` for readability.



Online payment
^^^^^^^^^^^^^^

For the payment itself, the user is redirected to payrexx. If they pay successfully, they will be redirected to the
``successRedirectUrl`` (e.g. an empty page with visibility state ``payment-success``). If the payment fails they will be
redirected to the ``failedRedirectUrl``  (e.g. another empty page with visibility state ``payment-failed``). If a user
cancels his payment he will be redirected to the ``cancelledRedirectUrl`` (e.g. the payment page where he might select
another payment method).

Web-Hook
^^^^^^^^

As soon as a payment is registered, the webhook in all connected systems is called. The webhook receives a
``transaction_id``. With this id, the payrexx transaction can be resolved using the api.

From this transaction we use the ``reference_id`` to identify the ``Payrexx_transaction`` in the tocco system. If either
the payrexx account or the transaction is not available in the tocco system, the webhook call is ignored.

.. warning:: Payrexx transactions are not the same as gateways, a transaction might have been initialised in a gateway but they are not linked.

If a ``Payrexx_transaction`` was identified, we resolve the linked order. If the order is not yet booked, it will be
booked automatically and the newly created ``Debitor`` is used. If it is already booked, the existing ``Debitor`` is
resolved. On this debitor, we check if the there is already a ``Voucher`` with the ``transaction_id`` of the transaction
we are currently processing. If not, a ``Voucher`` is created with the data of the transaction as follows.

.. list-table::
   :widths: 15 85
   :header-rows: 1

   * - Field
     - Description
   * - relVoucher_type
     - Hardcoded ``payment``
   * - relDebitor
     - The previously resolved / created debitor
   * - relCurrency
     - The currency of the debitor
   * - relCredit_account
     - The debitor summary account of the related debitor
   * - relDebit_account
     - The account, linked on the ``Payrexx_account``
   * - voucher_date
     - The ``time`` of the transaction
   * - transaction_id
     - The ``id`` of the transaction
   * - amount
     - The ``amount`` of the transaction