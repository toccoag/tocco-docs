Qualification
=============

From Evaluation_node to Evaluation
----------------------------------

Our qualification structure is defined by a tree of ``Evaluation_node``, ``Evaluation_node_input_node`` and ``Input_node`` entities.
Through the ``EventQualificationAction``, these are transformed into concrete ``Evaluation`` and ``Input`` entities. These two trees, while related,
are technically disconnected, so any changes to the definition tree are not copied to the concrete entities. Any such changes can easily lead
to issues when not adjusted manually.

New input action
----------------

The new input action in React replaces the old action. It has paging and customizable forms, both of which made the old action unwieldy and slow.
It is contained in the ``input-edit`` package and communicates mainly through the ``InputEditResource``. The different input types are handled by
``InputEditHandler`` implementations.

Evaluation
----------

The evaluation, aggregation and checking of all input data is done through ``EvaluationTask``. For each participant, all the data is passed to Drools.
The average and sum calculation rules are defined in ``input-calculate.drl``.

Drools integration
^^^^^^^^^^^^^^^^^^

Drools is integrated into qualification module through the ``custom-rule-header.drl``. The qualification module hands of control in ``EvaluationFactory``
to the ``KieBaseCompiler``.

Entity wrappers
---------------

A lot of code in the qualification module works with wrappers around entities. These can be found in ``QualificationBeanFactory``.

Reports
-------

Qualification reports should be written with our provided directives and methods, since they are optimized for traversing tree structures.
Use ``DisplayQualificationStructureDirective`` and ``QualificationStructureMethod`` to load and traverse through such a tree.
Use ``EvaluationReportDataModifier`` so your report can work with ``Evaluation`` and ``Evaluation_data`` entities the same way.
Only work directly with the relevant entities when unavoidable.

REST
----

The package ``ch.tocco.nice2.optional.qualification.impl.resources.api`` contains resources to control the qualification module. Check Swagger for
detailed documentation.