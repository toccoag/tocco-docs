.. |pathToModuleFolder| replace:: ``customer/${CUSTOMER}``
.. |pathToModuleResourceFolder| replace:: ``customer/${CUSTOMER}/resources``

Add Customer Module
===================

Adding a new module contains the following steps:

.. list-table::
   :header-rows: 1
   :widths: 10 10 80

   * - Nr
     - Required
     - Description
   * - 1
     - ✔
     - `Add Module in Backoffice`_
   * - 2
     - ✔
     - `Create Basic Folder Structure`_
   * - 3
     -
     - `Add Content to module Folder`_
   * - 4
     -
     - `Add Java Source Folders`_


.. include:: add-module-in-backoffice.rst.template


Create Basic Folder Structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A new customer module can be created using the command ``gradlew createNewModule -P moduleType=customer -P moduleName=<name>``.

This command creates the necessary folder structure and project files and registers the new module in the ``settings.gradle`` file.

Add dependencies
^^^^^^^^^^^^^^^^

As mentioned earlier, for every customer module you create, there needs to be a corresponding
module defined in :term:`BO`. This definition includes dependencies on all optional modules
(called marketing modules in BO) that a customer has licensed.  This information can be used
to generate the dependencies for inclusion in ``build.gradle`` and ``module-info.java``. For this
go to the customer module and generate the dependencies as shown in the following screenshot:

.. figure:: resources/generate_gradle.png

     dependency generation in BO

.. important::

    The generated dependencies can be incorrect under certain circumstances. Go through them and ensure
    they are correct.

.. todo::

       I've been told dependencies generated can be wrong but what can be wrong there? Is there anything
       in particular for which to look?

Add dependencies to existing ``customer/${CUSTOMER}/build.gradle`` (3) and
``customer/${CUSTOMER}/src/main/java/module-info.java`` (4):


Configure Application
^^^^^^^^^^^^^^^^^^^^^

``${CUSTOMER}/src/main/resources/application.properties`` was created, edit it as needed::

    i18n.locale.available=de-CH,en,fr-CH,it-CH  # languages available on the system
    i18n.locale.default=de-CH                   # fall back language

    nice2.dbrefactoring.businessunits=unit1,unit2  # comma separated list of Business Units

Languages and business units can bo found in :term:`BO` on *Installation*.

.. todo::

    Where can the information about the mail domains and default mail addresses be found?

See also :ref:`application-properties-v3`.


.. include:: add-content-to-module-folder.rst.template

Add Java Source Folders
^^^^^^^^^^^^^^^^^^^^^^^

Java code (e.g. for listeners, actions, services, rest-resources, ...) can be added to the
``src/main/java`` folder and should be placed in a ``impl`` package.

* impl -> the implementation of the module specific Java code

.. include:: include-resources-in-maven-archive.rst.template

.. _HikariCP: https://github.com/brettwooldridge/HikariCP
