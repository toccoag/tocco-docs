.. |pathToModuleFolder| replace:: ``optional/${MODULE}``
.. |pathToModuleResourceFolder| replace:: ``optional/${MODULE}/resources``

Add Non-Customer Module
=======================

Adding a new module contains the following steps:

.. list-table::
   :header-rows: 1
   :widths: 10 10 80

   * - Nr
     - Required
     - Description
   * - 1
     - ✔
     - `Add Module in Backoffice`_
   * - 2
     - ✔
     - `Create Basic Folder Structure`_
   * - 3
     -
     - `Add Content to module Folder`_
   * - 4
     -
     - `Add Java Source Folders`_
   * - 5
     - ✔ **if 3 is done**
     - `Include Resources in Archive`_


.. include:: add-module-in-backoffice.rst.template


Create Basic Folder Structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A new customer module can be created using the command ``gradlew createNewModule -P moduleType={core, optional} -P moduleName=<name>``.

This command creates the necessary folder structure and project files and registers the new module in the ``settings.gradle`` file.
Core modules are also automatically added to the generic ``:customer`` module so that they are included for every customer.
Optional modules are automatically added to the test (and some other) customer.

.. include:: add-content-to-module-folder.rst.template


Add Java Source Folders
^^^^^^^^^^^^^^^^^^^^^^^

Java code (e.g. listeners, actions, services, rest-resources, ...) can just be added to the ``src/main/java`` folder of
the module. We usually separate the code into the 3 packages ``impl``, ``api`` and ``spi``.

* api -> defines services which can be injected by other modules
* spi -> defines classes which other modules can use or extend.
* impl -> the implementation of the module specific Java code

.. figure:: resources/module-folder-structure.png

To be able to access classes of ``api`` or ``spi`` in other modules, all packages must be listed as ``exports`` in
``module-info.java``

.. code-block:: Java

   open module nice.core.netui {

       exports ch.tocco.nice2.netui.api.actions;
       exports ch.tocco.nice2.netui.api.actions.security;
       // ... mode exports / imports
   }

.. include:: include-resources-in-maven-archive.rst.template

.. _country-specific-module:

Country-specific Module
^^^^^^^^^^^^^^^^^^^^^^^

If only changes to text resources are required, these can be directly done in the base module with the file ``language_de_DE.properties`` (See :ref:`text-resources-different-orthography`).
For changes that go beyond this, an additional, Germany-specific module is created, which extends the base module.

The Gradle module name should match the pattern ``optional|core:[country]:[module]-[country]``. 
The Germany-specific module for the ``optional:address`` module is therefore named ``optional:de:address-de``.
The module name in ``module-info.java`` should match the pattern ``nice.optional|core.[module].[country]``.
The ``module-info.java`` for the  Germany-specific module address looks like:

.. code-block:: java

  open module nice.optional.address.de { 
      requires nice.optional.address; 
  }

Text resources work as usual in country-specific modules.
The only difference is that the German file should be named  ``language_de_DE.properties`` (There will be no ``language_de.properties``). 
If you just want to change German texts you can use ``# nice2validate stop validation -- textresources below this point will not be validated.``
as else all language files must contain all text resources keys.

Back Office
-----------

For modules of the type customer module, the country in which the respective customer is based is set.
If no country is set, Switzerland is used as default.

For modules of the types core, marketing and intermediate several countries can be selected.
If there is a Germany-specific module for the address module, Germany is selected in the country field of the country-specific module in the back office.
In the base module address the address-de module is added as "country-specific module variants".
