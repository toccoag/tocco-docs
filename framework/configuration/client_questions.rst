Client Questions
================

Client questions enable you to get user input in backend code that does not directly interact with the user, like listeners or validators. If you are
in some custom code that **does** interact with the user directly, do not use client questions but build the user interaction into the regular logic
flow.

Client questions abort the current request via exception, and then runs the request again with the user input provided. This results in some
restrictions you should be aware of when using them:

* Do not ask client questions at a point in the logic where a transaction has already been committed, a mail has been sent or some other side effects
  have occurred. When the client question runs the request again, all those side effects will also run again.
* Do not ask client questions in code that will catch all exceptions and handle them itself. Your client question will never arrive on the frontend
  and your logic will be stuck in a strange unfinished state.
* Asking client questions in code that has no associated user, like batch jobs or system background tasks, will not be able to be answered. You'll
  just get some default answer back.

Examples
--------

Client questions are accessed through the :nice:`ClientQuestionService <ch/tocco/nice2/web/core/api/clientquestion/ClientQuestionService>`. Check the
implementations of :nice:`ClientQuestion <ch/tocco/nice2/web/core/api/clientquestion/ClientQuestion>` for available client question types.

Make user confirm some logic
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: java

  ConfirmClientAnswer confirmClientAnswer = clientQuestionService.askQuestion(new ConfirmClientQuestion("your id", "title", "text"));
  if (confirmClientAnswer.getAnswer()) {
    // logic here that should only run after confirmation
  }
  // ConfirmClientQuestion can only be confirmed or cancelled, so anything not in the if above can never run

Allow user to choose between Yes and No buttons
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: java

  ConfirmClientAnswer confirmClientAnswer = clientQuestionService.askQuestion(new YesNoClientQuestion("your id", "title", "text"));
  if (confirmClientAnswer.getAnswer()) {
    // logic here that should run if user chooses "Yes"
  } else {
    // logic here that should run if user chooses "No"
  }
  // YesNoClientQuestion can still be cancelled, but it might also return false as an answer

Suppress client questiony by providing answers beforehand
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: java

  PredefinedAnswers predefinedAnswer = new PredefinedAnswers().add(question -> question.getQuestionId().equals("IgnoredQuestion"), new ConfirmClientAnswer(true, ""));
  clientQuestionService.withAnswersForQuestion(predefinedAnswer).invokeRTE(() -> {
    // logic where client question with id "IgnoredQuestion" never gets shown to user
  })