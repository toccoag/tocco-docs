Custom React Actions
====================

Complex actions will require their own frontend. These are written in React in their own NPM package.
This requires some understanding of the new :doc:`/framework/client/index`. This document is not intended
to explain how to build React apps, only how to integrate them into our product as actions.

Custom actions start their own React app which then communicates with the backend through endpoints (either custom or generic ones).
They can be added to list or detail forms.

.. tip::

    :doc:`/framework/configuration/actions/simple-actions` can handle many common use cases, are easier to develop and require much less maintenance.
    If your use case seems common but is not suppported, ask someone from client development if a custom action is applicable.

Client
------

Action package
^^^^^^^^^^^^^^

See :ref:`Client - Custom actions <custom_react_actions>` for further information.

Make action available to admin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Under ``packages/apps/admin/src/routes/entities/components/Action/actions``, create a file for your action. This file will usually be very simple and contain
something like the snippet below. Be sure to add a dependency on your action package to the ``package.json`` of the ``admin`` package. If you need to
further wrap your action or pass some special inputs to it, this is the place to do it.

.. code-block:: jsx

    export {default} from 'tocco-example-action/src/main'

Add a line for your action to ``index.js`` in its ``actions`` object.

Make action available to legacy client
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Publish package to NPM. Afterwards, add it to the ``package.json`` of the Nice module that should be able to call your action. Finally,
create an ExtJS action that starts your application (see the example below). This ExtJS action can then be included in forms like usual.

.. code-block:: jsx

    Ext.ns('nice2.optional.module');

    loadJs('/js/tocco-example-action/dist/index.js?v=' + Nice.revision);

    nice2.optional.module.ExampleReactAction = Ext.extend(nice2.modules.entityExplorer2.actions.AbstractEntityExplorerAction, {

        className: "nice2.optional.module.ExampleReactAction",

        _doPerform: function() {
            var input = {
                selection: {
                    type: 'ID',
                    ids: this.getSelection().selectedEntities,
                    entityName: 'Entity_model'
                }
            };

            var reactContainer = new nice2.widget.ReactContainer({
                reactAppName: 'example-action',
                input: input,
                publicPath: '/js/tocco-example-action/dist/',
                externalEvents: {}
            });

            var win = new Ext.Window({
                title: getText('example-action'),
                modal: true,
                bodyStyle: 'padding: 15px;',
                autoHeight: true,
                width: 400,
                items: [
                    reactContainer
                ]
            });

            win.show();
        }
    });

    NetuiActionRegistry.register("nice2.optional.module.ExampleReactAction", nice2.optional.module.ExampleReactAction);

Form Configuration
------------------

To include a custom action in a form, you'll need to add it as an XML tag and add a bit of ACL.
Most properties from :doc:`/framework/configuration/actions/simple-actions` can be used here as well, except for
``runInBackground``. You can also use ``endpoint`` if you want to use a ``preAction``. Simply let your endpoint extend
``AbstractPreActionResource``. In addition, you can use ``fullscreen`` to let the action open in the entire window instead of just a modal.

.. code-block:: xml
    :caption: XML configuration

    <action name="example-react-action"
            label="action.example-react-action"
            appId="example-action"/>

.. code-block:: text
    :caption: ACL configuration

    netuiactions("example-react-action"):
        grant netuiPerform to actionrole;
