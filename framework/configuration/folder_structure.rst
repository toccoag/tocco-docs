Folder Structure
================

Here's an example of a general folder structure of a module:

Resource folder
---------------

.. figure:: resources/folder-structure-resources.png


Source folder
-------------

.. figure:: resources/folder-structure-src.png