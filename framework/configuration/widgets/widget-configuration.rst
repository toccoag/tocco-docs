Widget Configuration
====================

Every widget has it's own widget configuration. The widget configuration is added by initialvalues (``/resources/db/initialvalues/Widget.yaml``).

.. code-block:: yaml
    
    Widget:
      localized:
        label: widgets.my_widget.label
      fields:
        unique_id: my_widget
        active: true
        deleted: false
      single-relations:
        relWidget_app: nice_widget_app where unique_id = 'widget_app'
      config:
        identifier: unique_id
        priority: 10

If the widget has its own app instead of using a default one like ``entity-browser``, a custom ``Widget_app`` will also need to be created.

.. code-block:: yaml
    
    Widget_app:
      fields:
        unique_id: widget_app
        app_name: client-app
        package_name: widget-bundle
      config:
        identifier: unique_id
        priority: 20

Usually all widgets are inside the ``widget-bundle`` package.

Licencing
^^^^^^^^^

By default all widgets are inactive (active: false). If a customer licences a widget, a tocco support user needs to
activate the licenced widget manually in the respective system(s). This can be done by accessing
``https://{installation}.tocco.ch/{new-admin}/e/Widget`` (e.g. ``https://master.tocco.ch/tocco/e/Widget``).

If a widget is no longer licenced and used by a customer it may be deactivated in the same way.

Entity
^^^^^^

The ``My_widget_widget_config.xml`` entity has to be added.

.. note::

    ``admin-delete-action`` and ``admin-create-action`` have to be set to ``false``.

.. code-block:: xml
    :caption: My_widget_widget_config.xml

    <?xml version="1.0" encoding="UTF-8"?>
    <entity-model
        xmlns="http://nice2.tocco.ch/schema/entityModel.xsd"
        admin-delete-action="false"
        admin-create-action="false">
      <key name="pk" type="serial"/>
      <visualisation>
        <sorting>pk</sorting>
        <display language="freemarker">${baseData.pk?c}</display>
      </visualisation>
    </entity-model>

The new ``My_widget_widget_config`` entity has then to be set as a relation on the root ``Widget_config``.

.. code-block:: xml
    :caption: Widget_config_relMy_widget_widget_config.xml
    
    <?xml version="1.0" encoding="UTF-8"?>
    <relation xmlns="http://nice2.tocco.ch/schema/relation.xsd">
      <source entity-model="Widget_config">
        <delete cascade="yes" visualisation="silent"/>
      </source>
      <target entity-model="My_widget_widget_config"/>
      <cardinality>n:0..1</cardinality>
    </relation>

.. tip::

    Do not forget to generate the changesets.


Common fields for the widget configuration are:

- ``entity_name``: entity name used in the widget (add ``<default type="hard">ENTITY_NAME</default>`` to entity model and do not show field in the forms of the widget configuration).
- ``form_base``: base name of the forms used in the widget (add ``<default type="hard">FORM_BASE</default>`` to entity model and do not show field in the forms of the widget configuration). See :ref:`widget-configurable-form-base` if the form_base should not be hard coded.

Common relations for the widget configuration are:

- ``relBusiness_unit``: Business unit in which the widget runs
- ``relPage_limit``: Number of items showed per page in the list
- ``relReport``: Reports added to list/detail if there exists a report placement
- ``relSearch_filter``: Search filter applied to list of widget
- ``relWidget_search_form_type``: How the search should be rendered

ACL
^^^
.. code-block:: text
    :caption: ACL configuration (entity.acl)

    entityManager(My_widget_widget_config):
      grant create to widgetmanager;

    entity(My_widget_widget_config):
      grant access, delete to widgetmanager;
      grant access(read) to widgetguest;

The common fields ``entity_name`` and ``form_base`` should be read-only:

.. code-block:: text
    :caption: ACL configuration (entity.acl)

    entityPath(My_widget_widget_config, entity_name, form_base):
      deny access(write);


Forms
^^^^^

The list and detail form for the ``My_widget_widget_config`` should be added to ``/resources/model/forms``.

Any forms needed inside the widget has to be added to ``/resources/model/widgetforms`` similar to the legacy ``publicforms`` folder.

Config relation contribution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``My_widget_widget_config`` entity will be mapped to input props to the client widget.
Therefore the field names will be transformed automatically from snake case to camel case (e.g. ``allow_create`` woudl result in ``allowCreate``).

If this is not good enough (e.g. for relations) a ``ConfigRelationContribution`` can be added to map the name explicitly:

.. code-block:: java

    @Bean
    public ConfigRelationContribution myRelationRelationContribution() {
        return new ConfigRelationContribution("my_widget", "widget-bundle", "relMyRelation", "relationKey");
    }

Common relations contributions applied globally:

- ``relBusiness_unit``: ``businessUnit``
- ``relPage_limit``: ``limit``
- ``relReport``: ``reportIds``
- ``relSearch_filter``: ``searchFilters``
- ``relWidget_search_form_type``: ``searchFormType``

.. _visibility_status_configuration:

Visibility status configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Visibility status are configured as ``Widget_visibility_status`` entities. Create one for each visibility status your app supports and relate it to your
``Widget_app``. The name of the status used in the client app can be entered into the ``visibility_status`` field.

.. code-block:: yaml
    :caption: Initial values for a ``list`` visibilty status on ``widget-app``
    
    Widget_visibility_status:
      localized:
        label: widget_visibility_status.widget_app.list.label
      fields:
        unique_id: widget_app_list
        visibility_status: list
      single-relations:
        relWidget_app: nice_widget_app where unique_id = 'widget_app'
      config:
        identifier: unique_id
        priority: 10

A code block configured to use a visibility status can then be used. These can be generated automatically by the
``widget-visibility-status-code-copy`` action on the ``Widget_config_detail``. See :ref:`visibility_status_integration` on how to change visibility status
in the widget app.

Removing a widget
^^^^^^^^^^^^^^^^^

If a  widget may no longer be used because it has been deprecated, replaced or some other reason,
you will need to write a changeset that sets the ``deleted`` field of the corresponding ``Widget`` entity to ``true``.

If a customer simply loses the licence for a widget, just set the ``active`` flag back to ``false``.

If existing widget configurations should no longer be able to be started, you can deactivate them by setting the
``active`` field on them to ``false``.