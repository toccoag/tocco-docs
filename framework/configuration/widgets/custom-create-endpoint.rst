Custom Create Endpoint
======================

Sometimes a widget may need a custom create action. This can be because some additional logic is needed when creating
entities or it might be that it needs to be run in privileged mode because no ACL rule for create rights is possible
or wanted.

To create such an endpoint, you will probably want to implement the
:nice:`CustomCreateEntitiesResource <ch/tocco/nice2/rest/entity/spi/entities/CustomCreateEntitiesResource>` interface
and extend from :nice:`AbstractEntitiesResource <ch/tocco/nice2/rest/entity/spi/entities/AbstractEntitiesResource>`.
Do keep security in mind and reference the documentation on :ref:`privileged rebinding <privileged-rebinding>` if you are unsure about it.

After the endpoint is created, you can configure the `*_create` form to use it by setting the ``create-endpoint``
attribute on the top most ``<form>`` element.
