.. _widget-configurable-form-base:

Configurable form base
======================

Per default the form base is hard coded in the widget configuration.
If the form base should be configurable via a dropdown in the widget configuration, the following steps are necessary:

1. Adjust entity model (and generate changesets)

.. code-block:: xml
    :caption: add new form entity (``WIDGET_NAME_form.xml``)

    <?xml version="1.0" encoding="UTF-8"?>
    <entity-model
        xmlns="http://nice2.tocco.ch/schema/entityModel.xsd"
        type="lookup">
      <key name="pk" type="serial"/>
      <field name="unique_id" type="identifier">
        <validations>
          <mandatory/>
        </validations>
      </field>
      <field name="label" type="string" localized="true">
        <validations>
          <mandatory/>
        </validations>
      </field>
      <field name="sorting" type="sorting"/>
      <field name="form_base" type="string">
        <validations>
          <mandatory/>
        </validations>
      </field>
      <field name="default_form" type="boolean"/>
      <visualisation>
        <sorting>sorting,label</sorting>
        <display language="freemarker" expression="${baseData.label}"/>
      </visualisation>
    </entity-model>

.. code-block:: xml
    :caption: add new relation between widget config and form entity (``WIDGET_NAME_widget_config_relWIDGET_NAME_form.xml``)

    <?xml version="1.0" encoding="UTF-8"?>
    <relation xmlns="http://nice2.tocco.ch/schema/relation.xsd">
      <source entity-model="WIDGET_NAME_widget_config">
        <delete cascade="no"/>
        <display show="false"/>
      </source>
      <target entity-model="WIDGET_NAME_form">
        <display show="false"/>
      </target>
      <cardinality>n:1</cardinality>
      <default type="query">default_form</default>
    </relation>

2. Migrate existing widget configurations

if there already exists some widget configuration, the mandatory form relation must be set

.. code-block:: xml
    :caption: add initialvalues changeset

    <changeSet author="anonymous" dbms="postgresql" id="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX">
      <preConditions onFail="MARK_RAN">
        <not>
          <sqlCheck expectedResult="0">SELECT COUNT(*) FROM nice_WIDGET_NAME_widget_config WHERE fk_WIDGET_NAME_form IS NULL</sqlCheck>
        </not>
      </preConditions>
      <customChange class="ch.tocco.nice2.dbrefactoring.impl.data.YamlInitialValueCustomChange">
        <param name="module" value="MODULE"/>
        <param name="valueFileName" value="WIDGET_NAME_form.yaml"/>
      </customChange>
      <update tableName="nice_WIDGET_NAME_widget_config">
        <column name="fk_WIDGET_NAME_form" valueComputed="(SELECT pk FROM nice_WIDGET_NAME_form WHERE default_form)"/>
        <where>fk_WIDGET_NAME_form IS NULL</where>
      </update>
    </changeSet>


3. Add initialvalues

.. code-block:: yaml
    :caption: ``WIDGET_NAME_form.yaml``

    WIDGET_NAME_form:
      localized:
        label: entities.WIDGET_NAME_form.default.label
      fields:
        form_base: VALUE
        unique_id: default
        default_form: true
        sorting: 10
        active: true
      config:
        identifier: unique_id

4. Add acl

.. code-block:: text
    :caption: ACL configuration (``entity.acl``)

    entity(WIDGET_NAME_form):
        grant access(read);
        grant access, delete to configurator;

    entityManager(WIDGET_NAME_form):
        grant create to configurator;

    entityPath(WIDGET_NAME_form, unique_id):
        deny access(write);

5. Adjust form of widget configuration

.. code-block:: xml
    :caption: add new file if a standard widget is used (``WIDGET_NAME_widget_config_detail.xml``)

    <?xml version="1.0" encoding="UTF-8"?>
    <form xmlns="http://nice2.tocco.ch/schema/formModel.xsd" generic-actions="false">
      <horizontal-box labeled="false">
        <vertical-box labeled="false">
          <vertical-box name="master_data" labeled="true">
            <field data="relWIDGET_NAME_form"/>
          </vertical-box>
        </vertical-box>
      </horizontal-box>
    </form>

6. Add text resources

.. code-block:: text
    :caption: ``language_XX.properties``

    // de
    entities.WIDGET_NAME_form=XXXXX Formular
    entities.WIDGET_NAME_form.label=Bezeichnung
    entities.WIDGET_NAME_form.sorting=Sortierung
    entities.WIDGET_NAME_form.unique_id=Kürzel
    entities.WIDGET_NAME_form.form_base=Formular
    entities.WIDGET_NAME_form.default_form=Standard
    forms.WIDGET_NAME_form_detail.master_data=Stammdaten
    entities.WIDGET_NAME_form.default.label=Standard

    // en
    entities.WIDGET_NAME_form=XXXXX form
    entities.WIDGET_NAME_form.label=Label
    entities.WIDGET_NAME_form.sorting=Sorting
    entities.WIDGET_NAME_form.unique_id=Unique ID
    entities.WIDGET_NAME_form.form_base=Form basis
    entities.WIDGET_NAME_form.default_form=Default
    forms.WIDGET_NAME_form_detail.master_data=Master data
    entities.WIDGET_NAME_form.default.label=Default

    // fr
    entities.WIDGET_NAME_form=XXXXX Formulaire
    entities.WIDGET_NAME_form.label=Appellation
    entities.WIDGET_NAME_form.sorting=Tri
    entities.WIDGET_NAME_form.unique_id=Code
    entities.WIDGET_NAME_form.form_base=Formulaire base
    entities.WIDGET_NAME_form.default_form=Standard
    forms.WIDGET_NAME_form_detail.master_data=Données de base
    entities.WIDGET_NAME_form.default.label=Standard

    // it
    entities.WIDGET_NAME_form=XXXXX Formulario
    entities.WIDGET_NAME_form.label=Denominazione
    entities.WIDGET_NAME_form.sorting=Ordine
    entities.WIDGET_NAME_form.unique_id=Codice
    entities.WIDGET_NAME_form.form_base=Formulario base
    entities.WIDGET_NAME_form.default_form=Standard
    forms.WIDGET_NAME_form_detail.master_data=Dati principali
    entities.WIDGET_NAME_form.default.label=Standard

7. Add contribution

.. code-block:: java
    :caption: add contribution to ``Nice2Application.java`` / ``XXXConfiguration.java``

    @Bean
    public FormBaseWidgetConfigContribution WIDGET_NAMEFormBaseWidgetConfigContribution() {
        return new FormBaseWidgetConfigContribution("WIDGET_NAME", "relWIDGET_NAME_form");
    }
