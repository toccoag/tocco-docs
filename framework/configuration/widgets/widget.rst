React Widgets
=============

Widgets with own frontend logic are written in React. 
This requires some understanding of the new :doc:`/framework/client/index`. This document is not intended
to explain how to build React apps, only how to integrate them into the our product as widgets.

Client
------

Widget package
^^^^^^^^^^^^^^

See :ref:`Client - Custom widgets <custom_react_widgets>` for further information.
