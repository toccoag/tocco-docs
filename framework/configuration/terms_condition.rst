Terms condition
===============

If a widget should contain terms condition and/or privacy protection, the Tocco framework provides a generic feature.
This section describes how to add both condition and privacy protection. However you can add them independently.
So you can add only privacy protection or create a new type.

Widget config
-------------

Add relations:

.. code-block:: xml
    :caption: ``WIDGET_CONFIG_ENTITY_relTerms_condition.xml``

    <?xml version="1.0" encoding="UTF-8"?>
    <relation xmlns="http://nice2.tocco.ch/schema/relation.xsd">
        <source entity-model="WIDGET_CONFIG_ENTITY">
            <delete cascade="no"/>
        </source>
        <target entity-model="Terms_condition">
            <delete cascade="no"/>
            <display show="false"/>
        </target>
        <cardinality>n:0..1</cardinality>
    </relation>


.. code-block:: xml
    :caption: ``WIDGET_CONFIG_ENTITY_relTerms_condition_privacy.xml``

    <?xml version="1.0" encoding="UTF-8"?>
    <relation xmlns="http://nice2.tocco.ch/schema/relation.xsd">
        <source entity-model="WIDGET_CONFIG_ENTITY" name="relTerms_condition_privacy">
            <delete cascade="no"/>
        </source>
        <target entity-model="Terms_condition" name="relWIDGET_CONFIG_ENTITY_config_privacy">
            <delete cascade="no"/>
            <display show="false"/>
        </target>
        <cardinality>n:0..1</cardinality>
    </relation>

Add the text resources:

.. code-block:: text
    :caption: ``language_xx.properties``

    entities.WIDGET_CONFIG_ENTITY.relTerms_condition=AGB Konfiguration
    entities.WIDGET_CONFIG_ENTITY.relTerms_condition_privacy=Datenschutz Konfiguration

Add relations to existing detail form:

.. code-block:: xml
    :caption: ``WIDGET_CONFIG_ENTITY_detail.xml``

    <single-select-box data="relTerms_condition" constriction="terms_condition"/>
    <single-select-box data="relTerms_condition_privacy" constriction="terms_privacy"/>

Association
-----------

If there is no association releation between the entity and ``Terms_condition_confirmation`` add one.

Add relation:

.. code-block:: xml
    :caption: ``Terms_condition_confirmation_relENTITY.xml``

    <?xml version="1.0" encoding="UTF-8"?>
    <relation xmlns="http://nice2.tocco.ch/schema/relation.xsd">
        <source entity-model="Terms_condition_confirmation">
            <delete cascade="no"/>
        </source>
        <target entity-model="ENTITY">
            <delete cascade="no"/>
            <display show="true" order="XXXXX"/>
        </target>
        <cardinality>n:0..1</cardinality>
    </relation>

Add relation to detail form:

.. code-block:: xml
    :caption: ``Terms_condition_confirmation_detail.xml``

    <?xml version="1.0" encoding="UTF-8"?>
    <form xmlns="http://nice2.tocco.ch/schema/formModel.xsd">
    <horizontal-box name="box1" labeled="false">
        <vertical-box name="box1" labeled="false">
            <vertical-box name="association" labeled="true">
                <field data="relENTITY" position="after:relUser"/>
            </vertical-box>
        </vertical-box>
    </horizontal-box>
    </form>

Add acl rule:

.. code-block:: text
    :caption: ``entity.acl``

    entity(Terms_condition_confirmation):
        grant access(read) to XXXXXmanager, XXXXXguest if exists(relENTITY);

Add contribution for term condition source entity:

.. code-block:: java

    @Bean
    public TermsConditionConfirmationSource XXXXXTermsConditionConfirmationSource() {
        return new TermsConditionConfirmationSource("ENTITY", "relENTITY");
    }

If you want to link multiple source entities use ``new TermsConditionConfirmationSource("ENTITY", "relENTITY", "relINTERMEDIATE");``.
In such a case ``ENTITY.relINTERMEDIATE`` is linked as ``relENTITY`` to the ``Terms_condition_confirmation``.


Widget forms
------------

Add the term elements to the widget form:

.. code-block:: xml

    <terms type="conditions" path="" use-label="hide"/>
    <terms type="privacy_protection" path="" use-label="hide"/>

If no ``Terms_condition`` for the given type is available, the field will not be rendered.

Per default the path should be an empty string. If the ``Terms_condition_confirmation`` should be linked with a nested relation,
set ``relRELATION[new]`` as path. For example in the event registration widget a user is created (``data="User"`` in create/update form).
However the ``Terms_condition_confirmation`` should be linked with the created ``Registration``. So the path is ``relRegistration[new]``.

Custom rebinding endpoint (Optional)
------------------------------------

Some widgets have a custom REST endpoint for rebinding. In the rebind limit configuration the paths ``relTerms_condition_confirmation[conditions]`` and
``relTerms_condition_confirmation[privacy_protection]`` must be added.

Custom condition resolver (Optional)
------------------------------------

Resolving the ``Term_condition`` has the following priority:

1. defined ``Term_condition`` on widget config
2. ``TermsConditionResolver`` registered for the entity model

A ``TermsConditionResolver`` is required if depending on the current entity another ``Term_condition`` should be loaded
(For example in the event registration). If this case is used, ``put(formData.setTermsCurrentEntity(MODEL_NAME, KEY))``
must be called during the initialization in the frontend code.
