Spring Boot - Run Environment
=============================

The run environment is equivalent to the currently active spring profile, which can be set using the
``spring.profiles.active`` parameter (for example add ``--spring.profiles.active=development`` to the command line).
The following profiles are supported:

``development``

    Some features are disabled:

    * do not create search index
    * disable history
    * allow insecure connection
    * no outgoing mails
    * …

``production``/``test``

     Run level for use with production and test systems respectively.

``update``

     Minimal system used for schema upgrades.

In the code the value can be requested using the ``RunEnvironmentService``.

.. tip::

    Profile can be set via ``SPRING_BOOT`` env. var. also.SPRING_PROFILE
