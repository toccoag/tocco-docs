History Configuration
=====================

History Relation Configuration
------------------------------

By default, any relations to non-lookup entities are not included in the entity history.
Such relations can explicitly included with the `HistoryRelationInclusionContribution` bean.

    .. code-block:: java
        :caption: Include Classroom_relAdmin, Classroom_relModerator and Classroom_relUser in Classroom entity history

        @Bean
        public HistoryRelationInclusionContribution classRoomUserRelations() {
            return new HistoryRelationInclusionContribution("Classroom", Set.of("relAdmin", "relModerator", "relUser"));
        }

Additionally, fields, lookup relations and entire entity models can be explicitly excluded from the history
with the `HistoryExclusionContribution` bean.

    .. code-block:: java
        :caption: Exclude Thumbnail entity from history

        @Bean
        public HistoryExclusionContribution thumbnailHistoryExclusion() {
            HistoryExclusionContribution contribution = new HistoryExclusionContribution();
            contribution.setModelName("Thumbnail");
            return contribution;
        }

    .. code-block:: java
        :caption: Exclude User_relAffiliation from User entity history

        @Bean
        public HistoryExclusionContribution userAffiliationExclusion() {
            HistoryExclusionContribution contribution = new HistoryExclusionContribution();
            contribution.setModelName("User");
            contribution.setRelationName("relAffiliation");
            return contribution;
        }

    .. code-block:: java
        :caption: Exclude entrance and exit fields on User from entity history

        @Bean
        public HistoryExclusionContribution userEntranceExitHistoryExclusionContribution() {
            HistoryExclusionContribution contribution = new HistoryExclusionContribution();
            contribution.setModelName("User");
            contribution.setFieldName("entrance,exit");
            return contribution;
        }