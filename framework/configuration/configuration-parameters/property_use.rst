.. index:: application property; set property

Set Application Properties
==========================

How to Set Properties
---------------------

Primary ways to set properties:

* **During development** create customer/${CUSTOMER}/etc/application-development.properties.

* **In production**, use Ansible's :doc:`env </devops/app_management/ansible_env>` variable. If available,
  set the corresponding Ansible variable listed in :doc:`properties_available` instead.

* Changes needed in **production and development**, commit to
  *customer/${CUSTOMER}/etc/application.properties*


More Ways to Set Properties
---------------------------

=================================================================== ============================================
 How/Where                                                           Use cases
=================================================================== ============================================
 Command line: ``java -Dproperty=value``                             * Development: options that should be
                                                                       enabled for all customers
                                                                     * Production: used when settings
                                                                       need to be overriddenduring DB
                                                                       refactoring.

 customer/${CUSTOMER}/etc/application.properties [#f1]_              * Hardcoded setting which are applied
                                                                       during development and in
                                                                       **production**.

 customer/${CUSTOMER}/etc/application-development.properties [#f1]_  * Development: options that should
                                                                       be enabled for a single customer
                                                                       locally only.

 Environment variable: ``property=value``                            * Docker: used to set properties
                                                                       when running in Docker
                                                                       (:doc:`/devops/openshift/nice_docker_image`)

                                                                     * OpenShift: may be used to enable setting
                                                                       temporally for debugging::

                                                                           oc set env dc/nice property=value

                                                                       Removal::

                                                                           oc set env dc/nice property=-

                                                                       **Ansible will overwrite any changes on
                                                                       next run.**

 Ansible :term:`config.yml`/:term:`global.yml`                       * Production: used to set properties on
                                                                       Openshift. In the background, Ansible
                                                                       sets the corresponding environment
                                                                       variables.

=================================================================== ============================================


.. rubric:: Footnotes

.. [#f1] Only the application-${SPRING_PROFILE}.properties file matching the current Spring Boot Profile
         is loaded. See :doc:`run_env`.
