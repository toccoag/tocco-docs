.. _application-properties-v3:

Application Property Implementation
===================================

Application properties are used to configure an installation. Each customer module contains an
``application.properties`` file that can be used to override the default properties.
These files are located under ``path/to/nice2/customer/CUSTOMER_NAME/src/main/resources``.

Application properties can be overwritten in a file ``application-{profile}.properties`` also located in the same directory.
This can be useful to test something locally or if the properties are sensitive (e.g. usernames and passwords).
An ``application-{profile}.properties`` file is ignored by git (.gitignore) and must be created manually.

.. note::

    The ``{profile}`` refers to the currently active spring profile (current Nice run environment), for example
    ``application-development.properties``.

Application properties can be injected into any spring component.

Injecting properties
--------------------

Properties can be injected into a setter method or directly into a private field:

.. code-block:: java

    @Value("${nice2.websocket.idle.timeout.seconds}")
    private int idleTimeoutSeconds;


.. code-block:: java

    @Value("${nice2.duplicate.min.trigramSimilarity}")
    public void setTrigramSimilarity(double trigramSimilarity) {
        this.trigramSimilarity = trigramSimilarity;
    }

Default Values
--------------

Default values can be stored in an additional ``*.properties`` file per module.
The additional file needs to be registered in the ``@Configuration`` class:

.. code-block:: java

    @Configuration
    @PropertySource("classpath:/ch/tocco/nice2/optional/calendar/impl/default.properties")
    public class CalendarConfiguration extends AbstractConfiguration {
