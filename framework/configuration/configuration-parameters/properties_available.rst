.. index:: application property; available properties

Available Application Properties
================================

Captchas
--------

.. tip::

    Captcha is configured via Ansible, see :term:`global.yml`.

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - nice2.userbase.captcha.v2.client.key
     - ``F8Gn_***********************************``
     - Captcha Key ID

   * - nice2.userbase.captcha.v2.secret
     - ``F8Gn_***********************************``
     - Capcha secret key

   * - nice2.userbase.captcha.v2.domains
     - ``example.net,subdomain.example.net``
     - Domains on which captchas may appear

List of business units

This setting should be hardcoded in ``application.properties``.

Customer-Specifics
------------------

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - nice2.dbrefactoring.businessunits
     - ``unit1,unit2,unit3``
     - List of business units

       This setting should be hardcoded in ``application.properties``.

   * - _`i18n.locale.available`
     - ``de-CH,en-US``
     - Languages available on the system

       Available locales are:

       * ``de-CH``
       * ``en-US``
       * ``fr-CH``
       * ``it-CH``

       This setting should be hardcoded in ``application.properties``.

   * - i18n.locale.default
     - ``de-CH``
     - Default locale

       Must be a locale that also appears in `i18n.locale.available`_.

       This setting should be hardcoded in ``application.properties``.


.. index:: debugging; application properties
.. _properties-debugging:

Debugging
---------

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - _`hibernate.main.hikari.leakDetectionThreshold`
     - ``5000`` (milliseconds)
     - Log DB connection not returned to pool in given time

       A stacktrace from where the connection was obtained is logged. Should
       the connection be returned after the limit is exceeded, this is
       logged too.

       **Ansible:**

       variable: |env|

   * - nice2.conversion.wkhtmltopdf.keepTemporaryFiles
     - ``true``/``false``
     - Whether to keep temporary files when creating reports

       Iff ``true``, intermediate HTML files kept for debugging
       purposes.

       See :doc:`/framework/architecture/reports/wkhtmltopdf`

   * - nice2.model.menu.writeAdminACL
     - ``true``/``false``
     - Write admin menu ACL to disk for debugging

   * - _`nice2.messaging.greenmail.imapSetup`
     - ``on``/``off``
     - Wheter to start a local test mail server

       See :doc:`/framework/modules/core/greenmail`

   * - nice2.metrics.minExecutionMillis
     - ``1000``
     - Minimum duration for an event to be logged

       Used for events logged in the `Activity report (System_activity)`_.

       **Ansible:**

       variable: |env|

   * - nice2.persist.enableSqlLogging
     - ``true`` / ``false``
     - Intercept DB queries to allow logging the duration and number of DB
       queriess in the `Activity report (System_activity)`_.

       **Ansible:**

       variable: |env|


History Database
----------------

All properties in `Main Database`_ can be used with slight differences in naming:

* Property name: hibernate.\ :green:`main`\ .databaseName →
  hibernate.\ :green:`history`\ .databaseName
* Ansible variable: db_name → :green:`history`\ _db_name

Additional properties relevant only to history DB:

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - nice2.persist.history.maximumAge
     - ``24`` months (default)
     - Retention time for record in the entity history

       **Warning**, there is also a batch job trimming
       the history installed on all DB servers. This
       because the in-app batch job was only added in 3.0.
       When this value is increased, it is likely that the
       batch job on the DB server will still trim the
       history retaining the history for too short. See
       :ansible-repo:`roles/postgres-nice/tasks/main.yml`

       **Ansible:**

       variable: |env|

   * - nice2.persist.history.persistence.state
     - ``DEFAULT`` (default)/``DISABLED``/``ENABLED``
     - Whether to enable entity history

       If set to ``DEFAULT``, history is enabled when :doc:`run_env`
       is set to ``production`` or ``test``

       **Ansible:**

       variable: |env|


Geolocation
-----------

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - nice2.optional.geolocation.google.apiKey
     - ``***************************************``
     - Google API key to use for geolocation service

       Only effective with::

           nice2.optional.geolocation.serviceName=google

       **Ansible:**

       variable: |env|

   * - nice2.optional.geolocation.serviceName
     - ``google``/``nominatim``
     - Geolocation service to use

       ``nominatim``, based on OpenStreetMap, is the default.

       As alternative, ``google`` can be used but requires a paid account. When
       using ``google``, ``nice2.optional.geolocation.google.apiKey`` must be set
       also.

       **Ansible:**

       variable: |env|


Mail
----

.. tip::

   For debugging mail locally, see `nice2.messaging.greenmail.imapSetup`_.


Incoming Mail
^^^^^^^^^^^^^

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Examples
     - Description
   * - incamail.\*
     -
     - Incamail-related settings

       Documented in :doc:`/bs/incamail`

       **Ansible:**

       variables: |env|

   * - nice2.optional.mailintegration.\*
     -
     - Fetch Mail via IMAP/POP3

       Documented in :doc:`mail_properties`

       **Ansible:**

       variables: |env|


Outgoing Mail
^^^^^^^^^^^^^

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Examples
     - Description

   * - email.hostname:
     - mxout1.example.com
     - Outgoing mail server

       See:

       * :ref:`use-a-custom-mail-server`
       * Ansible: :ref:`ansible-configure-mail-relay`

   * - email.port:
     - ``25``
     - Port used by outgoing mail server

       See:

       * :ref:`use-a-custom-mail-server`
       * Ansible: :ref:`ansible-configure-mail-relay`

   * - email.auth.username
     - ``user``/``user@example.com``
     - Username sent to outgoing mail server

       See:

       * :ref:`use-a-custom-mail-server`
       * Ansible: :ref:`ansible-configure-mail-relay`

   * - email.auth.password
     - M3UPXyOfpUjIWSHrqLU9
     - Password sent to outgoing mail server

       See:

       * :ref:`use-a-custom-mail-server`
       * Ansible: :ref:`ansible-configure-mail-relay`

   * - email.default.from
     - ``info@example.net``
     - Global fallback mail address

       Ansible: :ref:`ansible-configure-default-sender-addresses`

   * - email.noreply.from
     - ``noreply@example.net``
     - Global fallback noreply mail address

       Ansible: :ref:`ansible-configure-default-sender-addresses`

   * - email.allowedFromDomainsRegex
     - ``example\.com|user@example\.net``
     - Domains that may appear as sender

       See:

       * :ref:`allowed-from-domains-regex`
       * Ansible: :ref:`ansible-configure-email-sender-domains`

   * - _`recipientrewrite.enabled`
     - ``true``/``false``
     - Whether `recipientrewrite.mappings`_ should be applied

       See:

       * :ref:`rewrite-recipient-address`
       * Ansible: :ref:`ansible-restrict-allowed-mail-recipients`

   * - _`recipientrewrite.mappings`
     - ``example.com -> nowhere@example.com``
     - Rewrite email recipients on outgoing mail

       This is used to only allow mails to be sent to certain domains
       on test and pilot systems.

       Ignored unless `recipientrewrite.enabled`_ is set to ``true``.

       See:

       * :ref:`rewrite-recipient-address`
       * Ansible: :ref:`ansible-restrict-allowed-mail-recipients`

   * - email.sendPerSecond
     - ``2.5``
     - Max. number of mails sent per second

       Throttling is desired to avoid receiving MTAs blocking our relay based
       on mail volume. Some providers temporarily block high-volume relays,
       delaying mail delivery for all customers using the same relay.

       **Ansible:**

       | default: hardcoded in :term:`global.yml`
       | variable: |env|


Main Database
-------------

.. tip::

  Any `HakariCP Configuration Options`_  may be used by prefixing ``hibernate.main.hikari.``
  (or ``hibernate.history.hikari.`` for the history DB). Only some are documented here.

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Example
     - Description

   * - hibernate.main.databaseNamedb_server
     - ``nice_test``
     - Database name

       **Ansible:**

       | default: derived from installation name
       | variable: ``db_name``

   * - hibernate.main.hikari.leakDetectionThreshold
     -
     - See `hibernate.main.hikari.leakDetectionThreshold`_

   * - hibernate.main.hikari.maximumPoolSize
     - ``15``
     - Maximum Pool size

       Maximum number of simultaneous connections
       to the DB server and, hence, the maximum
       number of simultaneously running SQL
       queries.

       **Ansible:**

       variable: |env|

   * - hibernate.main.password
     - ``******************************``
     - Database password

       **Ansible:**

       | default: deterministically derived [#f1]_
       | variable: ``db_password``

   * - hibernate.main.serverName
     - ``db1.tocco.cust.vshn.net``
     - Database server name

       **Ansible:**

       variable: ``db_server``

   * - hibernate.main.user
     - ``nice_test``
     - Database user

       **Ansible**:

       | default: derived from installation name
       | variable: ``db_user``

   * - nice2.dbrefactoring.removeChangeLogLock
     - ``true``/``false`` (default)
     - Whether to forcefully remove lock during schema upgrades

       Iff ``true``, any lock created by Liquibase is removed before
       attempting a schema upgrade. Liquabase does not remove stale locks.

       This is used on OpenShift to remove stale locks. This is safe because
       Kubernetes ensures only one schema upgrade is running at a time.


Miscellaneous
-------------

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - action.selection.warning.threshold
     - ``100``
     - Number of records that trigger a confirmation dialog

       Old admin client:

       When *N* or more records are selected in the UI, the user
       warned that processing this many records may be time-consuming.

       New admin client:

       Use attributes ``showConfirmation`` and ``confirmationThreshold``
       on :doc:`../forms` instead. See :nice-commit:`ec32c11037ae91d21bb0b4baf5c9b6e420c491fb`.

       **Ansible:**

       variable: |env|

   * - nice2.doublet.limit
     - ``50000``
     - Max. results returned when searching `duplicates`_.

       **Ansible:**

       variable: |env|

   * - nice2.optional.usermanager.generatelogin.disabled
     - ``true`` (default)/``false``
     - Whether to create a login for every person

       Iff ``false``, whenever a person (entity User) is created
       a corresponding login (entity Principal) is created.

       **Ansible:**

       variable: |env|


Performance Tuning
------------------

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - persist.tasks.commandservice.maxThreads
     - ``5``
     - Number of asynchronous tasks run concurrently.

       This configuration applies to the thread pool of the
       :nice:`CommandService <ch/tocco/nice2/persist/core/api/exec/CommandService>`
       which is used to execute smaller tasks asynchronously.

       **Ansible:**

       variable: |env|

   * - spring.quartz.properties.org.quartz.threadPool.threadCount
     - ``3``
     - Number of background tasks run concurrently.

       This corresponds to the max. number of tasks that may be marked
       *Running*, at any given time, in `Task_execution`_.

       **Compatibility:**

       Available since Nice >= 3.1.

       In Nice < 3.1, this was governed by ``taskQueue.main.corePoolSize``.

       **Ansible:**

       variable: |env|


Fulltext-Search
---------------

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - nice2.enterprisesearch.elasticsearch.hostName
     - ``es1.stage.tocco.cust.vshn.net``
     - hostname for elasticsearch, if this is not set, the FakeFulltextIndexService is used (e.g. for local development)

   * - nice2.enterprisesearch.elasticsearch.port
     - ``443``
     - port to be used with elasticsearch

   * - nice2.enterprisesearch.elasticsearch.indexName
     - ``nice-test307``
     - name of the elasticsearch index

   * - nice2.enterprisesearch.elasticsearch.username
     - ``nice-test307``
     - username for elasticsearch service

   * - nice2.enterprisesearch.elasticsearch.password
     -
     - password for elasticsearch service

   * - nice2.enterprisesearch.elasticsearch.searchFields
     - ``pk,*_de,*_en,*_it,*_fr,*_t,*_b,*_dt,*_i,*_l,*_f,*_d``
     - fields to be searched in elasticsearch queries without specific field limitations

   * - nice2.enterprisesearch.queryfunction.queryLimit
     - ``30000``
     - the maximum number of results to be found when using the ``fulltext`` tql function. Combined with all other search parameters this must be below the maximum number of parameters of the used postgresql version (e.g. below 32767).

   * - nice2.enterprisesearch.indexfix.queryPartitionSize
     - ``10000``
     - partition size to be used in indexfix task. Can not be larger than the ``max_result_window`` of the corresponding elasticsearch installation. 

   * - nice2.enterprisesearch.elasticsearch.queryPageSize
     - ``10000``
     - page size to be used when searching. Can not be larger than the ``max_result_window`` of the corresponding elasticsearch installation.

   * - nice2.enterprisesearch.indexingPartitionSize
     - ``1000``
     - partition size to be used when indexing multiple entities.


S3
--

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - s3.main.bucketName
     - ``tocco-nice-test``
     - Name of the bucket to use

   * - s3.main.endpoint
     - ``https://objects.rma.cloudscale.ch``
     - Endpoint URL

   * - _`s3.main.accessKeyId`
     -
     - ID of the secret key

       For production, a user and a corresponding key is generated
       by Ansible. For development, a user is created for every
       developer (:doc:`/devops/s3/s3_bucket_for_dev`).

   * - s3.main.secretAccessKey
     -
     - Secret key

       See also `s3.main.accessKeyId`_

See also:

* :doc:`/framework/architecture/s3/s3`
* :doc:`/devops/s3/s3_bucket_for_dev`

Query-Timeout
-------------

.. note::

   There is a global database query timeout that cancels database queries that run for too long. This was added to stop
   the possibility to crash the database server with large queries. If the limits need to be adjusted the following
   properties may be used.

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - nice2.persist.defaultQueryTimeout
     - ``60000``
     - Sets the global database query timeout in milliseconds (maximum runtime of a database query, default is 60000).

   * - nice2.persist.dwrQueryTimeout
     - ``120000``
     - Sets the dwr database query timeout in milliseconds (maximum runtime of a database query invoked by dwr, default is 120000)

Startup Options
---------------

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - ch.tocco.nice2.disableRoleSync
     - ``true``/``false``
     - Wether to check for unused and missing roles

       Iff ``false``, roles used in ACLs are compared to roles in
       DB and discrepancies are logged.

   * - ch.tocco.nice2.disableLanguageSync
     - ``true``/``false``
     - Whether to check configured languages against languages on DB

   * - ch.tocco.nice2.enterprisesearch.disableStartup
     - ``true``/``false``
     - Whether to disable the creation of an index for fulltext search

   * - ch.tocco.nice2.disableStartupJsFileGeneration
     - ``true``/``false``
     - Wether to generate JS during startup

       Iff ``false``, JS files are generated on first use.

   * - ch.tocco.nice2.disableSchemaModelStartupCheck
     - ``true``/``false``
     - Whether DB schema should be validated against model

   * - ch.tocco.nice2.cms.template.synchronize.enable
     - ``true``/``false``
     - Whether templates should be synced to the DB during startup

       This setting affects CMS templates.

       Enabled by default unless :doc:`run_env`
       is set to ``development``.

   * - ch.tocco.nice2.enableUpgradeMode
     - ``true``/``false``
     - Whether to start application in upgrade mode

       In upgrade mode, schema is upgraded and application terminated.

       On OpenShift, this is controlled by the `entrypoint script`_.


Web
---

.. list-table::
   :header-rows: 1
   :widths: 10 10 20

   * - Name
     - Valid Values / Examples
     - Description

   * - nice2.web.cookie.sameSite
     - ``Lax``/``None``/``Strict``
     - Explicitly set cross-site cookie behavior

       This is used for development only. **Do not set this for**
       **any customer installation.**

       See also `SameSite cookies`_

       **Ansible:**

       variable: |env|

   * - nice2.web.core.referrerPolicy
     - ``strict-origin-when-cross-origin``
     - Can be used to override the "Referrer-Policy" header. If
       this property is set to empty, the header will no longer be sent.

       **Ansible:**

       variable: |env|

.. |env| replace::

   set this via :doc:`env </devops/app_management/ansible_env>` variable

.. _Activity report (System_activity): https://master.tocco.ch/tocco/e/System_activity/list
.. _duplicates: https://300.docs.tocco.ch/de/address_beschreibung_doubletten.html
.. _HakariCP Configuration Options: https://github.com/brettwooldridge/HikariCP#gear-configuration-knobs-baby
.. _entrypoint script: https://gitlab.com/toccoag/nice2/-/blob/master/customer/docker-root/usr/local/bin/entrypoint
.. _SameSite cookies: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite
.. _Task_execution: https://master.tocco.ch/tocco/e/Task_execution/list

.. rubric:: Footnotes

.. [#f1] Deterministically derived from ``master_secret`` (a random value), ``db_server`` and ``db_user``.
   Thus, moving the DB to a different host changes the password.
