############################
Less Style Guide
############################

Overview
========

This code style guide establishes the basic rules on how to write Less stylesheets for our Corporate Designs and Reports.

Naming Less stylesheets for Corporate Designs
---------------------------------------------

The created less stylesheets must always be named in the same pattern shown below.

In general we always create 3 types of CDs.
One for correspondences, one for finances and one for reports.

.. code-block::

  {instance}_{type}_{version}.less

  // examples
  physioswiss_correspondence_36.less
  physioswiss_finance_36.less
  physioswiss_report_36.less

Naming rules
------------

- Use Hyphen Delimited rule names. It is a pretty standard CSS naming convention. It is arguably more readable
- English names only

.. code-block::

    /* BAD - not hyphen delimited and in German */
    .roteBox {
      border: 1px solid red;
    }

    /* GOOD - hyphen delimited and English */
    .red-box {
      border: 1px solid red;
    }

Structuring Less stylesheets for Corporate Designs
--------------------------------------------------

When creating CDs Stylesheets it is important to always structure them in the same way from top to bottom. This will ensure consistency and maintainability.

The visualisation of what the default variables defined in ``core/reporting/resources/resources/reporting/_variables.less`` do can be seen in these PDFs:

- :download:`Korrespondenz.pdf <resources/Korrespondenz.pdf>`
- :download:`Finanzen.pdf <resources/Finanzen.pdf>`
- :download:`Reports.pdf <resources/Reports.pdf>`

**Procedure**

- Import Fonts first
- Overwrite the default less variables if necessary
- Declare `#header` styles (this ID selects the templatesnippets in the header part of the CD)

  - Nest the styles for logos and other assets within the header
  - When defining the size of logos, it is **important to define both dimensions of SVG (width and height)**. 
    Otherwise the PDF Engine seems to render a white border around the SGV which offsets its position.
- Declare other necessary rules
- Declare `#footer` styles (this ID selects the templatesnippets in the footer part of the CD)

Below is an example of a commented correspondence CD for a better understanding.

.. code-block::

  /* LOAD FONTS */
  [@loadGoogleFont url="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap"/]

  /* OVERWRITE DEFAULT VARIABLES */
  @footer-height: 33mm;

  @font-family-base: 'Poppins', sans-serif;

  @font-size-base: 10pt;

  @base-document-margin-left: 35mm;
  @base-document-margin-right: 27mm;

  @correspondence-document-padding-top: 84mm;

  /* DECLARE HEADER STYLES */
  #header {
      #physioswiss_logo {
          position: absolute;
          top: 10mm;
          left: 21mm;

          /* NOTE THAT BOTH DIMENSIONS OF THE SVG ARE DECLARED (HEIGHT & WIDTH) */
          svg {
              width: 50mm;
              height: 21.4mm;
              position: relative;
          }
      }
  }

  /* DECLARE OTHER STYLES */
  .sender-wrapper {
      font-size: 8pt;
      white-space: nowrap;
  }

   /* DECLARE FOOTER STYLES */
  #footer {
      margin-left: @base-document-margin-left;

      #physioswiss_shipping_logo {
          margin-top: 1.5mm;
          width: 52mm;
          height: 10.42mm;
      }

      #physioswiss_footer_address {
          position: relative;
          top: 7.5mm;
          font-size: 8pt;
          color: #213a8f;
          margin-left: 104mm - @base-document-margin-left;
      }

      #pagination {
          text-align: left;
          display: inline-block !important;
          position: absolute;
          top: 20mm;
          left: 160mm;
          font-size: 8pt;
      }

      &.first-page {
          #pagination {
              display: none !important;
          }
      }
  }