.. definitions included in all ReST files
.. ======================================

.. global roles
.. ------------

.. role:: strike
    :class: strike

.. role:: blue
    :class: blue

.. role:: green
    :class: green

.. role:: orange
    :class: orange

.. role:: red
    :class: red

.. role:: large-and-bold
    :class: large-and-bold

.. global links
.. ------------

.. _VSHN Control: https://control.vshn.net
.. _Requests and Limits: https://docs.okd.io/latest/admin_guide/overcommit.html#requests-and-limits
.. _Dockerfile reference: https://docs.docker.com/engine/reference/builder/

.. raw:: html

    <div class="build-pipeline-status">
      <a href="https://gitlab.com/toccoag/tocco-docs/-/pipelines"
         referrerpolicy="no-referrer">
        <img alt=""
             src="https://gitlab.com/toccoag/tocco-docs/badges/master/pipeline.svg"
             referrerpolicy="no-referrer"
             height="20"
             width="116"/>
      </a>
    </div>

.. rst-class:: tocco-disclaimer

   This is an internal documentation. There is a good chance
   you're looking for something else. See :doc:`/about/disclaimer`.
